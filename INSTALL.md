## Step 1: development environment setup.

1. Accept the invitation to view the 'ats' repository.
2. Open this repo link in a browser: https://bitbucket.org/royselim/ats
3. Clone the repo on your working directory using this command: git clone https://royselim@bitbucket.org/royselim/ats.git
4. Setup the development environment (NodeJS, electron@6)
5. Download NodeJS LTS from the node website and install the MSI
6. Install electron@6 globally using this command npm -i g electron@6
7. On the terminal, go to the ats folder and run 'yarn install' or 'npm install'.
8. Open the source code in any code editor, the 'node_modules' directory should now be added.
9. On a browser, open the ATS website: https://atsmro4.sharepoint.com/sites/ai
10. On the terminal, on the working directory, run 'yarn proxy' or 'npm run proxy'
11. Continue by entering the sharepoint url: https://atsmro4.sharepoint.com/sites/ai
12. On the next prompt, choose 'On-demand'
13. Open the proxy server: http://localhost:8081/
14. Test that the proxy is working by typing: _api/web in the SharePoint REST relative endpoint: textbox then tap 'Send HTTP request' using 'GET'
15. Enter your credentials and your MFA code
16. Run 'yarn startServers' or 'npm run startServers'
17. Home page should now display properly.

## Step 2: Testing the main menu 
### Debugging 'About your work' page.
1. In the code editor, expand the 'public' folder and open the file 'index.html'.
2. Replace line 53 & 54 with line 63 & 64

```
// line 53 & 54
<div id="root" style="padding-bottom: 40px;">
</div>
```

```
// line 63 & 64
<div id="secondaryPage" style="padding-bottom: 40px;">
</div>		
```
3. Copy this address: http://localhost:3000/SecondaryPage.aspx?group=About%20Your%20Work
4. The elements may not be formatted properly, this is okay. The next steps describes how to adjust the styling.
5. Open the file 'src/components/alternates/AltSecondaryPage.js' and edit line 130 to this 

```
<div className="ui stackable grid" style={{marginRight: '0px', marginTop: '0px'}}>
```

### Debugging 'About ATS', 'About You', 'Ats Academy', 'Giving Back', and 'Leaders' page.
1. In the code editor, expand the 'public' folder and open the file 'index.html'.
2. Replace line 53 & 54 with line 73 & 74

```
// line 53 & 54
<div id="root" style="padding-bottom: 40px;">
</div>
```

```
// line 73 & 74
<div id="aboutYou" style="padding-bottom: 40px;">
</div>
```
3. Copy any one of these addressess: http://localhost:3000/Template4.aspx?page=About%20ATS | http://localhost:3000/Template4.aspx?page=About%20You | http://localhost:3000/Template4.aspx?page=Ats%20Academy | http://localhost:3000/Template4.aspx?page=giving%20back | http://localhost:3000/Template4.aspx?page=leaders
4. The elements may not be formatted properly, this is okay. The next steps describes how to adjust the styling.
5. Open the file 'src/components/alternates/AboutYou.js' and edit line 207 to this 

```
<div className="ui stackable grid" style={{marginRight: '0px', marginBottom: '-70px', marginTop: '0px'}}>
```

### Debugging 'ATS Safety Hotline Reporting - web form'
1. When a user click the 'Safety Hotline' banner on the top right, a new tab opens with a web form.
2. In the code editor, expand the 'public' folder and open the file 'index.html'.
3. Replace line 53 & 54

```
// line 53 & 54
<div id="root" style="padding-bottom: 40px;">
</div>
```

with this line:
```
<div id="reportSafetyQualityConcern" style="padding-bottom: 40px;">
</div>
```
4. Open this address: http://localhost:3000/

### Debugging 'Airworthiness/ASAP Form'
1. When a user click the 'Airworthiness' banner on the top right, a new tab opens with a web form.
2. In the code editor, expand the 'public' folder and open the file 'index.html'.
3. Replace line 53 & 54

```
// line 53 & 54
<div id="root" style="padding-bottom: 40px;">
</div>
```

with this line:
```
<div id="asapForm" style="padding-bottom: 40px;">
</div>
```
4. Open this address: http://localhost:3000/

### Debugging news Archives
1. When the user click the 'MORE NEWS' link, the user is redirected to the news archives.
2. In the code editor, expand the 'public' folder and open the file 'index.html'.
3. Replace line 53 & 54

```
// line 53 & 54
<div id="root" style="padding-bottom: 40px;">
</div>
```

with this line

```
<div id="archive" style="padding-bottom: 40px;">
</div>
```
4. The formatting maybe off, we need to edit a few lines of code to correct this.
5. Replace line #75 of the file src/components/Archive.js with the code below. And refresh the page
```
return <div className="ui stackable grid" style={{marginTop: '0px'}}>
```


## Step 3: Build and deployment
1. Rename the build directory/folder to build_bakup inside the working directory.
2. In the termainal, in the working directory, type 'yarn build' or 'npm run build'
3. A new build folder will be created. 
4. Open the master page gallery: https://atsmro4.sharepoint.com/sites/ai/_catalogs/masterpage/Forms/AllItems.aspx

Note: For business logic changes.
5. Open the ats-redux folder, open the static subfolder and the js subfolder.
6. On the menu, click the 'FILES', and click 'Upload document'
7. Navigate to the {working directory}/build/static/js/ folder and upload the .js file

Note: If there are changes to the styles, a different .css file will also be created
8. Open the ats-redux folder, open the static subfolder and the css subfolder.
9. On the menu, click the 'FILES', and click 'Upload document'
10. Navigate to the {working directory}/build/static/css/ folder and upload the .css file.

11. Open the site https://atsmro4.sharepoint.com/sites/ai in SharePoint Designer 2013.
12. Click the 'Master Pages' on the left navigation pane.
13. Click on 'Display Templates' folder on the list.
14. Click on index.html and click 'edit file'.
15. To replace the css file using the new .css build, go to line #22 and replace the name of the referenced css file.
15. To replace the js file using the new .js build, go to line #160 and replace the name of the referenced js file.
16. Save the changes and refresh the SharePoint page to see the changes.
 