import React,{Component} from 'react'

export default class DeiRecommendationsOnlineForm extends Component {
	constructor(props){
        super(props)
        this.state = {
            fields: [],
            refs: {},
            posted: false,
            error: false,
            errorText: null,
            submitted: false
        }
        this.url = window.location.href.split('/Pages/')[0]
        this.options = {
			headers: {"accept": "application/json; odata=verbose"},
			credentials: 'include'
        }
    }

    componentDidMount(){
        fetch(				
			this.url + "/_api/web/lists/GetByTitle('DEI')/fields?$filter=Hidden eq false and CanBeDeleted eq true&$select=Title,FieldTypeKind,Choices,StaticName,Description", 
			this.options              
		).then( res => {
			res.json().then(json => {
				this.setState({
                    fields: json.d.results
                })                
        
                const fields = this.state.fields
                fields.length && fields.forEach(f => {
                    this.setState(state => {
                        return {
                            refs: Object.assign(state.refs, {}, {
                                [f.StaticName] : React.createRef()
                            })
                        }
                    })
                })
			})
        })
    }

    simpleHeader = (text) => <div className="ui inverted blue segment text-center"><h4 className="ui header">{text}</h4></div>
    simpleField = (fields,className) => <div className={className||'fields'}>{fields.map(i=>this.createInput(...i))}</div>
    getCol = (column,staticName) => this.state.fields.find(f => f.StaticName === staticName)[column]

    handleSubmit = (e) => {
        this.setState({submitted: true})
		e.preventDefault()
        e.stopPropagation()
        let {
            refs
        } = this.state
        let data = {
            __metadata: { 'type': 'SP.Data.DEIListItem'},
            Title: 'DEI Recommendation'
        }
        for(var n in refs ){
            let colType = this.getCol('FieldTypeKind',n)
            if(colType === 2 || colType === 3 || colType === 4 || colType === 9){
                let value = refs[n].current.value
                if(value) data[n] = value
            }
        }
        let options = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                "accept": "application/json; odata=verbose",
                "content-type": "application/json;odata=verbose",
                "X-RequestDigest": document.getElementById('__REQUESTDIGEST').value,
                "IF-MATCH": "*"            
            },
            credentials: 'include'
        }
        fetch(
            this.url + "/_api/web/lists/GetByTitle('DEI')/items",
            options
        ).then(res => {
            if(res.ok){
                this.setState({
                    posted: true
                })
            } else {
                alert('Error')
            }
        })
    }

    createInput = (staticName, className, type, note = false) => {
        const refs = this.state.refs
        const descr = this.getCol('Description', staticName)
        return(
            <div key={staticName} className={className||'field'}>
                <label htmlFor={staticName}>{descr}</label>
                {
                    !note ? <input ref={refs[staticName]} type={type || 'text'} />:
                    <textarea ref={refs[staticName]} 
                        type={type || 'text'} 
                        name={staticName}>
                    </textarea>
                }
            </div>
        )
    }

    render(){
        let {
            fields
        } = this.state
        const url = window.location.href.split('/Pages/')[0]
        const spinner = <div style={{textAlign: 'center'}}>
            <i className="fa fa-circle-notch fa-spin fa-md"></i>
        </div>
        return (
            <div>
                {
                    this.state.submitted ?
                    <div className="ui container">
                        <div className="column" style={{marginTop: "50px", marginBottom: "300px"}}>
                            <div className="ui segment centered">
                                {
                                    this.state.posted ?
                                    <div className="information-submitted" style={{fontSize: '18px', fontWeight: 'bold'}}>
                                        {`Thank you, the information is being submitted. `} 
                                        <a href={`${url}/Pages/DEI.aspx`}>
                                            Go back to the form
                                        </a> | <a href={url}>Home</a>
                                    </div>: spinner
                                }
                            </div>
                        </div>
                    </div>:
                    fields.length && 
                    <div className="container pb-5 pt-2 my-5 shadow" style={{width: '100%', maxWidth: '930px', margin: 'auto', backgroundColor: 'rgb(255, 255, 255)', borderRadius: '10px'}}>
                        <div className="ui blue ribbon massive label">
                            DEI RECOMMENDATIONS
                        </div>
                        <br />
                        <div className="ui image" style={{margin: '2rem 0'}}>                            
                            <span style={{fontSize: '20px'}}><i>We want to hear your voice!</i></span>
                        </div>          

                        <div className="ui equal width form success">	
                            {this.simpleField([
                                ['NameorEmployeeNo','sixteen wide field']])}
                            {this.simpleField([
                                ['Location','eight wide field'],
                                ['Shift','eight wide field']])}
                            <br />
                            <div className="text-center">
                                <p className="text-left mb-5" style={{color: '#797979'}}>
                                    We truly value your thoughts and experiences relating to diversity, equity, 
                                    and inclusion (DEI). Below are just a few questions to help guide your 
                                    feedback. Please feel free to share any additional ideas or suggestions that 
                                    you believe are important to be heard. Your unique perspectives are 
                                    essential to creating a more inclusive and equitable workplace for everyone.
                                </p>
                            </div>
                            {this.createInput('Howcanwepromote','field','text',true)}
                            {this.createInput('Howcanweimprove','field','text',true)}
                            {this.createInput('Whatresources','field','text',true)}
                            {this.createInput('Whatnon_x002d_profit','field','text',true)}
                            {this.createInput('Whattypeofculturalawareness','field','text',true)}
                            {this.createInput('WhatotherDEIideas','field','text',true)}
                            {this.createInput('OtherComments','field','text',true)}

                            <div className="field text-center my-5">
					            <button className="ui button large blue" type="submit" onClick={this.handleSubmit}>Submit</button>
					        </div>

                            <div className="sixteen wide computer sixteen wide tablet mobile column no-padding" 
                                style={{
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center center',
                                    backgroundRepeat: 'no-repeat',
                                    height: "330px",
                                    backgroundImage: "url('https://atsmro4.sharepoint.com/sites/ai/PublishingImages/DEI-Pad-People.png')",
                                    padding: '0px',
                                    margin: '-15px',
                                    marginBottom: '-45px'
                                }}>
                        </div>   
                        </div>
                    </div>
                }
            </div>
        )
    }
}