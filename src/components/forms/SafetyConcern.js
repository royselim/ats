import React,{Component} from 'react'
import {getSPListFields} from '../helpers/GetSPListFields'
import {updateSPListItems} from '../helpers/UpdateSPListItems'

export default class SafetyConcern extends Component {
	constructor(props){
		super(props)

		this.reportDate = React.createRef()
		this.location = React.createRef()
		this.locationOther = React.createRef()
		this.details = React.createRef()
		this.fullName = React.createRef()

		this.state = {
			posted: false,
			submitted: false,
			error: false,
			errorText: null
		}

		this.url = window.location.href.split('/Pages/')[0]
	}
	
	componentDidMount(){
		getSPListFields(this.url, 'ATS Safety Hotline Reports','&$select=Title,FieldTypeKind,Choices')
		.then(rslt => {
            if(Array.isArray(rslt)){
                this.setState({
					fields: rslt
				})
            } else {
                // TODO
            }
        })
	}

	handleClick = (e) => {
		this.setState({
			submitted: true
		})

		e.preventDefault()
		e.stopPropagation()

		let data = {
			__metadata: { 'type': 'SP.Data.ATS_x0020_Safety_x0020_Hotline_x0020_ReportsListItem'},
			Title: 'Alert! ATS Safety Report',
			Report_Date: this.reportDate.current.value,
			Location: this.location.current.value,
			Location_Other: this.locationOther.current.value,
			Details: this.details.current.value,
			Full_Name: this.fullName.current.value
		}

		let href = this.url + "/_api/web/lists/GetByTitle('ATS Safety Hotline Reports')/items"
		updateSPListItems(href, data, 'POST').then(ok => this.setState({posted: ok}))
	}
    render(){
		const spinner = <div style={{textAlign: 'center'}}>
			<i className="fa fa-circle-notch fa-spin fa-md"></i>
		</div>

		const {fields, submitted, posted} = this.state
		const locations = ( fields && fields.length && fields.find(f => f.Title === 'Location').Choices.results ) || []

        return(
			<div>
				{
					submitted ?
					<div className="ui container">
						<div className="column">
							<div className="ui segment centered">
								{
									!posted ? spinner:
									<div className="information-submitted" style={{fontSize: '18px', fontWeight: 'bold'}}>
										{`Thank you, the information is being submitted. `} 
										<a href={`${this.url}/Pages/ReportSafetyQualityConcern.aspx`}>
											Go back to the form
										</a> | <a href={this.url}>Home</a>
									</div>
								}
							</div>
						</div>
					</div>:
					<div className="container pb-5 pt-2 shadow my-5" style={{width: '100%', maxWidth: '930px', margin: 'auto', backgroundColor: '#fff', borderRadius: '10px'}}>
						<div className="ui equal width form success">
							{/* <!-- Header --> */}
							<div className="ui red ribbon massive label">
								<i className="shield icon"></i>ATS Safety Hotline Reporting - Web Form
							</div>
							<div className="ui image" style={{margin: '2rem 0'}}>                            
									<div className="img-wrapper my-2" style={{width: '65%'}}>                    
									<img 
										src="https://atsmro4.sharepoint.com/sites/ai/_api/siteiconmanager/getsitelogo" 
										alt=""
										height="auto"
										width="30%"
									/>
									</div>
							</div>          
															
							{/* <!-- Title --> */}
							<div className="container">
									<div className="ui section divider"></div>
							</div>

							<div ref={this.formRef} className="container my-5">
								<div className="field mb-5" style={{color: '#797979'}}>
									<p><strong>Thank you</strong> for taking time to report your safety concern. You can also call and leave an anonymous voicemail message at <strong><span style={{color: 'rgb(155, 0, 0)'}}>1.855.757.TIPS (8477)</span></strong>. If you'd rather type your responses, please complete the form below.</p>
									<p>Thanks for putting safety first at ATS!</p>
								</div>
						
								<div className="field" style={{color: '#5f5f5f'}}>
									<div className="ui inverted red segment">
										<h4 className="ui header">1. On approximately what DAY did the safety concern take place? If ongoing concern, please enter today's date.</h4>
									</div>
								</div>
		
								<div className="report-date four wide field">
									<label for="date">Date</label>
									<input ref={this.reportDate} type="date" placeholder="mm/dd/yyyy" />
								</div>
						
								<div className="ui section divider"></div>
						
								<div className="field" style={{color: '#5f5f5f'}}>
									<div className="ui inverted red segment">
										<h4 className="ui header">2. At which LOCATION did the safety concern take place?</h4>
									</div>
								</div>
		
								<div className="four wide field">
									<label for="location">Select Location</label>
									<select ref={this.location} name="location" id="">
										<option value="" disabled selected></option>
										{
											locations.length && locations.map((l,i) => <option key={i} value={l}>{l}</option>)
										}
									</select>
								</div>
		
								<div className="field">
									<label for="other-location">Other (please specify)</label>
									<input ref={this.locationOther} type="text" name="other-location" />
								</div>
						
								<div className="ui section divider"></div>
						
								<div className="field" style={{color: '#5f5f5f'}}>
									<div className="ui inverted red segment">
										<h4 className="ui header">3. Tell us more about the SAFETY CONCERN you'd like to report.</h4>
									</div>
									<h5>Please include helpful details, like specific building, location, description of equipment and people involved, etc.</h5>
								</div>
		
								<div className="eight wide field">
									<label for="concern">Tell us more</label>
									<textarea ref={this.details} name="concern" id="" cols="50" rows="4"></textarea>
								</div>
						
								<div className="ui section divider"></div>
						
								<div className="field" style={{color: '#5f5f5f'}}>
									<div className="ui inverted red segment">
										<h4 className="ui header">4. Your name (optional):</h4>
									</div>
								</div>
								<div className="six wide field">
									<label for="name">Full Name</label>
									<input ref={this.fullName} type="text" name="name" placeholder="Full name"/>
								</div>
						
								<div className="ui section divider"></div>
						
								<div className="field text-center my-5">
									<button className="ui button huge red" type="submit" onClick={this.handleClick}>Done</button>
								</div>
						
								<div className="field text-center my-5">
									<p className="ui text-center">&copy; 2019 Aviation Technical Services</p>
								</div>					
							</div>
						</div>        
					</div>    
				}
			</div>
        )
    }
}