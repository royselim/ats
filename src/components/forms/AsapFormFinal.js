import React,{Component} from 'react'

export default class AsapForm extends Component {
	constructor(props){
        super(props)
        this.state = {
            fields: [],
            refs: {},
            posted: false,
            error: false,
            errorText: null,
            submitted: false
        }
        this.url = window.location.href.split('/Pages/')[0]
        this.options = {
			headers: {"accept": "application/json; odata=verbose"},
			credentials: 'include'
        }
    }

    componentDidMount(){
        fetch(				
			this.url + "/_api/web/lists/GetByTitle('ASAP')/fields?$filter=Hidden eq false and CanBeDeleted eq true&$select=Title,FieldTypeKind,Choices,StaticName,Description", 
			this.options              
		).then( res => {
			res.json().then(json => {
				this.setState({
                    fields: json.d.results
                })                
        
                const fields = this.state.fields
                fields.length && fields.forEach(f => {
                    this.setState(state => {
                        return {
                            refs: Object.assign(state.refs, {}, {
                                [f.StaticName] : React.createRef()
                            })
                        }
                    })
                })
			})
        })
    }

    asapField = (text,ndx) => <div key={ndx} className="field"><label htmlFor="">{text}</label><input type="text" name="" disabled /></div>
    simpleHeader = (text) => <div className="ui inverted blue segment text-center"><h4 className="ui header">{text}</h4></div>
    simpleField = (fields,className) => <div className={className||'fields'}>{fields.map(i=>this.createInput(...i))}</div>
    getCol = (column,staticName) => this.state.fields.find(f => f.StaticName === staticName)[column]

    getRadioValue = (ref) => {        
		let ret = Array.from(ref.current.children).find((f,i) => {
            return f.children[0].children[0].checked
        })
		return ret && ret.textContent
	}

    handleSubmit = (e) => {
        this.setState({submitted: true})
        
		e.preventDefault()
        e.stopPropagation()

        let {
            refs
        } = this.state

        let data = {
            __metadata: { 'type': 'SP.Data.ASAPListItem'},
            Title: 'ASAP Report'
        }

        for(var n in refs ){
            let colType = this.getCol('FieldTypeKind',n)
            if(colType === 2 || colType === 3 || colType === 4 || colType === 9){
                let value = refs[n].current.value
                if(value) data[n] = value
            }
            else if(colType === 6){
                let value = this.getRadioValue(refs[n])
                if(value) data[n] = value
            }
            else if(colType === 8){
                let value = this.getRadioValue(refs[n])
                if(value) data[n] = value === 'Yes' ? true: false
            }
        }

        let options = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                "accept": "application/json; odata=verbose",
                "content-type": "application/json;odata=verbose",
                "X-RequestDigest": document.getElementById('__REQUESTDIGEST').value,
                "IF-MATCH": "*"            
            },
            credentials: 'include'
        }
        
        fetch(
            this.url + "/_api/web/lists/GetByTitle('ASAP')/items",
            options
        ).then(res => {
            if(res.ok){
                this.setState({
                    posted: true
                })
            } else {
                alert('Error')
            }
        })
    }

    createInput = (staticName, className, type, note = false) => {
        const refs = this.state.refs
        const label = this.getCol('Title', staticName)
        const descr = this.getCol('Description', staticName)
        return(
            <div key={staticName} className={className||'field'}>
                <label htmlFor={staticName}>{label}</label>
                {
                    !note ? <input ref={refs[staticName]} type={type || 'text'} placeholder={descr} />:
                    <textarea ref={refs[staticName]} 
                        type={type || 'text'} 
                        name={staticName} 
                        placeholder={descr}>
                    </textarea>
                }
            </div>
        )
    }

    createSelect = (staticName, className, selectClass, key) => {
        const refs = this.state.refs
        let choices = this.getCol('Choices',staticName)
        return(
            <div key={key} className={className||'field'}>
                <label htmlFor={staticName} className="text-center">{this.getCol('Title',staticName)}</label>
                <div ref={refs[staticName]} className={`inline fields pt-2 ${selectClass}`}>
                    {
                        ((choices && choices.results) || ['Yes','No']).map((c,i) => {
                            return (
                                <div key={i} className="field">
                                    <div className="ui radio checkbox ui-radio-checkbox">
                                        <input type="radio" name={staticName} checked="" tabIndex="0" className="hidden" />
                                        <label htmlFor={c}>{c}</label>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }

    render(){
        let {
            fields
        } = this.state
        const url = window.location.href.split('/Pages/')[0]
        const spinner = <div style={{textAlign: 'center'}}>
            <i className="fa fa-circle-notch fa-spin fa-md"></i>
        </div>
        return (
            <div>
                {
                    this.state.submitted ?
                    <div className="ui container">
                        <div className="column">
                            <div className="ui segment centered">
                                {
                                    this.state.posted ?
                                    <div className="information-submitted" style={{fontSize: '18px', fontWeight: 'bold'}}>
                                        {`Thank you, the information is being submitted. `} 
                                        <a href={`${url}/Pages/AsapForm.aspx`}>
                                            Go back to the form
                                        </a> | <a href={url}>Home</a>
                                    </div>: spinner
                                }
                            </div>
                        </div>
                    </div>:
                    fields.length && 
                    <div className="container pb-5 pt-2 my-5 shadow" style={{width: '100%', maxWidth: '930px', margin: 'auto', backgroundColor: 'rgb(255, 255, 255)', borderRadius: '10px'}}>
                        <div className="ui blue ribbon massive label">
                            <i className="lock icon"></i>CONFIDENTIAL
                        </div>
                        <div className="ui image" style={{margin: '2rem 0'}}>                            
                            <div className="img-wrapper my-2" style={{width: '65%'}}>                    
                            <img 
                                src="https://atsmro4.sharepoint.com/sites/ai/_api/siteiconmanager/getsitelogo" 
                                alt=""
                                height="auto"
                                width="30%"
                            />
                            </div>
                        </div>          
                                        
                        {/* <!-- Title --> */}
                        <div className="container">
                            <div className="ui section divider"></div>
                        </div>

                        <div className="text-center">
                            <p className="text-left mb-5" style={{color: '#797979'}}>
                                Note: There is a 24 hour limitation on submitting this report. Refer to the ATS Employee Policy Manual, “Aviation Safety Action Program (ASAP) Description”, QA-02 for more information.
                            </p>
                        </div>

                        <div className="ui equal width form success">		
                            {/* <!-- PERSONAL INFORMATION --> */}
                            {this.simpleHeader('Personal Information')}
                            {this.simpleField([
                                ['Full_x0020_Name','ten wide field'],
                                ['Employee_x0020_Number','six wide field','number']])}
                            {this.simpleField([['Address']])}
                            {this.simpleField([['City'],['State'],['Zip_x0020_Code']])}
                            {this.simpleField([
                                ['Home_x002f_Cell_x0020_Number','five wide field'],
                                ['Best_x0020_time_x0020_to_x0020_c','two wide field','time']])}
                            {this.simpleField([
                                ['Work_x0020_Number','five wide field'],
                                ['Best_x0020_time_x0020_to_x0020_c0','two wide field','time']])}

                            {/* <!-- WORK HISTORY --> */}  
                            {this.simpleHeader('Work History')}
                            {this.simpleField([
                                ['Date_x0020_of_x0020_Occurrence','field','date'],
                                ['Time_x0020_of_x0020_Event'],['Position_x002f_Title']])}
                            {this.simpleField([['Station'],['Location'],['Work_x0020_Shift']])}
                            {this.createInput('Consecutive_x0020_days_x0020_wor')}
                            {this.simpleField([
                                ['Did_x0020_event_x0020_occur_x002','eight wide field'],
                                ['Number_x0020_of_x0020_hours_x002','eight wide field','number']])}

                            {/* <!-- Aircraft Information --> */}
                            {this.simpleHeader('Aircraft Information (as applicable)')}
                            {this.simpleField([
                                ['Type_x0020_of_x0020_aircraft','six wide field'],
                                ['Reg_x0020_number','six wide field'],
                                ['ATA_x0020_Code','four wide field']])}
                            {this.simpleField([['Other'],['P_x002f_N'],['S_x002f_N']])}
                            {this.simpleField([['Component_x0020_Name'],['Component_x0020_Position']])}

                            {/* <!-- DOCUMENTS --> */}
                            {this.simpleHeader('Documents')}
                            {this.simpleField([
                                ['Job_x0020_number','ten wide field'],
                                ['Work_x0020_Card_x0020_number','ten wide field']])}
                            {this.createInput('List_x0020_Technical_x0020_Refer','field','text',true)}

                            {/* <!-- SUMMARY --> */}
                            {this.simpleHeader('Summary of Event')}
                            {this.createInput('Summary','field','text',true)}

                            {/* <!-- Contributing Factors --> */}
                            {this.simpleHeader('Contributing Factors')}
                            {this.simpleField([['Items_x0020_that_x0020_contribut','field','text',true]])}
                            {['Equipment_x0020__x002f__x0020_To','Aircraft_x0020_Design_x0020__x00',
                                    'Technical_x0020_Data','Environment','Facilities','Communication',
                                    'Individual_x0020_Factors'
                                ].map((i,x)=>this.createSelect(i,'inline field shadow pb-1','mb-4', x))}

                            {/* <!-- ADDITIONAL COMMENTS, OR RECOMMENDATIONS --> */}
                            {this.createInput('Additional_x0020_Comments','field','text',true)}

                            {/* <!-- SIGNATURE AND DATE --> */}
                            {this.simpleField([['Signature','field'],['Date','field','date']],'fields my-5')}

                            <div className="field text-center my-5">
					            <button className="ui button large blue" type="submit" onClick={this.handleSubmit}>Submit</button>
					        </div>

                            <div className="ui section divider"></div>
                            
                            {/* <!-- FOR ASAP USE ONLY --> */}
                            <div className="ui inverted segment text-center">
                                <h4 className="ui header">For ASAP Office Use Only</h4>
                            </div>          
                                            
                            <div className="container">
                                <div className="ui inverted segment">
                                    <div className="fields">
                                        {['ERC #','RECEIPT DATE','TIME','ARMS #'].map((i,x)=>this.asapField(i,x))}
                                    </div>
                                    <div className="field">
                                    <div className="ui inverted red segment text-center">
                                        Warning this document is protected under 14CFR Part 193 Disclosure Act, 49 U.S.C. section 4012
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}