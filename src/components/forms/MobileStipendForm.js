import React,{Component} from 'react'

export default class MobileStipendForm extends Component {
	constructor(props){
        super(props)
        this.state = {
            fields: [],
            refs: {},
            posted: false,
            error: false,
            errorText: null,
            submitted: false
        }
        this.url = window.location.href.split('/Pages/')[0]
        this.options = {
			headers: {"accept": "application/json; odata=verbose"},
			credentials: 'include'
        }
    }

    componentDidMount(){
        fetch(				
			this.url + "/_api/web/lists/GetByTitle('Mobile Stipend Form')/fields?$filter=Hidden eq false and CanBeDeleted eq true&$select=Title,FieldTypeKind,Choices,StaticName,Description", 
			this.options              
		).then( res => {
			res.json().then(json => {
				this.setState({
                    fields: json.d.results
                })                
        
                const fields = this.state.fields
                fields.length && fields.forEach(f => {
                    this.setState(state => {
                        return {
                            refs: Object.assign(state.refs, {}, {
                                [f.StaticName] : React.createRef()
                            })
                        }
                    })
                })
			})
        })
    }

    simpleHeader = (text) => <div className="ui inverted blue segment text-center"><h4 className="ui header">{text}</h4></div>
    simpleField = (fields,className) => <div className={className||'fields'}>{fields.map(i=>this.createInput(...i))}</div>
    getCol = (column,staticName) => this.state.fields.find(f => f.StaticName === staticName)[column]

    handleSubmit = (e) => {
		e.preventDefault()
        e.stopPropagation()
        let {
            refs
        } = this.state
        let data = {
            __metadata: { 'type': 'SP.Data.Mobile_x0020_Stipend_x0020_FormListItem'},
            Title: 'Mobile Phone Stipend Authorization'
        }
        for(var n in refs ){
            let colType = this.getCol('FieldTypeKind',n);
            if(colType === 2 || colType === 3 || colType === 4 || colType === 6 || colType === 9){
                let value = refs[n].current && refs[n].current.value
                if(value) data[n] = value
                else {
                    if (refs[n].current.required) {
                        alert(`${refs[n].current.id} is required`);
                        return;
                    }
                }
            }
        }
        let options = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                "accept": "application/json; odata=verbose",
                "content-type": "application/json;odata=verbose",
                "X-RequestDigest": document.getElementById('__REQUESTDIGEST').value,
                "IF-MATCH": "*"            
            },
            credentials: 'include'
        }
        fetch(
            this.url + "/_api/web/lists/GetByTitle('Mobile Stipend Form')/items",
            options
        ).then(res => {
            if(res.ok){
                this.setState({submitted: true})
                this.setState({
                    posted: true
                })
            } else {
                alert('Error')
            }
        })
    }

    createInput = (staticName, className, type, note = false, required = false, disabled = false) => {
        const refs = this.state.refs
        const descr = this.getCol('Description', staticName);
        const allDesc = descr.split('\n');
        return(
            <div key={staticName} className={className||'field'}>
                {allDesc.map((item, ndx) => {
                    return (
                        <span key={`${ndx}`}>
                            {allDesc.length > 1 && (<br />)}
                            {item !== '' && (<label htmlFor={staticName} style={{fontWeight: 'bold'}}>{item}</label>)}
                        </span>
                    )
                })}
                {
                    !note ? <input ref={refs[staticName]} type={type || 'text'} id={staticName} required={required} style={{backgroundColor: required ? '#00ffff' : 'white'}}/>:
                    <textarea ref={refs[staticName]} 
                        type={type || 'text'} 
                        required={required}
                        name={staticName}
                        id={staticName}
                        disabled={disabled}
                        style={{backgroundColor: required ? '#00ffff' : 'white'}}>
                    </textarea>
                }
            </div>
        )
    }

    createSelect = (staticName, className, required = false, relatedFieldStaticName = undefined) => {
        const {refs, fields} = this.state
        const descr = this.getCol('Description', staticName)
        const options = ( fields && fields.length && fields.find(f => f.StaticName === staticName).Choices.results ) || []
        return(
            <div key={staticName} className={className||'field'}>
                <label for={staticName}>{descr}</label>
                <select 
                    ref={refs[staticName]} 
                    name={staticName} 
                    id={staticName} 
                    required={required} 
                    style={{backgroundColor: required ? '#00ffff' : 'white'}}
                    onChange={(sEvent) => {
                        if(relatedFieldStaticName !== undefined && sEvent.target.value !== options[options.length - 1]) {
                            refs[relatedFieldStaticName].current.required = false;
                            refs[relatedFieldStaticName].current.disabled = true;
                            refs[relatedFieldStaticName].current.value = '';
                        }
                        if(relatedFieldStaticName !== undefined && sEvent.target.value === options[options.length - 1]) {
                            refs[relatedFieldStaticName].current.required = true;
                            refs[relatedFieldStaticName].current.disabled = false;
                        }
                    }}
                >
                    <option value="" disabled selected style={{backgroundColor: 'white'}}></option>
                    {
                        options.length && options.map((l,i) => <option key={i} value={l} style={{backgroundColor: 'white'}}>{l}</option>)
                    }
                </select>
            </div>
        )        
    }

    createCheckbox = (staticName, className, selectClass, key, relatedFieldStaticName) => {
        const refs = this.state.refs
        let choices = this.getCol('Choices',staticName)
        return(
            <div key={key} className={className||'field'} style={{borderColor: 'black', borderWidth: '1px', borderRadius: 5}}>
                <label htmlFor={staticName} className="text-left">{this.getCol('Description',staticName)}</label>
                <div ref={refs[staticName]} className={`inline fields pt-2 ${selectClass}`}>
                    {
                        ((choices && choices.results) || ['Yes','No']).map((c,i) => {
                            return (
                                <div key={i} className="field">
                                    <div className="ui radio checkbox ui-radio-checkbox" onClick={
                                        () => {
                                            refs[staticName].current.value = c;
                                            if(i === 2) {
                                                refs[relatedFieldStaticName].current.value = '0000000000';
                                            } else {
                                                refs[relatedFieldStaticName].current.value = '';
                                            }
                                        }
                                    }>
                                        <input type="radio" name={staticName} id={staticName} checked="" tabIndex="0" className="hidden" />
                                        <label htmlFor={c}>{c}</label>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }

    render(){
        let {
            fields,
            refs
        } = this.state;
        const url = window.location.href.split('/Pages/')[0]
        const spinner = <div style={{textAlign: 'center'}}>
            <i className="fa fa-circle-notch fa-spin fa-md"></i>
        </div>
        const mobileTypes = ( fields && fields.length && fields.find(f => f.StaticName === 'TypeofMobileNumber').Choices.results ) || []

        return (
            <div>
                {
                    this.state.submitted ?
                    <div className="ui container">
                        <div className="column" style={{marginTop: "50px", marginBottom: "300px"}}>
                            <div className="ui segment centered">
                                {
                                    this.state.posted ?
                                    <div className="information-submitted" style={{fontSize: '18px', fontWeight: 'bold'}}>
                                        {`Thank you, the information is being submitted. `} 
                                        <a href={`${url}/Pages/MobileStipendForm.aspx`}>
                                            Go back to the form
                                        </a> | <a href={url}>Home</a>
                                    </div>: spinner
                                }
                            </div>
                        </div>
                    </div>:
                    fields.length && 
                    <div className="container pb-5 pt-2 my-5 shadow" style={{width: '100%', maxWidth: '930px', margin: 'auto', backgroundColor: 'rgb(255, 255, 255)', borderRadius: '10px'}}>
                        <div className="ui blue ribbon massive label">
                            MOBILE PHONE STIPEND AUTHORIZATION FORM
                        </div>
                        <div className="ui image" style={{margin: '2rem 0'}}>
                            <div className="img-wrapper my-2" style={{width: '65%'}}>
                            <img 
                                src="https://atsmro4.sharepoint.com/sites/ai/_api/siteiconmanager/getsitelogo" 
                                alt=""
                                height="auto"
                                width="30%"
                            />
                            </div>
                        </div>
                        <div className="ui" style={{margin: '15px'}}>
                            <span style={{fontSize: '18px', fontWeight: 'bold'}}>
                                THIS FORM MUST BE COMPLETED BY EMPLOYEE'S DEPARTMENT VP AND SUBMITTED TO THE PAYROLL OFFICE.
                            </span>
                        </div>
                        <br />  
                        <div className="ui equal width form success">	
                            {this.createInput('EmployeeName','sixteen wide field','text',false, true)}
                            {this.createInput('EmployeeNumber','sixteen wide field','text',false, false)}
                            {this.createInput('JobTitle','sixteen wide field','text',false, false)}
                            {this.createInput('Department','field','text',false, false)}
                            {this.createInput('AuthorizingManager','field','text',false, true)}
                            <div className="report-date eight wide field">
                                <label for="date">{this.getCol('Description', 'EffectiveDateofPhoneServiceTrans')}</label>
                                <input id={`Date`} required={true} ref={this.state.refs.EffectiveDateofPhoneServiceTrans} type="date" placeholder="mm/dd/yyyy" style={{backgroundColor: '#00ffff'}} />
                            </div>
                            {this.createSelect('ApprovingVP', 'eight wide field', true)}
                            {this.createCheckbox('TypeofMobileNumber', 'sixteen wide field', 'TypeofMobileNumber', 'TypeofMobileNumber', 'MobileNumber')}
                            {this.createInput('MobileNumber','field','text',false, true)}
                            {this.createSelect('DeviceCurrentlyIssuedbyATS', 'eight wide field', true, 'JustificationNeeded')}
                            <div className="field text-center my-5">
                                <span style={{fontSize: '18px', fontWeight: 'bold'}}>
                                    Authorizing $50.00 stipend
                                </span>
                            </div>
                            {this.createInput('JustificationNeeded','field','text',true, false, true)}
                        </div>
                        <div className="field text-center my-5">
                            <button 
                                className="ui button large" 
                                style={{backgroundColor: '#00ffff'}} 
                                type="submit" 
                                onClick={this.handleSubmit}>
                                E-mail to VP for Approval
                            </button>
                        </div>
                    </div>
                }
            </div>
        )
    }
}