import React,{Component} from 'react'

export default class AsapForm extends Component {
	constructor(props){
		super(props)

		this.fullName = React.createRef()
		this.employeeNumber = React.createRef()
		this.address = React.createRef()
		this.city = React.createRef()
		this.state = React.createRef()
		this.zipCode = React.createRef()
		this.homeNumber = React.createRef()
		this.homeNumberBestTimeToCall = React.createRef()
		this.workNumber = React.createRef()
		this.workNumberBestTimeToCall = React.createRef()
		this.dateOfOccurance = React.createRef()
		this.timeOfEvent = React.createRef()
		this.positionTitle = React.createRef()
		this.station = React.createRef()
		this.location = React.createRef()
		this.workShift = React.createRef()
		this.consecutiveDaysWorked = React.createRef()
		this.didEventOccurOvertime = React.createRef()
		this.numberOfHoursWorkedEvent = React.createRef()
		this.typeOfAircraft = React.createRef()
		this.regNumber = React.createRef()
		this.ataCode = React.createRef()
		this.other = React.createRef()
		this.pinNumber = React.createRef()
		this.serialNumber = React.createRef()
		this.componentName = React.createRef()
		this.componentPosition = React.createRef()
		this.jobNumber = React.createRef()
		this.workCardNumber = React.createRef()
		this.listTechnicalReference = React.createRef()
		this.summary = React.createRef()
		this.itemContributedToEvent = React.createRef()
		this.equipmentTools = React.createRef()
		this.aircraftDesign = React.createRef()
		this.technicalData = React.createRef()
		this.environment = React.createRef()
		this.facilities = React.createRef()
		this.communication = React.createRef()
		this.individualFactors = React.createRef()
		this.additionalComments = React.createRef()
		this.signature = React.createRef()
		this.reportDate = React.createRef()
	}

	handleSubmit = (e) => {
		e.preventDefault()
		e.stopPropagation()

		let data = {
			__metadata: { 'type': 'SP.Data.ATS_x0020_Safety_x0020_Hotline_x0020_ReportsListItem'},
			Individual_Factors: this.getRadioValue(this.individualFactors),
			Communication: this.getRadioValue(this.communication),
			Facilities: this.getRadioValue(this.facilities),
			Environment: this.getRadioValue(this.environment),
			Technical_Data: this.getRadioValue(this.technicalData),
			Aircraft_Design: this.getRadioValue(this.aircraftDesign),
			Equipment_Tools: this.getRadioValue(this.equipmentTools),
			Work_Shift: this.getRadioValue(this.workShift),
			Event_Occured_Overtime: this.getRadioValue(this.didEventOccurOvertime),
			Full_Name: this.fullName.current.value,
			Employee_Number: this.employeeNumber.current.value,
			Address: this.address.current.value,
			City: this.city.current.value,
			State: this.state.current.value,
			ZIP_Code: this.zipCode.current.value,
			Home_Number: this.homeNumber.current.value,
			Home_Number_Call_Time: this.homeNumberBestTimeToCall.current.value,
			Work_Number: this.workNumber.current.value,
			Work_Number_Call_Time: this.workNumberBestTimeToCall.current.value,
			Date_Of_Occurance: this.dateOfOccurance.current.value,
			Time_Of_Event: this.timeOfEvent.current.value,
			Position: this.positionTitle.current.value,
			Station: this.station.current.value,
			Location: this.location.current.value,
			Consecutive_Days_Worked: this.consecutiveDaysWorked.current.value,
			Number_Of_Hours_Worked: this.numberOfHoursWorkedEvent.current.value,
			Aircraft_Type: this.typeOfAircraft.current.value,
			Registration_Number: this.regNumber.current.value,
			ATA_Code: this.ataCode.current.value,
			Other: this.other.current.value,
			PIN_Number: this.pinNumber.current.value,
			Serial_Number: this.serialNumber.current.value,
			Component_Name: this.componentName.current.value,
			Component_Position: this.componentPosition.current.value,
			Job_Number: this.jobNumber.current.value,
			Work_Card_Number: this.workCardNumber.current.value,
			List_Technical_Reference: this.listTechnicalReference.current.value,
			Summary: this.summary.current.value,
			Items_Contributed_To_Event: this.itemContributedToEvent.current.value			
		}

	}

	getRadioValue = (ref) => {
		let ret = Array.from(ref.current.children).find((f,i) => {
			if(i > 0){
				return f.children[0].children[0].checked
			}
		})
		return ret && ret.textContent
	}



    render(){
		return (
			<div className="container pb-5 pt-2 my-5 shadow" style={{width: '100%', maxWidth: '930px', margin: 'auto', backgroundColor: 'rgb(255, 255, 255)', borderRadius: '10px'}}>
				<div className="ui blue ribbon massive label">
					<i className="lock icon"></i>CONFIDENTIAL
				</div>
				<div className="ui image">
					
					<div className="img-wrapper my-2" style={{height: '20px', width: '50%'}}>
			
					<img 
						src="https://surveymonkey-assets.s3.amazonaws.com/survey/118635457/a5126dfc-0576-41ac-823b-9d17946653a9.jpg" 
						alt=""
						height="80%"
						width="auto"
					/>
					</div>
				</div>
		
				{/* <!-- Title --> */}
				<div className="container">
					<div className="ui section divider mt-5"></div>
				</div>

				<div className="text-center">
				<h1></h1>
				<p className="text-left mb-5" style={{color: '#797979'}}>
					Note: There is a 24 hour limitation on submitting this report. Refer to the ATS Employee Policy Manual, “Aviation Safety Action Program (ASAP) Description”, QA-02 for more information.
				</p>
				</div>

				<div className="ui equal width form success">
		
				{/* <!-- PERSONAL INFORMATION --> */}
				<div className="ui inverted blue segment text-center">
					<h4 className="ui header">Personal Information</h4>
				</div>
				{/* <!-- Row 1 --> */}
				<div className="fields">
				<div className="ten wide field">
					<label>Name</label>
					<input ref={this.fullName} type="text" placeholder="Full Name" />
				</div>
				<div className="six wide field">
					<label>Employee Number</label>
					<input ref={this.employeeNumber} type="number" placeholder="Numbers only" />
				</div>
				</div>
		
				{/* <!-- Row 2 --> */}
				<div className="field">
					<label>Address (Home)</label>
					<input ref={this.address} type="text" placeholder="Full Address" />
				</div>
		
				{/* <!-- Row 3 --> */}
				<div className="three fields">
				<div className="field">
					<label>City</label>
					<input ref={this.city} type="text" placeholder="City" />
				</div>
				<div className="field">
					<label>State</label>
					<input ref={this.state} type="text" placeholder="State" />
				</div>
				<div className="field">
					<label>Zip Code</label>
					<input ref={this.zipCode} type="text" placeholder="Zip Code" />
				</div>
				</div>
		
				{/* <!-- Row 4 --> */}
				<div className="fields">
				<div className="five wide field">
					<label>Home/Cell Number</label>
					<input ref={this.homeNumber} type="text" placeholder="Home/Mobile number" />
				</div>
				<div className="two wide field">
					<label>Best time to call</label>
					<input ref={this.homeNumberBestTimeToCall} type="time" placeholder="Time" />
				</div>
				</div>
		
				{/* <!-- Row 5 --> */}
				<div className="fields">
				<div className="five wide field">
					<label>Work Number</label>
					<input ref={this.workNumber} type="text" placeholder="Work/Office number" />
				</div>
				<div className="two wide field">
					<label>Best time to call</label>
					<input ref={this.workNumberBestTimeToCall} type="time" placeholder="Time" />
				</div>
				</div>
			{/* <!-- END OF PERSONAL INFORMATION --> */}
		
			
			{/* <!-- WORK HISTORY --> */}
				<div className="ui inverted blue segment text-center">
				<h4 className="ui header">Work History</h4>
				</div>
				{/* <!-- Row 1 --> */}
				<div className="fields">
					<div className="field">
					<label>Date of Occurance</label>
					<input ref={this.dateOfOccurance} type="date" placeholder="mm/dd/yyyy" />
					</div>
					<div className="field">
					<label>Time of Event/Occurance(24 hr. local)</label>
					<input ref={this.timeOfEvent} type="text" placeholder="hh:mm" />
					</div>
					<div className="field">
					<label>Position/Title</label>
					<input ref={this.positionTitle} type="text" placeholder="Position/Title" />
					</div>
				</div>
			
				{/* <!-- Row 2 --> */}
				<div className="fields">
					<div className="field">
					<label>Station</label>
					<input ref={this.station} type="text" placeholder="station" />
					</div>
					<div className="field">
					<label>Location</label>
					<input ref={this.location} type="text" placeholder="location" />
					</div>
					<div className="field">
					<label htmlFor="" className="text-center">Work Shift</label>
					<div ref={this.workShift} className="inline fields pt-2">
						<label htmlFor="shift"></label>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="shift" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Days">Days</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="shift" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">Evenings</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="shift" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Graveyard">Graveyard</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
								<input type="radio" name="shift" checked="" tabindex="0" className="hidden" />
								<label htmlFor="other">other</label>
							</div>
						</div>
					</div>
					</div>
				</div>
		
				{/* <!-- Row 3 --> */}
				<div className="field">
					<label htmlFor="">Consecutive days worked prior to event:</label>
					<input ref={this.consecutiveDaysWorked} type="text" placeholder="day1, day2, day3" />
				</div>
				
			
				{/* <!-- Row 4 --> */}
				<div className="fields">
					<div className="eight wide field">
					<label htmlFor="">Did event occur on overtime? </label>
					<div ref={this.didEventOccurOvertime} className="inline fields pt-2">
		
						<label htmlFor="overtime"></label>
						<div className="five wide field">
						<div className="ui radio checkbox">
							<input type="radio" name="overtime" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Days">Yes</label>
						</div>
						</div>
						<div className="field">
						<div className="ui radio checkbox">
							<input type="radio" name="overtime" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">No</label>
						</div>
						</div>
					</div>
					</div>
					<div className="eight wide field">
					<div className="field">
						<label htmlFor=""># of hours worked at time of event</label>
						<input ref={this.numberOfHoursWorkedEvent} type="number" placeholder="No. of hours" />
					</div>
					</div>
				</div>
				{/* <!-- END OF WORK HISTORY --> */}
		
			
				{/* <!-- Aircraft Information --> */}
				<div className="ui inverted blue segment text-center">
					<h4 className="ui header">Aircraft Information (as applicable)</h4>
				</div>
					{/* <!-- Row 1 --> */}
					<div className="fields">
					<div className="six wide field">
						<label>Type of aircraft</label>
						<input ref={this.typeOfAircraft} type="text" placeholder="Type of aircraft" />
					</div>
					<div className="six wide field">
						<label>Reg #</label>
						<input ref={this.regNumber} type="text" placeholder="Reg #" />
					</div>
					<div className="four wide field">
						<label>ATA Code</label>
						<input ref={this.ataCode} type="text" placeholder="ATA Code" />
					</div>
					</div>
			
			
					{/* <!-- Row 2 --> */}
					<div className="fields">
					<div className="field">
						<label>Other</label>
						<input ref={this.other} type="text" placeholder="Other" />
					</div>
					<div className="field">
						<label>P/N</label>
						<input ref={this.pinNumber} type="text" placeholder="P/N" />
					</div>
					<div className="field">
						<label>S/N</label>
						<input ref={this.serialNumber} type="text" placeholder="S/N" />
					</div>
					</div>
			
					{/* <!-- Row 3 --> */}
					<div className="fields">
					<div className="field">
						<label>Compenent Name</label>
						<input ref={this.componentName} type="text" placeholder="Component Name" />
					</div>
					<div className="field">
						<label>Compenent Position</label>
						<input ref={this.componentPosition} type="text" placeholder="Component Position" />
					</div>
					</div>
				{/* <!-- END OF AIRCRAFT INFORMATION --> */}
					
		
				{/* <!-- DOCUMENTS --> */}
					<div className="ui inverted blue segment text-center">
					<h4 className="ui header">Documents</h4>
					</div>
		
					{/* <!-- Row 1 --> */}
					<div className="fields">
						<div className="ten wide field">
						<label>Job #</label>
						<input ref={this.jobNumber} type="text" placeholder="job #" />
						</div>
						<div className="six wide field">
						<label>Work Card #</label>
						<input ref={this.workCardNumber} type="text" placeholder="Work card #" />
						</div>
					</div>
		
					{/* <!-- Row 2 --> */}
					<div className="field">
						<label htmlFor="reference">List Technical Reference</label>
						<textarea ref={this.listTechnicalReference} type="text" name="reference" placeholder="List reference"></textarea>
					</div>
					
				{/* <!-- END OF DOCUMENTS --> */}
						
		
		
				{/* <!-- SUMMARY --> */}
				<div className="ui inverted blue segment text-center">
					<h4 className="ui header">Summary of Event</h4>
				</div>
		
					{/* <!-- Row 1 --> */}
					<div className="field">
					<label htmlFor="">Summary</label>
					<textarea ref={this.summary} type="text" placeholder="Event Summary"></textarea>
					</div>
				{/* <!-- END OF SUMMARY --> */}
		
				{/* <!-- Contributing Factors --> */}
				<div className="ui inverted blue segment text-center">
					<h4 className="ui header">Contributing Factors</h4>
				</div>
		
					{/* <!-- Row 1 --> */}
					<div className="fields">
					<div className="field">
						<label htmlFor="">Please identify any item(s) that contributed to the event.</label>
						<textarea ref={this.itemContributedToEvent} type="text" placeholder="" rows="2"></textarea>
					</div>
					</div>
		
					{/* <!-- Row 2 --> */}
					<div className="inline field shadow pb-1">
					<label htmlFor="Tools" className="text-center">Equipment / Tools</label>
					<div ref={this.equipmentTools	} className="inline fields pt-2 mb-4">
						<label htmlFor="shift"></label>
						<div className="field">
						<div className="ui radio checkbox">
							<input type="radio" name="Tools" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Days">Unsafe</label>
						</div>
						</div>
						<div className="field">
						<div className="ui radio checkbox">
							<input type="radio" name="Tools" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">Unavailable</label>
						</div>
						</div>
						<div className="field">
						<div className="ui radio checkbox">
							<input type="radio" name="Tools" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Graveyard">No INstructions</label>
							</div>
						</div>
						<div className="field">
						<div className="ui radio checkbox">
							<input type="radio" name="Tools" checked="" tabindex="0" className="hidden" />
							<label htmlFor="other">Not Used</label>
							</div>
						</div>
					</div>
					</div> 

					{/* <!-- Row 3 --> */}
					<div className="inline field shadow pb-1">
					<label htmlFor="aircraft" className="text-center">Aircraft Design / Configuration / Parts</label>
					<div ref={this.aircraftDesign} className="inline fields pt-2 mb-4">
						<label htmlFor="aircraft"></label>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="aircraft" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Days">Parts unavailable</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="aircraft" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">A/C configuration variability</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="aircraft" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Graveyard">Easy to install incorrectly</label>
							</div>
						</div>
					</div>
					</div>  
		
		
					{/* <!-- Row 4 --> */}
					<div className="inline field shadow pb-1">
					<label htmlFor="technical" className="text-center">Technical Data (Work Cards, AMM, IPC, etc.)</label>
					<div ref={this.technicalData} className="inline fields pt-2 mb-4">
						<label htmlFor="shift"></label>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="technical" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Days">Unavailable/Inaccessible</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="technical" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">Incorrect </label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="technical" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Graveyard">Not used</label>
							</div>
						</div>
					</div>
					</div> 
		
					{/* <!-- Row 5 --> */}
					<div className="inline field shadow pb-1">
					<label htmlFor="environment" className="text-center">Environment</label>
					<div ref={this.environment} className="inline fields pt-2 mb-4">
						<label htmlFor="shift"></label>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="environment" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Days">Weather conditions</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="environment" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">High noise levels</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="environment" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Graveyard">Cleanliness </label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
								<input type="radio" name="environment" checked="" tabindex="0" className="hidden" />
								<label htmlFor="other">Lighting </label>
							</div>
						</div>
					</div>
					</div> 
		
					{/* <!-- Row 6 --> */}
					<div className="inline field shadow pb-1">
					<label htmlFor="facilities" className="text-center">Facilities</label>
					<div ref={this.facilities} className="inline fields pt-2 mb-4">
						<label htmlFor="shift"></label>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="facilities" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Days">Hazardous/Toxic substances</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="facilities" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">Power sources</label>
							</div>
						</div>
					</div>
					</div> 
					
					{/* <!-- Row 7 --> */}
					<div className="inline field shadow pb-1">
					<label htmlFor="communication" className="text-center">Communication</label>
					<div ref={this.communication} className="inline fields pt-2 mb-4">
						<label htmlFor="shift"></label>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="communication" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Days">Between mechanics</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="communication" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">from flight crews</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="communication" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">from supervision</label>
							</div>
						</div>
					</div>
					</div>
		
					{/* <!-- Row 8 --> */}
					<div className="inline field shadow pb-1">
					<label htmlFor="individual" className="text-center">Individual Factors</label>
					<div  ref={this.individualFactors} className="inline fields pt-2 mb-4">
						<label htmlFor="shift"></label>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="individual" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Days">Time constraints</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="individual" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Evenings">Fatigue</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
							<input type="radio" name="individual" checked="" tabindex="0" className="hidden" />
							<label htmlFor="Graveyard">Personal Event</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
								<input type="radio" name="individual" checked="" tabindex="0" className="hidden" />
								<label htmlFor="other">Distraction</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
								<input type="radio" name="individual" checked="" tabindex="0" className="hidden" />
								<label htmlFor="other">Work interruption</label>
							</div>
						</div>
					</div>
					</div>
					{/* <!-- END OF SUMMARY --> */}
		
					{/* <!-- ADDITIONAL COMMENTS, OR RECOMMENDATIONS --> */}
					<div className="field">
					<label htmlFor="additional-comments">Additional Comments, or Recommendations:</label>
					<textarea name="additional-comments" id="" cols="30" rows="4"></textarea>
					</div>
					{/* <!-- END OF ADDITIONAL COMMENTS, OR RECOMMENDATIONS --> */}
		
					{/* <!-- SIGNATURE AND DATE --> */}
					<div className="fields my-5">
					<div className="field">
						<label htmlFor="singature">Signature</label>
						<input type="text" name="signature" placeholder="Enter full name" />
					</div>
					<div className="field">
						<label htmlFor="date">Date</label>
						<input type="date" name="date" />
					</div>
					</div>
		
					<div className="field text-center my-5">
					<button className="ui button large blue" type="submit" onClick={this.handleSubmit}>Submit</button>
					</div>
					
		
					<div className="ui section divider"></div>
					{/* <!-- END OF SIGNATURE AND DATE --> */}
				
				{/* <!-- FOR ASAP USE ONLY --> */}
				<div className="ui inverted segment text-center">
				<h4 className="ui header">For ASAP Office Use Only</h4>
				</div>
		
				<div className="container">
				<div className="ui inverted segment">
					<div className="fields">
					<div className="field">
						<label htmlFor="">ERC #</label>
						<input type="text" name="" disabled />
					</div>
					<div className="field">
						<label htmlFor="">RECEIPT DATE</label>
						<input type="text" name="" disabled />
					</div>
					<div className="field">
						<label htmlFor="">TIME</label>
						<input type="text" name="" disabled />
					</div>
					<div className="field">
						<label htmlFor="">ARMS #</label>
						<input type="text" name="" disabled />
					</div>
					</div>
					<div className="field">
					<div className="ui inverted red segment text-center">
						Warning this document is protected under 14CFR Part 193 Disclosure Act, 49 U.S.C. section 4012
					</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		)
    }
}