import React,{Component} from 'react'

export default class NearMissReporting extends Component {
	constructor(props){
        super(props)
        this.state = {
            fields: [],
            refs: {},
            posted: false,
            error: false,
            errorText: null,
            submitted: false,
        }
        this.url = window.location.href.split('/Pages/')[0]
        this.options = {
			headers: {"accept": "application/json; odata=verbose"},
			credentials: 'include'
        }
        this.dateRef = React.createRef();
    }

    componentDidMount(){
        fetch(				
			this.url + "/_api/web/lists/GetByTitle('Near Miss Reporting')/fields?$filter=Hidden eq false and CanBeDeleted eq true&$select=Title,FieldTypeKind,Choices,StaticName,Description", 
			this.options              
		).then( res => {
			res.json().then(json => {
				this.setState({
                    fields: json.d.results
                })                
        
                const fields = this.state.fields
                fields.length && fields.forEach(f => {
                    this.setState(state => {
                        return {
                            refs: Object.assign(state.refs, {}, {
                                [f.StaticName] : React.createRef()
                            })
                        }
                    })
                })
			})
        })
    }

    simpleHeader = (text) => <div className="ui inverted blue segment text-center"><h4 className="ui header">{text}</h4></div>
    simpleField = (fields,className, required) => <div className={className||'fields'}>{fields.map(i=>this.createInput(...i, required))}</div>
    getCol = (column,staticName) => this.state.fields.find(f => f.StaticName === staticName)[column]

    handleSubmit = (e) => {
		e.preventDefault()
        e.stopPropagation()
        let {
            refs
        } = this.state
        let data = {
            __metadata: { 'type': 'SP.Data.Near_x0020_Miss_x0020_ReportingListItem'},
            Title: 'Near Miss Reporting'
        }
        for(var n in refs ){
            let colType = this.getCol('FieldTypeKind',n)
            if(colType === 2 || colType === 3 || colType === 4 || colType === 9){
                let value = refs[n].current.value
                if(value) {
                    data[n] = value
                }
                else {
                    if (refs[n].current.required) {
                        alert(`${refs[n].current.id} is required`);
                        return;
                    }
                }
            }
        }
        let options = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                "accept": "application/json; odata=verbose",
                "content-type": "application/json;odata=verbose",
                "X-RequestDigest": document.getElementById('__REQUESTDIGEST').value,
                "IF-MATCH": "*"            
            },
            credentials: 'include'
        }
        fetch(
            this.url + "/_api/web/lists/GetByTitle('Near Miss Reporting')/items",
            options
        ).then(res => {
            if(res.ok){
                this.setState({submitted: true})
                this.setState({
                    posted: true
                })
            } else {
                alert('Error')
            }
        })
    }

    createInput = (staticName, className, type, note, required) => {
        const refs = this.state.refs
        const descr = this.getCol('Description', staticName)
        return(
            <div key={staticName} className={className||'field'}>
                <label htmlFor={staticName}>{descr}</label>
                {
                    !note ? <input ref={refs[staticName]} id={staticName} required={required} type={type || 'text'} style={{backgroundColor: required ? '#00ffff' : 'white'}}/>:
                    <textarea ref={refs[staticName]} 
                        id={staticName}
                        required={required}
                        type={type || 'text'} 
                        name={staticName}
                        style={{backgroundColor: required ? '#00ffff' : 'white'}}>
                    </textarea>
                }
            </div>
        )
    }

    render(){
        let {
            fields
        } = this.state
        const url = window.location.href.split('/Pages/')[0]
        const spinner = <div style={{textAlign: 'center'}}>
            <i className="fa fa-circle-notch fa-spin fa-md"></i>
        </div>
        return (
            <div>
                {
                    this.state.submitted ?
                    <div className="ui container">
                        <div className="column" style={{marginTop: "50px", marginBottom: "300px"}}>
                            <div className="ui segment centered">
                                {
                                    this.state.posted ?
                                    <div className="information-submitted" style={{fontSize: '18px', fontWeight: 'bold'}}>
                                        {`Thank you, the information is being submitted. `} 
                                        <a href={`${url}/Pages/NearMissReporting.aspx`}>
                                            Go back to the form
                                        </a> | <a href={url}>Home</a>
                                    </div>: spinner
                                }
                            </div>
                        </div>
                    </div>:
                    fields.length && 
                    <div className="container pb-5 pt-2 my-5 shadow" style={{width: '100%', maxWidth: '930px', margin: 'auto', backgroundColor: 'rgb(255, 255, 255)', borderRadius: '10px'}}>
                        <div className="ui blue ribbon massive label">
                            NEAR MISS REPORTING FORM
                        </div>
                        <div className="ui image" style={{margin: '2rem 0'}}>                            
                            <div className="img-wrapper my-2" style={{width: '65%'}}>                    
                            <img 
                                src="https://atsmro4.sharepoint.com/sites/ai/_api/siteiconmanager/getsitelogo" 
                                alt=""
                                height="auto"
                                width="30%"
                            />
                            </div>
                        </div>  
                        <div className="field text-center my-5">
                            <button 
                                className="ui button large" 
                                style={{backgroundColor: '#00ffff'}} 
                                type="submit" 
                                onClick={this.handleSubmit}>
                                Click Here to Submit to Health & Safety
                            </button>
					    </div>
                        <br />      

                        <div className="ui equal width form success">	
                            <div className="report-date four wide field">
                                <label for="date">Date</label>
                                <input id={`Date`} required={true} ref={this.state.refs.Date} type="date" placeholder="mm/dd/yyyy" style={{backgroundColor: '#00ffff'}} />
                            </div>
                            {this.createInput('Employee_Name','sixteen wide field','text',false, true)}
                            {this.createInput('Employee_Number','sixteen wide field','text',false, true)}
                            {this.createInput('Maintenance_Line','sixteen wide field','text',false, true)}
                            {this.createInput('Description','field','text',true, true)}
                            {this.createInput('Suggestions_for_Improvement','field','text',true)}
                        </div>

                        <div className="ui" style={{marginTop: '45px'}}>
                            <span style={{fontSize: '18px', fontWeight: 'bold'}}>
                                When form is complete, click on the "Submit" button at the top of the form to send to the Safety Department.
                            </span>
                        </div>
                        <div className="ui" style={{marginTop: '45px'}}>
                            <span style={{fontSize: '18px', fontWeight: 'bold'}}>
                                The Near-Miss reported today is the accident that does not happen tomorrow.
                            </span>
                        </div>

                    </div>
                }
            </div>
        )
    }
}