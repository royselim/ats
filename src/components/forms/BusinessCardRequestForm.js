import React,{Component} from 'react'

export default class BusinessCardRequestForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            fields: [],
            refs: {},
            posted: false,
            error: false,
            errorText: null,
            submitted: false
        }
        this.url = window.location.href.split('/Pages/')[0]
        this.options = {
            headers: {"accept": "application/json; odata=verbose"},
            credentials: 'include'
        }
    }

    componentDidMount(){
        fetch(				
            this.url + "/_api/web/lists/GetByTitle('Business Card Request Form')/fields?$filter=Hidden eq false and CanBeDeleted eq true&$select=Title,FieldTypeKind,Choices,StaticName,Description", 
            this.options              
        ).then( res => {
            res.json().then(json => {
                this.setState({
                    fields: json.d.results
                })                
        
                const fields = this.state.fields
                fields.length && fields.forEach(f => {
                    this.setState(state => {
                        return {
                            refs: Object.assign(state.refs, {}, {
                                [f.StaticName] : React.createRef()
                            })
                        }
                    })
                })
            })
        })
    }

    simpleHeader = (text) => <div className="ui inverted blue segment text-center"><h4 className="ui header">{text}</h4></div>
    simpleField = (fields,className) => <div className={className||'fields'}>{fields.map(i=>this.createInput(...i))}</div>
    getCol = (column,staticName) => this.state.fields.find(f => f.StaticName === staticName)[column]

    handleSubmit = (e) => {
        e.preventDefault()
        e.stopPropagation()
        let {
            refs
        } = this.state
        let data = {
            __metadata: { 'type': 'SP.Data.Business_x0020_Card_x0020_Request_x0020_FormListItem'},
            Title: 'New Business Card request'
        }
        for(var n in refs ){
            let colType = this.getCol('FieldTypeKind',n);
            if((colType === 2 || colType === 3 || colType === 4 || colType === 6 || colType === 9 ) && refs[n].current){
                let value = refs[n].current.value
                if(value) data[n] = value
                else {
                    if (refs[n].current.required) {
                        alert(`${refs[n].current.id} is required`);
                        return;
                    }
                }
            } 
            if (colType === 15 && refs[n].current) {
                let value = refs[n].current.value
                if(value) data[n] = { "results" : value }
            }
        }
        let options = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                "accept": "application/json; odata=verbose",
                "content-type": "application/json;odata=verbose",
                "X-RequestDigest": document.getElementById('__REQUESTDIGEST').value,
                "IF-MATCH": "*"            
            },
            credentials: 'include'
        }

        console.log({options})

        fetch(
            this.url + "/_api/web/lists/GetByTitle('Business Card Request Form')/items",
            options
        ).then(res => {
            console.log({res})
            if(res.ok){
                this.setState({submitted: true})
                this.setState({
                    posted: true
                })
            } else {
                alert('Error')
            }
        })
    }

    createInput = (staticName, className, type, note = false, required = false, disabled = false) => {
        const refs = this.state.refs
        const descr = this.getCol('Description', staticName);
        const allDesc = descr.split('\n');
        return(
            <div key={staticName} className={className||'field'}>
                {allDesc.map((item, ndx) => {
                    return (
                        <span key={`${ndx}`}>
                            {allDesc.length > 1 && (<br />)}
                            {item !== '' && (<label htmlFor={staticName} style={{fontWeight: 'bold'}}>{item}</label>)}
                        </span>
                    )
                })}
                {
                    !note ? <input ref={refs[staticName]} type={type || 'text'} id={staticName} required={required} style={{backgroundColor: required ? '#00ffff' : 'white'}}/>:
                    <textarea ref={refs[staticName]} 
                        type={type || 'text'} 
                        required={required}
                        name={staticName}
                        id={staticName}
                        disabled={disabled}
                        style={{backgroundColor: required ? '#00ffff' : 'white'}}>
                    </textarea>
                }
            </div>
        )
    }

    createSelect = (staticName, className, required = false) => {
        const {refs, fields} = this.state
        const descr = this.getCol('Description', staticName)
        const options = ( fields && fields.length && fields.find(f => f.StaticName === staticName).Choices.results ) || []
        return(
            <div key={staticName} className={className||'field'}>
                <label for={staticName}>{descr}</label>
                <select 
                    ref={refs[staticName]} 
                    name={staticName} 
                    id={staticName} 
                    required={required} 
                    style={{backgroundColor: required ? '#00ffff' : 'white'}}
                >
                    <option value="" disabled selected style={{backgroundColor: 'white'}}></option>
                    {
                        options.length && options.map((l,i) => <option key={i} value={l} style={{backgroundColor: 'white'}}>{l}</option>)
                    }
                </select>
            </div>
        )        
    }

    createRadioButtons = (staticName, className, selectClass) => {
        const refs = this.state.refs
        let choices = this.getCol('Choices',staticName)
        return(
            <div className={className||'field'} style={{borderColor: 'black', borderWidth: '1px', borderRadius: 5}}>
                <label htmlFor={staticName} className="text-left">{this.getCol('Description',staticName)}</label>
                <div ref={refs[staticName]} className={`inline fields pt-2 ${selectClass}`}>
                    {
                        ((choices && choices.results) || ['Yes','No']).map((c,i) => {
                            return (
                                <div key={i} className="field">
                                    <div className="ui radio checkbox ui-radio-checkbox" onClick={
                                        () => {
                                            refs[staticName].current.value = c;
                                        }
                                    }>
                                        <input type="radio" name={staticName} id={staticName} checked="" tabIndex="0" />
                                        <label htmlFor={c}>{c}</label>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }

    createColumnCheckbox = (staticName, className, rowContainer) => {
        const refs = this.state.refs;
        let choices = this.getCol('Choices',staticName)
        choices && choices.results.forEach((val, ndx) => {
            rowContainer[ndx % rowContainer.length].push(val)
        })
        return(
            <div ref={refs[staticName]} id={staticName} name={staticName} className={className||'field'} style={{borderColor: 'black', borderWidth: '1px', borderRadius: 5}}>
                <label htmlFor={staticName} className="text-left" style={{marginBottom: 10}}>{this.getCol('Description',staticName)}</label>
                    {rowContainer.map((v,x) => {
                        return (
                            <div key={`${staticName}${x}`} className={`inline fields`} style={{marginBottom: 0}}>
                                {
                                    ((rowContainer[x]) || ['Yes','No']).map((c,i) => {
                                        return (
                                            <div key={`${staticName}${i}`} className="field">
                                                <div className="ui checkbox ui-checkbox">
                                                    <input 
                                                        
                                                        onChange={(event) => {
                                                            if(event.target.checked) {
                                                                if(refs[staticName].current.value === undefined) {
                                                                    refs[staticName].current.value = [c];
                                                                } else if(!refs[staticName].current.value.includes(c)) {
                                                                    refs[staticName].current.value.push(c)
                                                                } 
                                                            } else {
                                                                if(refs[staticName].current.value === undefined) {
                                                                    // Do nothing
                                                                } else if(refs[staticName].current.value.includes(c)) {
                                                                    refs[staticName].current.value.splice(refs[staticName].current.value.indexOf(c), 1)
                                                                }
                                                            }
                                                        }}
                                                        type="checkbox" 
                                                        name={`${staticName}-${x}-${i}`} 
                                                        id={`${staticName}-${x}-${i}`} 
                                                        tabIndex="0" 
                                                    />
                                                    <label htmlFor={c}>{c}</label>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        )
                    })}
            </div>
        )
    }

    render(){
        let {
            fields,
            refs
        } = this.state;
        console.log({fields})
        const url = window.location.href.split('/Pages/')[0]
        const spinner = <div style={{textAlign: 'center'}}>
            <i className="fa fa-circle-notch fa-spin fa-md"></i>
        </div>

        return (
            <div>
                {
                    this.state.submitted ?
                    <div className="ui container">
                        <div className="column" style={{marginTop: "50px", marginBottom: "300px"}}>
                            <div className="ui segment centered">
                                {
                                    this.state.posted ?
                                    <div className="information-submitted" style={{fontSize: '18px', fontWeight: 'bold'}}>
                                        {`Thank you, the information is being submitted. `} 
                                        <a href={`${url}/Pages/BusinessCardRequestForm.aspx`}>
                                            Go back to the form
                                        </a> | <a href={url}>Home</a>
                                    </div>: spinner
                                }
                            </div>
                        </div>
                    </div>:
                    fields.length && 
                    <div className="container pb-5 pt-2 my-5 shadow" style={{width: '100%', maxWidth: '930px', margin: 'auto', backgroundColor: 'rgb(255, 255, 255)', borderRadius: '10px'}}>
                        <div className="ui blue ribbon massive label">
                            BUSINESS CARD REQUEST FORM
                        </div>
                        <div className="ui image" style={{margin: '2rem 0'}}>
                            <div className="img-wrapper my-2" style={{width: '65%'}}>
                            <img 
                                src="https://atsmro4.sharepoint.com/sites/ai/_api/siteiconmanager/getsitelogo" 
                                alt=""
                                height="auto"
                                width="30%"
                            />
                            </div>
                        </div>
                        <div className="ui" style={{margin: '15px'}}>
                            <span style={{fontSize: '18px', fontWeight: 'bold'}}>
                                BUSINESS CARD REQUEST FORM
                            </span>
                        </div>
                        <br />  
                        <div className="ui equal width form success">	
                            <div class="ui grid" style={{marginBottom: 14}}>
                                {this.createInput('FullName','sixteen wide field','text',false)}
                                {this.createInput('JobTitle','sixteen wide field','text',false)}
                                {this.createInput('Department','sixteen wide field','text',false)}
                                {this.createInput('OfficePhone','sixteen wide field','text',false, true)}
                                {this.createInput('MobilePhone','sixteen wide field','text',false, true)}
                            </div>
                            <div><span><i>* Must include at least one phone number & country code if outside U.S.</i></span></div><br />
                            <div class="ui grid" style={{marginBottom: 14}}> 
                                {this.createInput('EmailAddress','sixteen wide field','text',false, true)}
                                {this.createSelect('UseAddressfor', 'sixteen wide field')}
                                {this.createInput('StreetAddress1','sixteen wide field','text',false)}
                                {this.createInput('StreetAddress2','sixteen wide field','text',false)}
                                {this.createInput('City','sixteen wide field','text',false)}
                                {this.createInput('State','sixteen wide field','text',false, )}
                                {this.createInput('ZipCode','sixteen wide field','text',false, )}
                                {this.createInput('Country','sixteen wide field','text',false, )}
                                {this.createColumnCheckbox('Businessunit', 'sixteen wide field', [[], [], []] )}
                            </div>
                            <hr />
                            <div><span>* Optional: Any additional information required to complete order:</span></div><br />
                            <div>
                                {this.createInput('EmployeeNumber','sixteen wide field','text',false, )}
                                {this.createInput('DeptBudgetCode','sixteen wide field','text',false, )}
                                {this.createInput('DirectSupervisor','sixteen wide field','text',false, )}
                            </div>
                        </div>
                        <div className="field text-center my-5">
                            <button 
                                className="ui button large" 
                                style={{backgroundColor: '#00ffff'}} 
                                type="submit" 
                                onClick={this.handleSubmit}>
                                SUBMIT
                            </button>
                        </div>
                    </div>
                }
            </div>
        )
    }
}