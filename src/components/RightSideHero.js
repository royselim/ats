import React, {Component} from 'react'
import rightSideHeroDate from '../functions/rightSideHeroDate'
import {getSPListItems,getUrl} from './helpers/GetSPListItems'
import Loader from './helpers/Loader';
import {
    Home_main_slider_list_name,
    React_Styles_list_name,
    Right_Side_Hero_Slide_Interval_Time
} from '../constants'

export default class RightSideHero extends Component {
    constructor(props){
        super(props)
        this.state = {
            items: [],
            activeIndex: 0,
            active: null
        }
    }

    changeSlide = () => {
        let keys = [0,1,2]
        let currentIndex = keys.indexOf(this.state.activeIndex)
        keys.push(keys.shift());
        this.setState(state => {
            return {
                activeIndex: keys[currentIndex],
                active: state.items[keys[currentIndex]]
            }
        })
    }

    componentDidMount(){
        const url = getUrl(this.props.context)
        const {config} = this.props
        const list = config.find(f=>f.Title === Home_main_slider_list_name).Value
        const slideInterval = config.find(f=>f.Title === Right_Side_Hero_Slide_Interval_Time).Value
        getSPListItems(url,list,config).then(rslt => {
            if(Array.isArray(rslt)){
                this.setState(state => {
                    return {
                        items: rslt,
                        active: rslt[state.activeIndex]
                    }
                })
                                    
                this.timerID = setInterval(
                    () => this.changeSlide(),
                    (Number(slideInterval) || 5000)
                )
            } else {
                this.setState({response: rslt})
            }
        })

        const styleList = config.find(f=>f.Title === React_Styles_list_name).Value
        getSPListItems(url,styleList,config).then(styles => {
            if(Array.isArray(styles)){
                this.setState({styles})
            } else { /** TODO */}
        })
    }

    handleClick = (index) => {
        this.setState(state => {
            return {
                activeIndex: index,
                active: state.items[index]
            }
        })
    }

    getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
    }
    
    getStyle = (title) => {
        const {styles} = this.state
        const itm = styles && styles.find(f=>f.Title === title)
        let itms = {}
        try{ itms = itm && JSON.parse(itm.Style) }
        catch(e){}
        return itms
    }

    render(){
        let {
            items,
            activeIndex,
            active,
            styles
        } = this.state

        let transStyle = active && {
            backgroundColor: active.Transparency ? 
                (active.Transparency_Background === 'dark'? 
                    'rgba(0,0,0,' + active.Transparency_Value + ')':
                    'rgba(255,255,255,' + active.Transparency_Value + ')')
                :'none'
        }

        const bids = this.getStyle('banner-img-details')
        const rshts = this.getStyle('right-side-hero-title')
        const rshds = this.getStyle('right-side-hero-date')
        const rshxs = this.getStyle('right-side-hero-description')

        const imageUrl = active && this.getImage(active.Image2Id)

        const html = 
            active && styles && <div className="" id="right-side-hero">
                <div className="ui grid">
                    <div className="twelve wide computer sixteen wide tablet mobile column no-padding" style={{
                        backgroundSize: 'cover',
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat',
                        height: "330px",
                        backgroundImage: "url('"+ imageUrl +"')",
                        padding: '0px'
                        }}      
                    >
                        <a href={active.Link.Url}>
                            <div className="banner-img-details" style={active.Transparency ? Object.assign({},transStyle,bids): bids}>
                                <div className="right-side-hero-title">
                                    <h3 className="right-side-hero-title" style={Object.assign({},{color: (active.Transparency && active.Transparency_Background === 'dark') ? 'white': 'rgb(0,0,120)', fontWeight: '400'},rshts)}>
                                        {active.Title}
                                    </h3>
                                </div>
                                <div className="right-side-hero-description">
                                    <span className="right-side-hero-description" style={Object.assign({},{color: (active.Transparency && active.Transparency_Background === 'dark') ? 'white': 'rgb(0,0,150)'},rshxs)}>
                                        {active.Description}
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
            
                    <div className="four wide computer only column no-padding" style={{padding: '0px'}}>                    
                        {
                            items.map( (item, index) => {
                                let isIndex = index === activeIndex
                                return (
                                    <div
                                        className={`banner-tags ${isIndex ? 'active-index': ''}`}
                                        style={{
                                            backgroundColor:  isIndex ? '#4597cb': '#e8e3e7',                                    
                                        }}
                                        key={index} onClick={(e) => this.handleClick(index)}
                                    >
                                        <div className="right-side-hero-tab-title" style={{color: isIndex ? '#fff': '#787878'}}>{item.Title}</div>
                                        <div className="right-side-hero-tab-date" style={{color: isIndex ? '#fff': '#787878'}}>{rightSideHeroDate(item.Date)}</div>
                                        <div id="triangle-left" style={{display: isIndex ? 'block' : 'none'}} ></div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>              
            </div>
        return (
            // <div id="banner-widget" className="ui segment widgets no-border">
            <div id="banner-widget" className="" style={{padding: '16px'}}>
                <div className="banner-wrapper">
                    {html || <Loader />}
                </div>  
            </div>
        )
    }
}