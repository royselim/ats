import React, { Component } from 'react'
import {Header_HOME_link_label} from '../../constants'

export default class HeaderHome extends Component {
    render(){
        const href = window.location.href
        const {config,context} = this.props
        const homeLabel = config.find(i => i.Title === Header_HOME_link_label)
        return (
            <li className={`nav-item ${href === context.siteAbsoluteUrl ? 'active': ''}`}>
                <a href={context.webAbsoluteUrl}>{homeLabel.Value}</a>
            </li>
        )
    }
}