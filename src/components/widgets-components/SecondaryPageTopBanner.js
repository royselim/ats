import React, {Component} from 'react'
import Loader from '../helpers/Loader'
import {Secondary_Page_Plane_icon as plane} from '../../constants'

export default class SecondaryPageTopBanner extends Component{
    getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
    }
    
    render(){
        const {unit,images,context,config} = this.props
        const bannerUrl = unit && this.getImage(unit.Top_Banner_Image2Id)
        const planeImg = config.find(f=>f.Title===plane).Value
        const planeUrl = images && images.find(f=>f.ATS_x0020_Title===planeImg)
        const planeFF = planeUrl && planeUrl.FieldValuesAsText.FileRef
        const planeIcon = planeFF && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,planeFF)

        return (
            <div>
                {
                    unit ?
                    <div className="ten wide column"> 
                        <div className="ui grid"
                            style={{
                                backgroundImage: 'url("'+ bannerUrl +'")',
                                backgroundSize: 'cover',
                                backgroundPosition: 'center center',
                                backgroundRepeat: 'no-repeat',
                                height: '250px',
                                margin: '0',
                                borderRadius: '5px'
                            }}
                        >
                            <div className="ten wide column no-padding" style={{padding: '0px'}}>
                                <div className={unit.Top_Banner_Transparency ? 'secondary-page-banner-img-details secondary-page-transparency': 'secondary-page-banner-img-details'}>
                                    {/* <div className="ui segment no-padding secondary-page-column"> */}
                                    <div className="secondary-page-column">
                                        <div className="ui grid">
                                            <div className="two wide column no-padding" style={{padding: '0px'}}>
                                                <div className="ui segment no-padding"
                                                    style={{
                                                        backgroundImage: 'url("'+planeIcon+'")',
                                                        backgroundSize: 'contain',
                                                        backgroundPosition: 'center center',
                                                        backgroundRepeat: 'no-repeat',
                                                        backgroundColor: 'rgba(0,0,0,0)',
                                                        boxShadow: 'none',
                                                        padding: '0',
                                                        border: '0',
                                                        height: '45px'
                                                    }}
                                                >
                                                </div>                                                            
                                            </div>
                                            <div className="fourteen wide column no-padding secondary-page-column" style={{padding: '0px'}}>
                                                <div className="secondary-page-column secondary-page-banner-title"
                                                    style={{padding: '15px 0px'}}
                                                >
                                                    {unit.Banner_Title}
                                                </div>
                                                <div className="secondary-page-column secondary-page-banner-subtitle" style={{padding: '0px'}}>
                                                    {unit.Banner_Subtitle}
                                                </div>
                                                <div className="secondary-page-column secondary-page-banner-details" style={{padding: '0px'}}>
                                                    {unit.Banner_Details}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>	
                        </div>
                    </div>
                    :
                    <div>
                        <Loader height={'250px'} text='THIS PAGE DOES NOT EXIST.' />
                    </div>
                }
            </div>
        )
    }
}