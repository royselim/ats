import React, {Component} from 'react'

export default class Article extends Component{
    createMarkup(richText){
        return {__html: richText}
    }
    
    render(){
        let {title,subTitle,content} = this.props
        return (
            <div className="column">
                <div className="tertiary-page-article-container">
                    <h2 className="tertiary-page-article-title">{title.toUpperCase()}</h2> 
                    <span className="tertiary-page-article-subtitle">{subTitle}</span> 
                </div>​​
                <div dangerouslySetInnerHTML={this.createMarkup(content)}
                    className="tertiary-page-article-content"
                />
            </div>
        )
    }
}