import React, { Component } from 'react'
import ReportAnEmergency from './ReportAnEmergency'

export default class MiniWidget extends Component {
	getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
	}

	openModal = (e) => {
		const eId = e.target.id
		if( eId === 'report_an_emergency'){
			const modal = document.getElementById('alert-media-modal')
			modal.style.display = "block";
		}
	}

	closeModal = (e) => {
		const modal = document.getElementById('alert-media-modal')
		modal.style.display = 'none'
	}

	render() {
		const {item,images,modalText,config,context} = this.props
		const imageUrl = item && this.getImage(item.Banner2Id)

		return (
			<div className="column" style={{padding: '15px 15px 0px 0px'}}>
				{
					images && <a onClick={this.openModal} href={item.Link && item.Link.Url} target={item.Link_Opens === 'new tab'? '_blank': '_self'}>
						<img 
							id={item.Title.replace(/\s/g,'_').toLowerCase()} 
							className="mini-widget-boxes"
							src={imageUrl} 
							alt="" 
							width="100%" 
							height="140" 
							style={{cursor: 'pointer'}}
						/>
					</a>
				}
				<div id="alert-media-modal" className="alert-media-modal">
					<div className="alert-media-modal-content">
						<span className="alert-media-close" onClick={this.closeModal}>&times;</span>
						<ReportAnEmergency config={config} images={images} context={context} modalText={modalText}/>
					</div>
				</div>
			</div>
		)
	}
}