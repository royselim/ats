import React, {Component} from 'react'
import {getSPResource} from '../../components/helpers/GetSPResource';

export default class MaterialNavigation extends Component{
    constructor(props){
        super(props);
        this.state = {
            visible: false,
        }
    }

    componentDidMount(){
        const {Check_List_Access} = this.props.fields;
        const {userId, url} = this.props;
        let listRoles = [];
        if(Check_List_Access) {
            fetch(url + "/_api/web/lists/GetByTitle('"+
            Check_List_Access+"')?$select=RoleAssignments&$expand=RoleAssignments")
            .then((res) => {
                if(res.ok){
                    res.json().then(data => {
                        const {results} = data.d.RoleAssignments;
                        results.forEach((result) => {
                            listRoles.push(result.PrincipalId);
                            if(result.PrincipalId === userId) {
                                this.setState({visible: true})
                            }
                        });

                        getSPResource(url + 
                        "/_api/web/currentuser/groups").then(data2 => {
                            if(data2 && Array.isArray(data2.value)){
                                data2.value.forEach(group => {
                                    if(listRoles.includes(group.Id)){
                                        this.setState({visible: true})
                                    }
                                })
                            }
                        }).catch((err) => {console.dir(err)});
                    }).catch((error) => {console.dir(error)})
                } else {
                    /** TODO */
                }
            }).catch((error) => {console.dir(error)})
        }
    }

    render(){ 
        const {getImage} = this.props;
        const {ImageId, Title, Subtitle, Description, Link, Check_List_Access} = this.props.fields;
        const {visible} = this.state


        return visible || Check_List_Access === null ? <a href={Link || ''}><div className="ui stackable grid" 
            style={{
                borderRadius: 5, 
                border: '1px solid #b1b1f1',
                height: '140px',
                overflow: 'hidden',
                margin: '0px -15px 15px -15px'
            }}>
            <div className="four wide column">
                <div style={{
                    backgroundImage: 'url("'+ getImage(ImageId) +'")',
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    width: '100%',
                    height: '100%'
                }}>
                </div>
            </div>
            <div className="twelve wide column">
                <div style={{fontSize: 18, color: '#2b2b5d', fontWeight: 500, marginBottom: 10}}>{Title.toUpperCase()}</div>
                <div style={{fontSize: 13, color: 'blue', marginBottom: 10}}>{Subtitle}</div>
                <div style={{fontSize: 13, maxHeight: 40, overflow: 'hidden', color: 'black'}}>{Description}</div>
            </div>
        </div></a>: <div />
    }
}