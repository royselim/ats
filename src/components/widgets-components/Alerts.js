import React, { Component } from 'react'
import AltAlertDetails from '../alternates/AltAlertDetails';
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {
	Alerts_list_name,
	Icons_list_name,Alerts_label,
	Alerts_Slide_Interval_Time	
} from '../../constants'

class Alerts extends Component {
	constructor(props){
		super(props)
		this.state = {
			alerts: null,
			icons: null,
			static_texts: null
		}
	}

	componentDidMount(){
		const {config,context} = this.props
		const clists = [Alerts_list_name,Icons_list_name]
		const lists = config.filter(f=>clists.includes(f.Title)).map(m=>m.Value)
		const url = getUrl(context)
		lists.forEach(l => {
			getSPListItems(url, l,config).then(rslt => {
				if(Array.isArray(rslt)){
					this.setState({
						[l.replace(/\s/g,'_').toLowerCase()]: rslt
					})
				} else {/** TODO */}
			})
		})
	}

	render() {
		const {config,images,context} = this.props
		const state = this.state
		const alerts = state[config.find(f=>f.Title===Alerts_list_name).Value.replace(/\s/g,'_').toLowerCase()]
		const icons = state[config.find(f=>f.Title===Icons_list_name).Value.replace(/\s/g,'_').toLowerCase()]
		const timeInterval = Number(config.find(f=>f.Title===Alerts_Slide_Interval_Time).Value);
		let alertText = config.find(t => t.Title === Alerts_label).Value
		return (
			<div style={{paddingTop: '15px'}}>
				{
					alerts && alerts.length && icons ?
					<AltAlertDetails 
						alertItems={alerts} 
						icons={icons} 
						alertText={alertText} 
						images={images}
						context={context}
						timeInterval={timeInterval}
					/>:
					<div></div>
				}
			</div> 
		)
	}
}

export default Alerts
