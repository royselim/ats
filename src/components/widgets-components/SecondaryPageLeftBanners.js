import React, {Component} from 'react'
import Banner from './Banner'

export default class SecondaryPageLeftBanners extends Component{
    render(){
        const {unit,config,context,images} = this.props
        let bannerItems = this.props.bannerItems

        return(
            <div>
                {
                    [1,2,3].map((i,x)=>unit[`Left_Banner${i}Id`] && 
                    <div key={x} className="no-padding no-box-shadow" style={{border: 'unset'}}>
                        <Banner 
                            items={bannerItems} 
                            id={unit[`Left_Banner${i}Id`]} 
                            bannerHeight="150px"
                            config={config}
                            images={images}
                            context={context}
                        />
                    </div>)
                }
            </div>
        )
    }
}