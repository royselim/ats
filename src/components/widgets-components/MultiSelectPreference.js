import React,{Component} from 'react'

export default class MultiSelectPreference extends Component {
    constructor(props){
        super(props)
        this.state = {
            posted: false,
            item: {}
        }
    }

    componentDidUpdate(){
        this.props.submitted && this.submit()
    }    

    static getDerivedStateFromProps(props, state){
        return {
            item: (Object.keys(state.item).length !== 0 && state.item) || props.item || {}
        }
    }

    handleChange = (StaticName) => {
        this.setState((state) => {
            return {
                item: Object.assign(state.item, {}, {
                    [StaticName]: state.item[StaticName] === undefined ? false: !state.item[StaticName]
                })
            }
        })
    }

    submit = () => {
        let {title,email,reset,post} = this.props
        let {item} = this.state
        
        reset()

        let data = Object.assign(item, {}, {
            __metadata: { 'type': 'SP.Data.'+ title.replace(/\s/g, '_x0020_') +'ListItem'},
            Title: email
        })

        let options = {
            method: item.Id ? 'MERGE': 'POST',
            body: JSON.stringify(data),
            headers: {
                "accept": "application/json; odata=verbose",
                "content-type": "application/json;odata=verbose",
                "X-RequestDigest": document.getElementById('__REQUESTDIGEST').value,
                "IF-MATCH": "*"            
            },
            credentials: 'include'
        }

        let url = window.location.href.split('/Pages/')[0]

        fetch(
            url + "/_api/web/lists/GetByTitle('" + title + "')/" + 
                ( item.Id ? ("GetItemById("+ item.Id +")") : "items" ),
            options
        ).then(res => {
            post(title)
            if(res.ok){
                // window.location.assign(window.location.href)
            } else {
                alert('Error')
            }
        })
    }

    render(){
        let {title} = this.props
        let {item} = this.state
        return(
            <div className="ui form my-5">
                <h2 className="ui header">{title}</h2>                
                <div className="">
                    {
                        this.props.fields.map((f,x) => <div key={x}>
                            <div className="ui checkbox">
                            <input
                                type="checkbox" 
                                name={f.Title} 
                                value={f.Title} 
                                checked={item[f.StaticName] === undefined ? true: item[f.StaticName]}
                                onChange={() => this.handleChange(f.StaticName)}
                            />
                            <label htmlFor="">{` ${f.Title}`}</label>
                            </div>
                        </div>)
                    }
                </div>                
            </div>
        )
    }
}