import React, { Component } from 'react'
import {About_Your_Work_Link_label,About_Your_Work_URL} from '../../constants'

export default class AboutYourWork extends Component {
    render(){
        const {config,context,active} = this.props
        const label = config.find(i => i.Title === About_Your_Work_Link_label)
        const link = config.find(f=>f.Title === About_Your_Work_URL).Value
        return (
            <li className={`nav-item ${active === label.Value ? 'active': ''}`}>
                <a href={`${context.webAbsoluteUrl}${link}`}>{label.Value}</a>
            </li>
        )
    }
}