import React, { Component } from 'react'

class EventsCalendarDetails extends Component {
	createMarkup(richText){
        return {__html: richText}
	}
	
	render() {
		let item = this.props.item;
		var dt = new Date(item.EventDate);

		// var dtArr = dt && dt.toLocaleString().split(' ');
		dt.setHours(dt.getHours() + (item.fAllDayEvent ? dt.getTimezoneOffset()/60 : 0));

		var dtArr = dt && dt.toLocaleString().split(' ');
		var timeArr = dtArr[1].split(':');
		var timePart = `${timeArr[0]}:${timeArr[1]} ${dtArr[2]}`;
		return (
			<div className="ui widgets two column grid events-list">
				<div className="four wide column mb-2" style={{paddingBottom: '0px'}}>
					<strong>
						<div>{dtArr[0].replace(',','')}</div>
						{
							!item.fAllDayEvent && 
							<div>{timePart}</div>
						}
					</strong>
				</div>

				<div className="twelve wide column" style={{paddingBottom: '0px'}}>
					<div className="">
						<div className="content">
							<strong className="event-title">
								<a href={item.Link && item.Link.Url}>
									{item.Title.toUpperCase()}
								</a>
							</strong>
							<div className="description paragraph">
								<div dangerouslySetInnerHTML={this.createMarkup(item.Description)}
									className="tertiary-page-article-content"
								/>
							</div>							
						</div>
					</div>        
				</div>
			</div>
		)
	}
}

export default EventsCalendarDetails
