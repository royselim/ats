import React, {Component} from 'react'
import {
    Report_An_Emergency_image as image,
    Report_An_Emergency_link as link
} from '../../constants'

export default class ReportAnEmergency extends Component{
    getImage = (atsTitle) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.ATS_x0020_Title === atsTitle)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
    }
    
    render(){
        const {config,modalText,context} = this.props
        const popupImage = config.find(f=>f.Title === image).Value
        const img = popupImage && this.getImage(popupImage)
        const lnk = config.find(f=>f.Title === link).Value
                
        return(
            <div
                style={{
                    backgroundImage: img && `url("${img}")`,
                    backgroundSize: 'cover',
                    backgroundPosition: 'center center',
                    backgroundRepeat: 'no-repeat',
                    borderRadius: '5px'
                }}
            >
                <div style={{
                    backgroundColor: 'rgba(255,255,255,0.75)',
                    borderRadius: '5px',
                    height: 'inherit',
                    padding: '20px',
                }}>
                    <div style={{
                        fontWeight: 'bold',
                        fontSize: 'large'
                    }}>
                        <center>{modalText}</center>
                    </div>
                    {
                        context.isSiteAdmin && <div>    
                            <center><a href={lnk}>edit</a></center>
                        </div>
                    }
                </div>
            </div>
        )
    }
}