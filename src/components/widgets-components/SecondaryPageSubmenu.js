import React, {Component} from 'react'
import {Secondary_Page_Left_Submenu_icon as title} from '../../constants'

export default class SecondaryPageSubmenu extends Component {
    constructor(props){
        super(props)
        this.state = {
            groupExpanded: true
        }
    }
    handleToggleGroup = () => {
        this.setState((state)=>{
            return {
                groupExpanded: !state.groupExpanded
            }
        })
    }

    render(){
        let {
            groupExpanded
        } = this.state
        let arrowDir = groupExpanded ? 'up' : 'down'
        let {
            content,
            handleChangeContent,
            group,
            contentProps,
            images,
            context,
            config
        } = this.props

        const groupMenus = Object.keys(contentProps).map(k => k.replace(/_/g, ' ').toUpperCase())

        const iconVal = config.find(f=>f.Title === title).Value
        const icon = images && images.find(f=>f.ATS_x0020_Title === iconVal)
        const iconFF = icon && icon.FieldValuesAsText.FileRef
        const iconUrl = iconFF && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,iconFF)

        const planeFly = <div style={{
            backgroundImage: 'url("'+iconUrl+'")',
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center center',
            height: '15px',
            width: '20px',
            display: 'inline-block'
        }} />
        const empty = <div style={{
            height: '15px',
            width: '20px',
            display: 'inline-block'
        }} />

        return(
            <div className="ui grid">
                <div className="one wide column no-right-padding"></div>
                <div className="fifteen wide column submenu-group">
                    {
                        group && 
                        <div onClick={this.handleToggleGroup} className="secondary-page-submenu bold black">
                        {group.toUpperCase()}
                        {' '}
                        <i className={`chevron ${arrowDir} icon`} />
                    </div>
                    }
                    <div>
                        {
                            groupExpanded ?
                            <div className="submenu-group">
                                {
                                    groupMenus && groupMenus.map((g,x) => {
                                        let h = g.replace(/\s/g, '-')
                                        let addClass = g === content ? 'black': 'sub-blue'
                                        return <div className="padding-bottom-5">
                                            {g === content ? planeFly: empty}
                                            <div key={x}
                                                style={{display: 'inline'}}
                                                className={`submenu-${h} secondary-page-submenu ${addClass}`} 
                                                onClick={() => handleChangeContent(g)}
                                            >                                                
                                                {g.toUpperCase()}
                                            </div>
                                        </div>
                                    })
                                }                              
                            </div>
                            :
                            null
                        }
                    </div >
                    {/* <div style={{padding: '0px 0px 0px 20px'}}>
                        {
                            links.map((l,x) => {
                                let m = l.Title.toLowerCase().replace(/\s/g, '-')
                                return <a key={x} href={l.Link.Url}>
                                    <div className={`${m} secondary-page-submenu sub-blue`}>
                                        {l.Title.toUpperCase()}
                                    </div>
                                </a>
                            })
                        }
                    </div> */}
                </div>
            </div>
        )
    }
}