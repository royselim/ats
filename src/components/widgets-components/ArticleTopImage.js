import React, {Component} from 'react'

export default class ArticleTopImage extends Component{
    render(){
        return (
            <div className="ui segment"
                style={{
                    backgroundImage: 'url("' + this.props.url + '")',
                    backgroundSize: 'cover',
                    backgroundPosition: 'center center',
                    backgroundRepeat: 'no-repeat',
                    height: this.props.height || '315px'
                }}
            />
        )
    }
}