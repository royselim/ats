import React, { Component } from 'react'
import DiversityCalendarDetails from './DiversityCalendarDetails';
import {getSPListItems} from '../helpers/GetSPListItems'
import {
	Diversity_Calendar_URL,
	Diversity_Calendar_Site,
	Diversity_Calendar_Title,
	Diversity_Calendar_label,
	Diversity_Calendar_Number_Of_Items
} from '../../constants'

class DiversityCalendar extends Component {
	constructor(props){
		super(props)
		this.state = {}
	}

	componentDidMount(){
		const {config} = this.props
		const url = config.find(c=>c.Title===Diversity_Calendar_Site).Value
		const list = config.find(c=>c.Title===Diversity_Calendar_Title).Value
		const items = config.find(c=>c.Title===Diversity_Calendar_Number_Of_Items).Value
		const date = new Date();
		const queryDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
		getSPListItems(url, list,config, '$orderby=EventDate asc&$filter=EventDate gt \''+ queryDate.toISOString() +'\'&$top='+items).then(rslt => {
            if(Array.isArray(rslt)){
                this.setState({items: rslt})
            } else { /** TODO */ }
        }).catch((error) => {
			// to avoid error during dev/debugging.
		})
	}

	render() {
		let items = this.state.items
		items && items.length && items.sort((a,b) => {
			return new Date(a.EventDate).getTime() - new Date(b.EventDate).getTime();
		})
		const {config} = this.props
		const label = config.find(f=>f.Title===Diversity_Calendar_label).Value
		const url = `${config.find(f=>f.Title===Diversity_Calendar_URL).Value}&gridview=${new Date().toISOString().split('T')[0]}`
		return (
			<div id="events-widget" className="ui segment widgets no-border">
				<div className="" style={{paddingBottom: '20px'}}>
					<h2 style={{display: 'inline'}}>{label}</h2>
					<div className="" style={{display: 'inline', float: 'right', paddingTop: '5px'}}>
						<a href={url}>
							<i className="calendar outline icon" style={{fontSize: 'large'}}></i>
						</a>
					</div>
				</div>
				{
					items && items.map((item,index) => {
						return <DiversityCalendarDetails key={index} item={item}/>
					})
				}
			</div>

		)
	}
}

export default DiversityCalendar
