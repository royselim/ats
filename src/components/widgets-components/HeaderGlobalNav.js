import React, {Component} from 'react'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {Global_Navigation_list_name as title} from '../../constants'

export default class HeaderGlobalNav extends Component{
    constructor(props){
        super(props)
        this.state = {}
    }

    componentDidMount(){
        const {config,context} = this.props
        const url = getUrl(context)
        const list = config.find(f=>f.Title === title).Value
        getSPListItems(url, list,config).then(rslt => {
            if(Array.isArray(rslt)){
                this.setState({
					items: rslt
				})
            } else {
                // TODO
            }
        })
    }

    render(){
        let {items} = this.state
        let {active, context, groupIds} = this.props
        const {userId} = context;
        const globalNavigationItems = items && items.filter(i => i.Menu_Level === 1 && i.Users_AllowedId === null)
        const allowedLinks = items && items.filter(i => {
            const level1 = i.Menu_Level === 1
            const userAllowed = i.Users_AllowedId && i.Users_AllowedId.results
            const usersGroups = (userAllowed || []).filter(grp => groupIds.indexOf(grp) !== -1)
            return level1 && userAllowed && (userAllowed.indexOf(userId) !== -1 || usersGroups.length > 0)
        })
        return (
            // <div style={{paddingTop: '5px'}}>
            <div>
                {
                    globalNavigationItems && globalNavigationItems.map((item,index) => {
                        return <li key={index} className={`nav-item ${active === item.Title.toUpperCase() ? 'active': ''}`}>
                            <a href={item.Link && item.Link.Url} target={item.Opens_in === "New Window" ? '_blank' : '_self'}>{item.Title.toUpperCase()}</a>
                        </li>
                    })
                }
                {
                    allowedLinks && allowedLinks.map((item,index) => {
                        return <li key={index} className={`nav-item ${active === item.Title.toUpperCase() ? 'active': ''}`}>
                            <a href={item.Link && item.Link.Url} target={item.Opens_in === "New Window" ? '_blank' : '_self'}>{item.Title.toUpperCase()}</a>
                        </li>
                    })
                }
            </div>
        )
    }
}