import React, {Component} from 'react'

export default class ContentWidgetHeader extends Component{
    render(){
        const {title,subTitle,addClass,addStyle} = this.props
        const styl = Object.assign({},addClass,JSON.parse(addStyle.Style))
        return(
            <div>
                <div className="content-widget-header" style={styl}>{title}</div>
                {
                    subTitle && 
                    <div className="content-widget-header-subtitle" style={Object.assign({},styl,{fontSize:'18px'})}>{subTitle}</div>
                }
            </div>
        )
    }
}