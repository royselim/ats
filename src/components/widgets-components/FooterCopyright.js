import React from 'react'

const FooterCopyright = ({text}) => {
	return (
		<span className="footer-img-text">
			&copy; {text}
		</span>
	)
}

export default FooterCopyright
