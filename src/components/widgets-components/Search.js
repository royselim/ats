import React, {Component} from 'react'
import {
    Search_Page,
    Search_Placeholder_text
} from '../../constants'

export default class Search extends Component{
    constructor(props){
        super(props)
        this.state ={
            search: ''
        }
    }

    handleChange = (e) => {
        this.setState({
            search: e.target.value
        })
    }

    handleEnter = (e) => {
        if(e.key === 'Enter'){
            e.preventDefault()
            e.stopPropagation()
            document.getElementById('nav-header-search').click()
        }
    }

    detectIE = () => {
        var ua = window.navigator.userAgent;
    
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
    
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
    
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
           // Edge (IE 12+) => return version number
           return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
    
        // other browser
        return false;
    }

    render(){
        const {config, context} = this.props
        const ie = (this.detectIE() && this.detectIE() <= 11);
        const searchUrl = ie ? '/_layouts/15/osssearchresults.aspx': 
            config.find(i => i.Title === Search_Page).Value;
        const placeholder = config.find(i => i.Title === Search_Placeholder_text).Value
        const baseUrl = context && context.webAbsoluteUrl
        return (
            <div className="input-form-group" id="nav_menu_input_search">
                <input type="text" 
                    className="form-control" 
                    name="search" 
                    placeholder={placeholder}
                    value={this.state.search} 
                    onChange={this.handleChange}
                    onKeyPress={this.handleEnter}
                />
                <a id="nav-header-search" target={ie ? '_blank': '_self'} href={`${baseUrl}${searchUrl}?k=${this.state.search}&u=${encodeURIComponent(baseUrl)}`}
                    style={{display: 'none'}}
                >#</a>
            </div>
        )
    }
}