import React, {Component} from 'react'

export default class TertiaryPageSubmenu extends Component {
    render(){
        let {
            links
        } = this.props

        return(
            <div>
                {
                    links.map((l,x) => {
                        let m = l.Title.toLowerCase().replace(/\s/g, '-')
                        return <a key={x} href={l.Link.Url}>
                            <div className={`${m} secondary-page-submenu bold sub-blue`}>
                                {l.Title.toUpperCase()}
                            </div>
                        </a>
                    })
                }
            </div>
        )
    }
}