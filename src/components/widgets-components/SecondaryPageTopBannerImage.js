import React, {Component} from 'react'

export default class SecondaryPageTopBannerImage extends Component{
    render(){
        const {bannerUrl,unit,planeIcon} = this.props
        return (
            <div className="ten wide column"> 
                <div className="ui grid"
                    style={{
                        backgroundImage: 'url("'+ bannerUrl +'")',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat',
                        height: '250px',
                        margin: '0',
                        borderRadius: '5px'
                    }}
                >
                    <div className="ten wide column no-padding">
                        <div className={unit.Top_Banner_Transparency ? 'secondary-page-banner-img-details secondary-page-transparency': 'secondary-page-banner-img-details'}>
                            <div className="ui segment no-padding secondary-page-column">
                                <div className="ui grid">
                                    <div className="two wide column no-padding">
                                        <div className="ui segment no-padding"
                                            style={{
                                                backgroundImage: 'url("'+planeIcon+'")',
                                                backgroundSize: 'cover',
                                                backgroundPosition: 'center center',
                                                backgroundRepeat: 'no-repeat',
                                                backgroundColor: 'rgba(0,0,0,0)',
                                                boxShadow: 'none',
                                                padding: '0',
                                                border: '0',
                                                height: '35px'
                                            }}
                                        >
                                        </div>                                                            
                                    </div>
                                    <div className="fourteen wide column no-padding secondary-page-column">
                                        <div className="ui segment no-padding secondary-page-column secondary-page-banner-title"
                                            style={{paddingTop: '15px'}}
                                        >
                                            {unit.Banner_Title}
                                        </div>
                                        <div className="ui segment no-padding secondary-page-column secondary-page-banner-subtitle">
                                            {unit.Banner_Subtitle}
                                        </div>
                                        <div className="ui segment no-padding secondary-page-column secondary-page-banner-details">
                                            {unit.Banner_Details}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        )
    }
}