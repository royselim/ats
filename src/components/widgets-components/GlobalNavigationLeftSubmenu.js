import React,{Component} from 'react'
import {Secondary_Page_Left_Submenu_icon as title} from '../../constants'

export default class GlobalNavigationLeftSubmenu extends Component{
    constructor(props){
        super(props)
        this.state = {
            groupExpanded: true,
            clicked: ''
        }
    }

    static getDerivedStateFromProps(props, state){
        const queryContent = props.page || props.group

        // let includedItems = props.items.filter(i => i.Parent && i.Parent.toLowerCase()
        //     .indexOf(queryContent && queryContent.toLowerCase()) !== -1)

        let includedItems = props.items.filter(i => {
            let filterArray = i.Parent && i.Parent.replace(/\s$/, '').toLowerCase().split('->')
            filterArray && filterArray.forEach((item, index) => {
                filterArray[index] = item.trim()
            })
            return filterArray && filterArray.indexOf(queryContent && queryContent.toLowerCase()) !== -1
        })

        let leftNav = {}
        includedItems.forEach(i => {
            let path = i.Parent.toLowerCase().split('->')
            let title = i.Title.toLowerCase()
            if(path.length === 1){
                leftNav[title] = {
                    item: i,
                    children: {},
                    expanded: ( state[title] && state.clicked === title && state[title].expanded ) || false
                }
            } else if(path.length === 2){
                leftNav[path[1].trim()]['children'][title] = {
                    item: i
                } 
            }
        });
        return {
            leftNav: leftNav
        }
    }

    toggleHidden = (e,t) => {
        const tname = e.target.className.replace(/_/g,' ')
        const name = t && t.toLowerCase()
        tname.includes(name) && this.setState((state) => {
            return state.leftNav[name] && {
                [name]: Object.assign({}, state.leftNav[name], {expanded: !state.leftNav[name].expanded}),
                clicked: name
            }
        })
    }

    handleToggleGroup = () => {
        this.setState((state)=>{
            return {
                groupExpanded: !state.groupExpanded
            }
        })
    }

    createItem = (obj, parent) => {
        const {config,images,context} = this.props
        const iconVal = config.find(f=>f.Title === title).Value
        const icon = images && images.find(f=>f.ATS_x0020_Title === iconVal)
        const iconFF = icon && icon.FieldValuesAsText.FileRef
        const iconUrl = iconFF && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,iconFF)

        const planeFly = <div style={{
            backgroundImage: 'url("'+iconUrl+'")',
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center center',
            height: '15px',
            width: '20px',
            display: 'inline-block'
        }} />
        const empty = <div style={{
            height: '15px',
            width: '20px',
            display: 'inline-block'
        }} />

        let ret = []
        for(var n in obj){
            let item = obj[n].item
            let cdrn = obj[n].children
            const cname = item.Title.replace(/\s/g,'_').toLowerCase()
            let arrowDir = (this.state.leftNav[n] && this.state.leftNav[n].expanded) ? 'up': 'down'
            ret.push(<div id={n.replace(' ', '-')} key={n} name={n.replace(' ', '-')}  className="padding-bottom-5">
                {
                    item.Item_Type === 'link'? 
                    <a style={{paddingLeft: parent ? '20px': '0px', color: 'black'}} href={item.Link && item.Link.Url} target={item.Opens_in === "New Window" ? '_blank' : '_self'}>{item.Title}</a>:
                    <div onClick={e => this.toggleHidden(e,item.Title)}>
                        {arrowDir === 'up' ? planeFly: empty}
                        <span id={item.Title.replace(/\s/g,'_').toLowerCase()} className={cname} 
                            style={{fontWeight: arrowDir === 'up'? 'bold': 'normal',color: arrowDir === 'down'? '#4597cb': '#000000'}}>
                            {item.Title.toUpperCase()}
                        </span>
                        {` `}<i className={`chevron ${arrowDir} icon ${cname}`} />
                        {    
                            cdrn && 
                            <div style={{paddingLeft: '35px', display: this.state.leftNav[n].expanded ? 'block': 'none'}}>
                               {this.createItem(cdrn, false)}
                            </div>
                        }
                   </div>
                }
            </div>)
        }
        return ret
    }

    render(){
        const {page} = this.props

        let {
            groupExpanded
        } = this.state
        let arrowDir = groupExpanded ? 'up' : 'down'

        let html = this.createItem(this.state.leftNav, true)

        return(
            <div className="ui grid">
                <div className="one wide column no-right-padding"></div>
                <div className="fifteen wide column submenu-group">
                    {
                        page && 
                        <div onClick={this.handleToggleGroup} className="secondary-page-submenu bold black">
                            {page.toUpperCase()}
                            {`  `}
                            <i className={`chevron ${arrowDir} icon`}/>
                        </div>
                    }
                    <div>
                        {
                            groupExpanded ?
                            <div className="submenu-group">
                                {html}                           
                            </div>
                            :
                            null
                        }
                    </div>
                    {/* <div style={{padding: '0px 0px 0px 20px'}}>
                        {
                            this.props.links.map((l,x) => {
                                let m = l.Title.toLowerCase().replace(/\s/g, '-')
                                return <a key={x} href={l.Link.Url}>
                                    <div className={`${m} secondary-page-submenu sub-blue`}>
                                        {l.Title.toUpperCase()}
                                    </div>
                                </a>
                            })
                        }
                    </div> */}
                </div>
            </div>
        )
    }
}