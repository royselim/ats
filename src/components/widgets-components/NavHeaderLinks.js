import React, {Component} from 'react'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {Header_top_links_list_name as title} from '../../constants'
import {ERROR_GETTING_DATA} from '../languages/en-us'

export default class NavHeaderLinks extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }

    componentDidMount(){
        const {context,config} = this.props
        const list = config.find(i => i.Title === title).Value
        const url = getUrl(context)
        getSPListItems(url, list, config).then(items => {
            if(Array.isArray(items)){
                this.setState({items})
            } else {
                this.setState({response:items})
            }
        })
    }

    render() {
        const { items,response } = this.state
        return (
            <ul className="mt-3">
                {
                    items ? items.map((item,index) => 
                    <li key={index}>
                        <a className="header-links" 
                            href={item.Link.Url} 
                            target={item.Target === 'new tab'? '_blank': '_self'}>
                            {item.Title.toUpperCase()}
                        </a>
                    </li>):
                    response && <span style={{color: 'white'}}>
                        {`${ERROR_GETTING_DATA}${response.status} - ${response.statusText}`}
                    </span>
                }
            </ul>
        )
    }
}