import React, {Component} from 'react'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {getSPListFields} from '../helpers/GetSPListFields'
import {Business_Units_Preference_list_name} from '../../constants'
import {
    Secondary_Page,
    Business_Units_query_parameter_name
} from '../../constants'

export default class BusinessUnits extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }

    componentDidMount(){
        const {context, config} = this.props
        const url = getUrl(context)
        const list = config && config.find(i => i.Title === Business_Units_Preference_list_name).Value
        getSPListItems(url, list,config).then(rslt => {
            if(Array.isArray(rslt)){
                this.setState({items: rslt})
            } else {/** TODO */}
        })
        getSPListFields(url, list,'&$select=Title').then(rslt => {
            if(Array.isArray(rslt)){
                this.setState({fields: rslt})
            } else {/** TODO */}
        })
    }

    render(){
        const {items,fields} = this.state
        const {context, active, config} = this.props
        const secPage = config && config.find(i => i.Title === Secondary_Page).Value
        const paramName = config && config.find(i => i.Title === Business_Units_query_parameter_name).Value

        const busFields = items && items.filter(i => i.Title === context.userEmail)[0]
        const bus = busFields && Object.keys(busFields).filter(b => 
            typeof busFields[b] === 'boolean' && busFields[b]).map(m => m.replace(/_x0020_/g, ' '))

        let absUrl = ( context && context.webAbsoluteUrl ) || 'https://atsmro4.sharepoint.com/sites/ai'
        
        const links = bus || ( fields && fields.map(f => f.Title) )

        return (
            <span style={{paddingTop: '5px'}}>
                {
                    ( links && links.length ) ? links.map((item, index) => {
                        return <li key={index} className={`nav-item ${active === item.toUpperCase() ? 'active': ''}`}>
                            <a href={`${absUrl}${secPage}?${paramName}=${item}`}>
                                {item.toUpperCase()}
                            </a>
                        </li>
                    }): <div></div>
                }
            </span>
        )
    }
}