import React, { Component } from 'react'

export default class ContentLinkDetails extends Component{
    getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
    }
    
    render(){
        const {link} = this.props
        const imageUrl = link && this.getImage(link.Image2Id)

        return (
            <div>
                {
                    link ? 
                    <a href={link.Link.Url}>
                        <div className="content-link">
                            <div className="content-link-title">
                                {link.Title}
                            </div>
                            <div className="content-link-image-details">
                                <div className="ui grid">
                                    <div className="five wide column">
                                        <div className="content-link-image"
                                            style={{
                                                backgroundImage: 'url("'+imageUrl+'")',
                                                backgroundSize: 'cover',
                                                backgroundPosition: 'center center',
                                                backgroundRepeat: 'no-repeat',
                                                height: '90px',
                                                borderRadius: '5px'
                                            }}
                                        >
                                        </div>
                                    </div>
                                    <div className="eleven wide column">
                                        <div className="content-link-details">
                                            <div className="content-link-subtitle">{link.Subtitle}</div>
                                            <div className="content-link-details">{link.Description}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                
                    </a>: <div></div>
                }
            </div>
        )
    }
}