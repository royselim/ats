import React, {Component} from 'react'

export default class OtherFilter extends Component {
    handleChange = (e) => {
        const {change} = this.props
        change(e.target.value)
    }
    render(){
        const {otherAirlines,selected} = this.props
        return <div>
            <select style={{padding: '5px', width: '100%'}} 
                onChange={this.handleChange} 
                className="ui selection dropdown" 
                value={selected}
            >
                <option value="0">Select a filter</option>
                {
                    otherAirlines.map(o=><option value={o.Id}>{o.Title}
                    </option>)
                }
            </select>
        </div>
    }
}