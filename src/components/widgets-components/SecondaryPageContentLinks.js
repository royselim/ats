import React, { Component } from 'react'
import ContentLinkDetails from './ContentLinkDetails'

export default class SecondaryPageContentLinks extends Component{
    render(){
        const {unit,links,images,context,config} = this.props

        let item1 = unit.Content_Link1Id ? links.find(i => i.Id === unit.Content_Link1Id) : null
        let item2 = unit.Content_Link2Id ? links.find(i => i.Id === unit.Content_Link2Id) : null
        let item3 = unit.Content_Link3Id ? links.find(i => i.Id === unit.Content_Link3Id) : null
        let item4 = unit.Content_Link4Id ? links.find(i => i.Id === unit.Content_Link4Id) : null

        return (
            <div>
                {
                    (unit.Content_Link1Id && item1) &&
                    <div className="ui segment no-padding content-link-container">
                        <ContentLinkDetails link={item1} 
                            images={images}
                            context={context}
                            config={config}
                        />
                    </div>
                }
                {
                    (unit.Content_Link2Id && item2) && 
                    <div className="ui segment no-padding content-link-container">
                        <ContentLinkDetails link={item2} 
                            images={images}
                            context={context}
                            config={config}
                        />
                    </div>
                }  
                {
                    (unit.Content_Link3Id && item3) &&
                    <div className="ui segment no-padding content-link-container">
                        <ContentLinkDetails link={item3} 
                            images={images}
                            context={context}
                            config={config}
                        />
                    </div>
                } 
                {
                    (unit.Content_Link4Id && item4) &&
                    <div className="ui segment no-padding content-link-container">
                        <ContentLinkDetails link={item4} 
                            images={images}
                            context={context}
                            config={config}
                        />
                    </div>
                }    
            </div>
        )
    }
}