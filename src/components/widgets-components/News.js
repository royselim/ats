import React, { Component } from 'react'
import NewsDetails from './NewsDetails';
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {
	Home_ATS_News_list_name,
	ATS_News_Label,
	Home_Archive_Label,
	Archive_Page_URL
} from '../../constants'

export default class News extends Component {
	constructor(props){
		super(props)
		this.state = {}
	}

	componentDidMount(){
		const url = getUrl(this.props.context)
		const {config} = this.props
		const list = config.find(f=>f.Title === Home_ATS_News_list_name).Value
        getSPListItems(url, list,config, '&$top=4').then(rslt => {
            if(Array.isArray(rslt)){
                this.setState({items: rslt})
            } else { /** TODO */ }
        })
    }
	
	render() {
		const {config,context} = this.props
		const siteURL = context.siteAbsoluteUrl
		const {items} = this.state
		const label = config.find(t => t.Title === ATS_News_Label).Value
		const archiveLabel = config.find(t => t.Title === Home_Archive_Label).Value
		const archiveURL = config.find(t => t.Title === Archive_Page_URL).Value

		return (
			<div>
				{
					items && 
					// <div id="news-widget" className="ui segment widgets no-border" style={{marginTop: '-25px'}}>
					<div id="news-widget" className="widgets no-border" style={{marginTop: '-25px',padding: '16px'}}>
					<h2>{label}</h2>	
					<div className="ui two column doubling stackable grid serps-wrapper">
						{
							items.map((item, index) => {
								return <div key={index} className="eight wide column no-bg">
									{/* <div className="ui segment widgets no-padding no-border no-bg"> */}
									<div className="widgets no-padding no-border no-bg">
									<NewsDetails item={item}/>
								</div>
							</div>
							})
						}
					</div>					
					<div className="ui grid">
					<div className="column">
						<a href={`${siteURL}${archiveURL}`}>
							<span style=
								{{
									fontWeight: 'bold',
									float: 'right',
									margin: '10px', 
									padding: '5px 15px 5px 15px', 
									backgroundColor: 'rgb(69, 151, 203)', 
									borderRadius: '5px',
									color: 'white'
								}}>
								{archiveLabel}
							</span>
						</a>
						{/* <button className="ui right floated blue button">ARCHIVE</button> */}
					</div>
					</div>					
				</div>
				}
			</div>
		)
	}
}