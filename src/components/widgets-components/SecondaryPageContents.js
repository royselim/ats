import React, {Component} from 'react'
import semanticConverter from '../helpers/semanticUIConverter'
import BusinessUnits from '../alternates/AltBusinessUnits'
import {
    Business_Unit_Quick_Links_label as qlabel,
    Application_Parents_Other_Title as Other    
} from '../../constants'
import AltOtherFilter from '../alternates/AltOtherFilter'

export default class SecondaryPageContents extends Component {
    constructor(props){
        super(props)

        const userPrefs = props.buPreferences.find(f=>f.Title === props.context.userEmail)
        const userBus = userPrefs && Object.entries(userPrefs).filter(f=>
            typeof f[1] === 'boolean' && f[1])

        this.state = {
            parentId: 1,
            businessUnit: userBus && userBus[0][0],
            otherAirline: 0,
            otherLetter: null,
            contentType: props.contentType
        }
    }

    componentDidMount(){        
        fetch('https://api.ipify.org?format=json')
        .then(res=>{
            if(res.ok){
                res.json().then(data => {
                    this.setState({userIp: data.ip})
                })
            } else {
                /** TODO */
            }
        })
    }

    handleChangeBu = (bu) => {
        this.setState({
            businessUnit: bu
        })
    }

    static getDerivedStateFromProps(props, state) {
        return {
            contentType: props.contentType,
            otherLetter: state.contentType !== props.contentType ? 
                null: state.otherLetter
        }
    }

    setDefaultBu = (bu) => {
        this.setState((state)=>{
            const unit = state.businessUnit
            return {
                businessUnit: unit ? unit: bu
            }
        })
    }

    handleChangeOther = (id) => {
        this.setState({
            otherAirline: id,
            otherLetter: null
        })
    }

    handleChangeOtherletter = (ltr) => {
        this.setState({
            otherLetter: ltr,
            otherAirline: 0
        })
    }

    handleChangeParent = (id) => {
        this.setState({
            parentId: id
        })
    }

    getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
    }
    
    getRange = (start, end) => {
        const range = []
        for(let i = start; i<=end;i++){
            range.push(i)
        }
        return range
    }

    render(){
        let {parentId,businessUnit,otherAirline,otherLetter,userIp} = this.state

        let {
            contentType,
            applicationsParentsItems,
            contentProps,
            context,
            config,
            buPreferences,
            quickLinks,
            otherAirlines,
            ipWhitelist
        } = this.props

        let allRange = []
        let whiteListed = false
        if(userIp){
            for(let n in ipWhitelist){
                let ipad = ipWhitelist[n].IP_Address
                const wild = /\.\d{0,3}[X|x]$/
                let baseRange = []
                if(ipad.includes('-')){
                    baseRange = ipad.split(/\s{0,9}-\s{0,9}/)
                    let start = baseRange[0].split('.').pop()
                    let end = baseRange[1].split('.').pop()
                    let betRange = this.getRange(start,end)
                    let base = baseRange[0].replace(/\d{1,3}$/,'')
                    allRange = betRange.map(b => base + b )
                } else if(wild.test(ipad)){
                    let lastTerm = ipad.match(wild)[0].replace('.','')
                    let start = lastTerm.replace(/X|x/,'0')
                    let base = ipad.replace(wild,'')
                    let betRange = this.getRange(start,255)
                    allRange = betRange.map(b => base + '.' + b )
                } else {
                    allRange.push(ipad)
                }
                whiteListed = allRange.indexOf(userIp) !== -1
                if(whiteListed) break
            }
        }

        const parent = applicationsParentsItems.find(f=>f.Id === parentId)
        const other = config.find(f=>f.Title === Other).Value
        const isOther = parent.Title === other

        const content = contentProps[contentType.replace(/\s/g, '_').toLowerCase()]
        let visibleItems = content && content.filter(a =>{
            const sameParent = a.ParentId && a.ParentId === parentId
            const otherAir = ( isOther && otherAirline !== 0 && a.Other_AirlinesId === Number(otherAirline)) 
            const letItems = isOther && otherLetter && a.Title.charAt(0).toUpperCase() === otherLetter
            return isOther ? (sameParent && (otherAir || letItems)) : sameParent
        })

        const vln = visibleItems ? visibleItems.length: 0
        const vslice = Math.ceil(vln/2)
        const visibleItems1 = visibleItems && visibleItems.slice(0, vslice)
        const visibleItems2 = visibleItems && visibleItems.slice(vslice, vln)

        const visbileQl = quickLinks && quickLinks.filter(f=>f.Business_Unit === businessUnit)
        const buqlLabel = config && config.find(f=>f.Title === qlabel).Value
        const ln = visbileQl ? visbileQl.length: 0
        const slice = Math.ceil(ln/2)
        const visbileQl1 = visbileQl && visbileQl.slice(0, slice)
        const visibleQl2 = visbileQl && visbileQl.slice(slice, ln)

        return (
            <div>
                <div className="ui stackable grid">
                    <div className="sixteen wide column">
                        <div className="secondary-page-business-unit" style={{marginBottom: '0px'}}>
                            <div className="secondary-page-business-unit-container">
                                <div className="spbu-title">
                                    {buqlLabel}
                                </div>
                            </div>                    
                        </div>
                        <div className="secondary-page-business-unit">
                            <div className="secondary-page-business-unit-container">
                                <BusinessUnits 
                                    context={context} 
                                    config={config} active='' 
                                    items={buPreferences}
                                    changeGroup={this.handleChangeBu}
                                    defaultBu={this.state.businessUnit}
                                    setDefault={this.setDefaultBu}
                                />
                            </div>
                        </div>
                        <div className="spbu-links">
                            <div className="spbu-links-content">
                                <div className="ui stackable grid">
                                    {
                                        visbileQl && [visbileQl1,visibleQl2].map((i,x)=>
                                        <div key={x} className="ui eight wide column">
                                            {
                                                i.map((v,y) => {
                                                    return <div key={y} className="secondary-page-actual-content-container"> 
                                                        <a href={v.Link && v.Link.Url} target={v.Link_Opens === 'new tab' ? '_blank': '_self'}>
                                                            <div className="secondary-page-actual-content-title">{v.Title.toUpperCase()}</div>
                                                        </a>
                                                        <div className="secondary-page-actual-content-description">{v.Description}</div>                                    
                                                    </div>
                                                })
                                            }
                                        </div>)
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    whiteListed ?
                    <div>
                        <div className="ui stackable grid">
                            <div className="sixteen wide computer only column">
                        <div className="secondary-page-content-title">
                            {contentType.toUpperCase()}
                        </div>
                        <div className={`ui ${semanticConverter[applicationsParentsItems.length]} column stackable grid`}>
                            {
                                applicationsParentsItems.map((p,x) => {
                                    let activeColor = p.Id === parentId ? '#ffffff': '#d6d6d6'
                                    const logoUrl = this.getImage(p.Logo2Id)
                                    return <div key={x} className="column" onClick={() => this.handleChangeParent(p.Id)}>
                                        <div className="ui segment" style={{padding: '5px', backgroundColor: activeColor}}>
                                            <div style={{
                                                backgroundImage: 'url("'+logoUrl+'")',
                                                backgroundPosition: 'center center',
                                                backgroundSize: 'contain',
                                                backgroundRepeat: 'no-repeat',
                                                padding: '5px'
                                            }}>
                                                {"\u00A0"}
                                            </div>
                                        </div>
                                    </div>
                                })
                            }
                        </div>
                        <div className="secondary-page-content-full">
                            <div className="ui stackable grid">                        
                                {
                                    parent.Title === other && otherAirlines && 
                                    <div className="ui sixteen wide column">
                                        <AltOtherFilter otherAirlines={otherAirlines} 
                                            change={this.handleChangeOther}
                                            selected={otherAirline}  
                                            changeLetter={this.handleChangeOtherletter}   
                                            content={content}
                                            parentId={parentId}
                                            otherLetter={otherLetter}                     
                                        />
                                    </div>
                                }                        
                                {
                                    visibleItems && [visibleItems1,visibleItems2].map((q,w)=>
                                    <div key={w} className="ui eight wide column">
                                        {
                                            q.map((v,x)=>{
                                                return <div key={x} className="secondary-page-actual-content-container">
                                                    <a href={v.Link && v.Link.Url}>
                                                        <div className="secondary-page-actual-content-title">{v.Title.toUpperCase()}</div>
                                                    </a>
                                                    <div className="secondary-page-actual-content-description">{v.Description}</div>
                                                </div>
                                            })
                                        }
                                    </div>)
                                }
                            </div>
                        </div>                        
                    </div>
                    </div>
                    </div>: <div />
                    // <div style={{marginTop: '25px'}}>Ask your Admin to add your IP address to the IP Whitelist.</div>
                }
            </div>
        )
    }
}