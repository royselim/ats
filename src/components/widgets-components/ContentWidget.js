import React, {Component} from 'react'
import ContentWidgetHeader from './ContentWidgetHeader'
import ContentWidgetContent from './ContentWidgetContent'

export default class ContentWidget extends Component{
    render(){
        const {widget,addClass,addStyle,getImage,forSlide, backgroundSize, leftImageBgSize} = this.props
        return (
            <div className="content-widget">
                {
                    widget && widget.Show_Header && 
                    <ContentWidgetHeader class
                        title={widget.Title} 
                        subTitle={widget.Sub_Title}
                        addClass={addClass}
                        addStyle={addStyle}
                    />
                }
                {
                    widget && 
                    <ContentWidgetContent 
                        widget={widget}
                        getImage={getImage}
                        forSlide={forSlide}
                        width={widget.Width}
                        backgroundSize={backgroundSize || 'cover'}
                        leftImageBgSize={leftImageBgSize || 'cover'}
                    />
                }
            </div>
        )
    }
}