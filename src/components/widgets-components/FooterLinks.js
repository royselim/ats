import React from 'react'

const FooterLinks = ({ link, url }) => {
	return (
		<div className="ui card widgets no-border" id="footer-widget">
			<div className="content no-padding">
				<div className="left aligned header links"><a href={url}>{link}</a></div>
			</div>
		</div>
	)
}

export default FooterLinks
