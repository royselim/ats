import React, {Component} from 'react';
import {getSPListItems, getUrl} from '../helpers/GetSPListItems';
import {
    Documents_And_Manuals_Library_Name as libName,
    Documents_And_Manuals_Items_Per_Page as itemsPerPage
} from '../../constants'

export default class DocumentLibrarySearch extends Component {
    constructor(props){
        super(props);
        const ipp = props.config.find(item => item.Title === itemsPerPage);
        const ipPage = ( ipp && ipp.Value ) || 10;
        this.state ={
            searchText: '',
            items: [],
            pages: 1,
            currentPage: 1,
            itemsPerPage: Number(ipPage),
            filteredItems: [],
            resultsMessage: ''
        }
    }

    componentDidMount(){
        const {context, config} = this.props;
        const listNameItem = config.find(item => item.Title === libName);
        const listNAme = (listNameItem && listNameItem.Value) || 'Documents';
        const url = getUrl(context);

        getSPListItems(
            url, 
            listNAme, 
            null, 
            "$select=FieldValuesAsText&$expand=FieldValuesAsText&$top=5000"
        ).then(data => this.setState({items: data}));        
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleEnter = (e) => {
        if(e.key === 'Enter'){
            e.preventDefault()
            e.stopPropagation()
            this.handleClick();
        }
    }

    handleChangePage = (add) => {
        this.setState((state) => {
            const {pages, currentPage} = state;
            const newCurrentPage = currentPage + Number(add);
            return {
                currentPage: newCurrentPage > pages ? 1: ( newCurrentPage < 1 ? pages: newCurrentPage)
            }
        })
    }

    handleClick = () => {
        const {searchText, items, itemsPerPage} = this.state;

        const filteredItems = items.filter(item => 
            item.FieldValuesAsText.FileLeafRef.toLowerCase().includes(searchText.toLowerCase()));

        this.setState({
            filteredItems: filteredItems.map(item => {
                return {
                    FileLeafRef: item.FieldValuesAsText.FileLeafRef,
                    FileRef: item.FieldValuesAsText.FileRef
                }
            }),
            pages: Math.ceil(filteredItems.length/itemsPerPage),
            resultsMessage: filteredItems.length ? '' : 'No results found.',
            currentPage: 1
        });
    }

    handleClear = () => {
        this.setState({
            filteredItems: [],
            searchText: '',
            resultsMessage: '',
            pages: 1
        });
    }

    render(){
        const {searchText, filteredItems, resultsMessage, pages, currentPage, itemsPerPage} = this.state;
        const startItem = (currentPage - 1) * itemsPerPage;
        const endItem = startItem + itemsPerPage;
        const displayItems = filteredItems.length && filteredItems.filter((i,x) => x >= startItem && x <= endItem - 1);
        return (
            <div style={{padding: '5px 20px'}}>
                <div style={{fontSize: '18px', marginBottom: '15px'}}>
                    {`Search`}
                </div>
                <input type="text" 
                    name="searchText" 
                    onChange={this.handleChange} 
                    value={searchText}
                    style={{padding: '4px', height: '28px'}}
                    onKeyPress={this.handleEnter}
                />
                <input style={{backgroundColor: '#69a7d1', color: 'white', border: 'none'}} type="button" value="Search" onClick={this.handleClick}/>
                <input style={{backgroundColor: '#e8e3e7', color: 'black', border: 'none'}} type="button" value="Clear" onClick={this.handleClear} />
                <div style={{paddingTop: '10px'}}>
                    {
                        displayItems.length ? displayItems.map((item, index) => 
                            <div key={index} style={{paddingTop: '10px'}}>
                                <a href={item.FileRef} style={{color: 'black'}}>{item.FileLeafRef}</a>
                            </div>
                        ): <div>{resultsMessage}</div>
                    }
                </div>
                <div style={{marginTop: '30px'}}>
                    {
                        pages > 1 ? <div>
                            <span onClick={() => this.handleChangePage(-1)} 
                                style={{
                                    margin: '10px', 
                                    padding: '5px 10px', 
                                    backgroundColor: '#69a7d1', 
                                    color: 'white', 
                                    border: 'none',
                                    cursor: 'pointer'
                                }}>{`<`}
                            </span>
                            <input type="text" 
                                value={`page ${currentPage} of ${pages}`} 
                                disabled={true} 
                                style={{padding: '3px'}} 
                            />
                            <span onClick={() => this.handleChangePage(1)} 
                                style={{
                                    margin: '10px', 
                                    padding: '5px 10px', 
                                    backgroundColor: '#69a7d1', 
                                    color: 'white', 
                                    border: 'none',
                                    cursor: 'pointer'
                                    }}>{`>`}
                            </span>
                        </div>:<span></span>
                    }
                </div>
            </div>
        )
    }
}