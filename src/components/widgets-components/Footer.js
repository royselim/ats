import React, { Component } from 'react'
import FooterImages from './FooterImages';
import FooterMenu from './FooterMenu';
import FooterCopyright from './FooterCopyright';
import AltFooterWidget from '../alternates/AltFooterWidget'
import AltFooterLinks from '../alternates/AltFooterLinks'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {
	Footer_Locations_list_name,
	Footer_Links_1_list_name,
	Footer_Links_2_list_name,
	Footer_Images_1_list_name,
	Footer_Images_2_list_name,
	Footer_Copyright_text,
	Images_Document_Library_Name,
	Footer_Locations_URL
} from '../../constants'

export default class Footer extends Component {
	constructor(props){
		super(props)
		this.state = {}
	}

	componentDidMount(){
        const vrd = (s) => s.replace(/\s/g,'_').toLowerCase()
        const {config,context} = this.props
        const url = getUrl(context)
		const clists = [
			Footer_Locations_list_name,
			Footer_Links_1_list_name,
			Footer_Links_2_list_name,
			Footer_Images_1_list_name,
			Footer_Images_2_list_name,
			Images_Document_Library_Name
        ]
        const lists = config.filter(f=>clists.includes(f.Title)).map(m=>m.Value)
        const configLists = {}
        config.filter(f=>clists.includes(f.Title)).forEach(m=>{
            configLists[vrd(m.Title)] = vrd(m.Value)
        })
        this.setState({lists: configLists})
        
        lists.forEach(l => {
            getSPListItems(url, l,config).then(rslt => {
                if(Array.isArray(rslt)){
                    this.setState({[vrd(l)]: rslt})
                } else {/** TODO */}
            })
        })
    }

	render() {
		const vrd = (s) => s.replace(/\s/g,'_').toLowerCase()  
        const {config,context} = this.props   
        const {lists} = this.state
		const state = this.state

		const footer_locations = lists && state[lists[vrd(Footer_Locations_list_name)]]
        const footer_links_1 = lists && state[lists[vrd(Footer_Links_1_list_name)]]
        const footer_images_1 = lists && state[lists[vrd(Footer_Images_1_list_name)]]
        const footer_images_2 = lists && state[lists[vrd(Footer_Images_2_list_name)]]
		const footer_links_2 = lists && state[lists[vrd(Footer_Links_2_list_name)]]
		const images = lists && state[lists[vrd(Images_Document_Library_Name)]]

		const copyrightObject = config.find(s => s.Title === Footer_Copyright_text).Value
		const locations = config.find(s => s.Title === Footer_Locations_URL).Value

		return (
			<div id="footer-wrapper">
				<div id="footer-container">
					<div 
						style={{width: '100%',maxWidth: '1550px',margin: 'auto'}} 
						className="ui two column stackable grid"				
					>					
					<div className="ten wide column">
						<div className="ui grid">
						<div className="ui eight wide column">
							{
								footer_locations && 
								footer_locations.map((item, index) => {
									return <AltFooterWidget 
										key={index}
										header={item.Title}
										cityState={`${item.City  || ''}${item.City ? ',': ''} ${item.State || ''}`}
										address={item.Address1}
										zip={item.ZIP}
										country={item.Country}
										locations={locations}
									/>
								})
							}
						</div>					
						<div className="ui eight wide column">
							<div className="column">
								{
									footer_links_1 && 
									footer_links_1.map((item, index) => {
										return <AltFooterLinks 
											key={index}
											link={item.Title}
											url={item.Link.Url}
											opens={item.Link_Opens}
											context={context}
											config={config}
										/>										
									})
								}
							</div>
						</div>
						</div>
					</div>
					<div className="six wide column">
						<div className="ui two column doubling stackable grid">
							<div className="column">
								{ footer_images_1 && footer_images_1[0] && <FooterImages item={footer_images_1[0]} context={context} images={images}/> }	
									
							</div>
							<div className="column">
								{ footer_images_2 && <FooterImages item={footer_images_2[0]} context={context} images={images}/>}	
								{ footer_links_2 && <FooterMenu items={footer_links_2} /> }
								{ <FooterCopyright text={copyrightObject} />}	
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		)
	}
}