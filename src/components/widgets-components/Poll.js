import React, { Component } from 'react'
import AltPollDetails from '../alternates/AltPollDetails'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {updateSPListItems} from '../helpers/UpdateSPListItems'
import {
	Quick_Polls_list_name,
	Icons_list_name,
	Buy_and_Sell_list_name,
	Quick_Poll_Offset_Hours
} from '../../constants'

export default class Poll extends Component {
	constructor(props){
		super(props)
		this.state = {
			active: 0
		}
	}

	componentDidMount(){
		this.getitems()
	}

	getitems = () => {
		const {config, context} = this.props
		const clists = [Quick_Polls_list_name,Icons_list_name,Buy_and_Sell_list_name]
		const lists = config.filter(f=>clists.includes(f.Title)).map(m=>m.Value)
		const url = getUrl(context)
		lists.forEach(l => {
			getSPListItems(url, l,config).then(rslt => {
				if(Array.isArray(rslt)){
					this.setState({
						[l.replace(/\s/g,'_').toLowerCase()]: rslt
					})
				} else {/** TODO */}
			})
		})
	}

	handleNext = () => {
		this.setState(state => {
			return {
				active: state.active < state.quick_poll.length ? 
					state.active + 1 : 0
			}
		})
	}

	handlePrev = () => {
		this.setState(state => {
			return {
				active: state.active > 0 ? 
					state.active - 1 : state.quick_poll.length
			}
		})
	}

	handleVote = (id, data) => {
		const {config,context} = this.props
		const list = config.find(f=>f.Title===Quick_Polls_list_name).Value
		const url = getUrl(context) + "/_api/web/lists/GetByTitle('"+ list +"')/GetItemById("+ id +")"
		updateSPListItems(url, data).then(ok => ok && this.getitems())
	}

	render() {
		const {config,context,images} = this.props
		const state = this.state
		const quick_poll = state[config.find(f=>f.Title===Quick_Polls_list_name).Value.replace(/\s/g,'_').toLowerCase()]
		const icons = state[config.find(f=>f.Title===Icons_list_name).Value.replace(/\s/g,'_').toLowerCase()]
		const cOffset = Number(config.find(f=>f.Title===Quick_Poll_Offset_Hours).Value)

		let activePolls = context && quick_poll && quick_poll.filter(item => {
			let poll = true
			const username = context.userEmail.split('@')[0]
			for(var n in item){
				let value = item[n]
				if( value && value.toString().indexOf(username) !== -1 ){
					item['Voted'] = true
					const voters = value.split(',')
					const userVote = voters.find(v=>v.includes(username))
					const offset = userVote && ((new Date().valueOf() - userVote.split('|')[1])/(1000*3600))
					poll = offset<cOffset
				}
			}
			return poll === true
		});

		let item = activePolls && activePolls[this.state.active]

		return (
			<div style={{padding: !item ? '0px': '30px 15px 0px 15px', marginTop: !item ? '-10px': '0px'}}>
				{
					item && icons && 
					<AltPollDetails item={item} 
						next={this.handleNext} 
						prev={this.handlePrev} 
						icons={icons} 
						context={context}
						vote={this.handleVote}
						images={images} 
						config={config} />
				}
			</div>
		)
	}
}