import React from 'react'

const FooterWidget = ({ header, cityState, zip, address }) => {
  
  return (
    <div className="column">
      <div className="ui card widgets no-border" id="footer-widget">
        <div className="content no-padding">
          <div className="left aligned header">{header}</div>
          <br/>
          <div className="left aligned meta">{address}</div>
          <div className="left aligned meta">{cityState}</div>
          <div className="left aligned meta">{zip}</div>
        </div>
      </div>
    </div> 
  )
}

export default FooterWidget
