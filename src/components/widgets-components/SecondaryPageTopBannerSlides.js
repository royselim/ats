import React, {Component} from 'react'

export default class SecondarayPageTopBannerSlides extends Component{
    constructor(props){
        super(props)
        this.state = {
            keys: Object.keys(props.images)
        }
    }
    
    changeImage = (step) => {
        step > 0 ?
        this.setState((state)=>{
            state.keys.push(state.keys.shift())
            return {
                keys: state.keys
            }
        }):
        this.setState((state)=>{
            state.keys.unshift(state.keys.pop())
            return {
                keys: state.keys
            }
        })
    }

    rotate = (target) => {
        const {keys} = this.state
        while(target !== Number(keys[0])){
            keys.push(keys.shift())
        }
        this.setState({keys})
    }

    render(){
        const {keys} = this.state
        const {images, sliderHeight} = this.props
        // console.dir(sliderHeight)
        const current = images[keys[0]]
        const index = Number(keys[0])
        return (
            <div className="ten wide column"> 
                <div className="ui grid"
                    style={{
                        backgroundImage: 'url("'+ current +'")',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat',
                        height: sliderHeight || '250px',
                        margin: '0',
                        borderRadius: '5px',
                        backgroundColor: 'black'
                    }}
                >
                    <div className="two wide column center">
                        <div>
                            <i onClick={() => this.changeImage(-1)} 
                                style={{cursor: 'pointer'}}
                                class="large chevron circle left icon">
                            </i>
                        </div>
                    </div>
                    <div className="twelve wide column" >
                        <div style={{paddingTop: '85px'}} className="center">
                            {
                                images.map((m,i)=>
                                    <i onClick={() => this.rotate(i)} style={{padding: '8px', cursor: 'pointer'}} 
                                        className={`small ${index===i ? 'green': 'red'} circle icon`}>
                                    </i>
                                )
                            }
                        </div> 
                    </div>
                    <div className="two wide column center">
                        <div>
                            <i onClick={() => this.changeImage(1)} 
                                style={{cursor: 'pointer'}}
                                class="large chevron circle right icon">
                            </i>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}