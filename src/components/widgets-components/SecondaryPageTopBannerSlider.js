import React, {Component} from 'react'
import {Image_Sliders_list_name} from '../../constants'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import SecondarayPageTopBannerSlides from './SecondaryPageTopBannerSlides'

export default class SecondaryPageTopBannerSlider extends Component{
    constructor(props){
        super(props)
        this.state = {}
    }

    getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
    }

    componentDidMount(){
        const vrd = (s) => s.replace(/\s/g,'_').toLowerCase()
        const {config,context} = this.props
        const url = getUrl(context)
        const list = config.find(f=>f.Title === Image_Sliders_list_name).Value
        getSPListItems(url, list, config).then(rslt => {
            if(Array.isArray(rslt)){
                this.setState({[vrd(list)]: rslt})
            } else {/** TODO */}
        })
    }
    
    render(){
        const {imageSliderId,addClass, sliderHeight} = this.props
        const {image_sliders} = this.state
        const slider = image_sliders && image_sliders.find(f => f.Id === imageSliderId)
        const images = slider && Object.keys(slider).filter(f=>{
            return f !== 'ID' && f !== 'Id' && f !== 'Title' && f !== '__metadata'
        }).map(i => this.getImage(slider[i])).filter(m=>m !== null)

        return (
            <div style={addClass && addClass}>
                {
                    images && images.length && 
                    <SecondarayPageTopBannerSlides images={images} sliderHeight={sliderHeight}/>
                }
            </div>
        )
    }
}