import React, {Component} from 'react'
import SecondaryPageTopBannerSlider from './SecondaryPageTopBannerSlider'

export default class ContentWidgetContent extends Component{
    createMarkup = (richText) => {
        return {__html: richText}
    }

    render(){
        const {widget,getImage,forSlide,backgroundSize,leftImageBgSize} = this.props
        const {images,context,config} = forSlide
        const links = widget.links
        const cntnt = widget.Content
        const imgId = widget.Content_Left_ImageId
        const slider = widget.Content_SliderId
        const linksOdd = links && links.filter((l,i) => i % 2 === 1)
        const linksEven = links && links.filter((l,i) => i % 2 === 0)
        const brdr = widget && widget.Show_Border
        const bgImageId = widget.Background_ImageId;

        const defaultStyle = {
            backgroundImage: 'url("'+ getImage(bgImageId) +'")', 
            backgroundSize: backgroundSize || 'cover',
            backgroundRepeat: 'no-repeat',
            borderTop: brdr && !widget.Show_Header && '1px solid #989898'            
        }
        const biCustomStyle = widget.BI_Custom_Style && Object.assign({},defaultStyle,JSON.parse(widget.BI_Custom_Style));

        return(
            <div 
                className={brdr ? "content-widget-content": ""} 
                style={ !biCustomStyle ?  
                    defaultStyle : biCustomStyle
                }
            >
                {
                    slider && 
                    <SecondaryPageTopBannerSlider 
                        images={images}
                        context={context}
                        config={config}
                        imageSliderId={slider}
                        addClass={{margin:'-10px'}}
                    />
                }
                {
                    cntnt && 
                    <div className="ui grid" style={{margin: '-10px', padding: '10px'}}>
                        {
                            imgId &&
                            <div className="five wide column"
                                style={{
                                    backgroundImage: 'url("' + getImage(imgId) + '")',
                                    backgroundSize: leftImageBgSize || 'cover',
                                    backgroundPosition: 'center center',
                                    backgroundRepeat: 'no-repeat'
                                }}
                            /> 
                        }
                        {
                            imgId &&
                            <div className="eleven wide column">
                                <div dangerouslySetInnerHTML={this.createMarkup(cntnt)} />
                            </div>
                        }
                        {
                            !imgId &&
                            <div className="sixteen wide column" style={{paddingBottom: '0px'}}>
                                <div dangerouslySetInnerHTML={this.createMarkup(cntnt)} />
                            </div>
                        }
                    </div>
                    // <div dangerouslySetInnerHTML={this.createMarkup(cntnt)} />
                }
                {
                    (!widget.Width || widget.Width === 'Full') && links && links.length > 0 && <div className="ui stackable grid" style={{margin: '-10px'}}>
                        <div className="eight wide column">
                            <ul className="about-you-ul">
                                {
                                    linksEven && linksEven.map((l,i) => 
                                        <li key={i}>
                                            <a href={l.URL}>{l.Title}</a>
                                        </li>
                                    )  
                                }    
                            </ul>                  
                        </div>
                        <div className="eight wide column">
                            <ul className="about-you-ul">
                                {
                                    linksOdd && linksOdd.map((l,i) => 
                                        <li key={i}>
                                            <a href={l.URL}>{l.Title}</a>
                                        </li>
                                    )  
                                }  
                            </ul>                     
                        </div>
                    </div>
                }
                {
                    widget.Width === 'Half' && links && links.length > 0 && <div className="ui stackable grid" style={{margin: '-10px'}}>
                    <div className="sixteen wide column">
                        <ul className="about-you-ul">
                            {
                                links && links.map((l,i) => 
                                    <li key={i}>
                                        <a href={l.URL}>{l.Title}</a>
                                    </li>
                                )  
                            }    
                        </ul>                  
                    </div>
                </div>                   
                }
            </div>
        )
    }
}