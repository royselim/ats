import React, {Component} from 'react'

export default class Banner extends Component{
    getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
    }
    
    render(){
        const {items,id,bannerHeight} = this.props
        const item = items.find(i => i.Id === id)

        const bgUrl = item && this.getImage(item.Image2Id)

        return (
            item ? <a href={item.Link.Url}>
                <div 
                    style={{
                        backgroundImage: 'url("' + bgUrl + '")',
                        backgroundSize: 'contain',
                        backgroundPosition: 'center center',
                        backgroundRepeat: 'no-repeat',
                        height: bannerHeight,
                        borderRadius: '5px'
                    }}
                />
            </a>: <div></div>
        )
    }
}