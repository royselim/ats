import React,{Component} from 'react'
import MultiSelectPreference from './MultiSelectPreference'
import MixedTypePreference from './MixedTypePreference'

export default class Preferences extends Component {
    constructor(props){
        super(props)
        this.state = {}
        this.url = window.location.href.split('/Pages/')[0]
        this.options = {
			headers: {"accept": "application/json; odata=verbose"},
			credentials: 'include'
        }
    }

    componentDidMount(){
        let list = this.props.list
        fetch(				
			this.url + "/_api/web/lists/GetByTitle('" + list.Title + "')/fields?$filter=Hidden eq false and CanBeDeleted eq true&$select=Id,Title,StaticName,FieldTypeKind", 
			this.options              
		).then( res => {
			res.json().then(json => {
				this.setState({
                    fields: json.d.results
                })                
			})
        })
    }

    render(){
        let {list,type,email,submitted,reset,post} = this.props
        const {fields} = this.state
        let item = list.items.find(i => i.Title === email)
        let types = {
            'Multi-Select': MultiSelectPreference,
            'Single-Select': MultiSelectPreference,
            'Yes/No': MultiSelectPreference,
            'User Input': MultiSelectPreference,
            'Mixed Type': MixedTypePreference
        }

        return(
            <span>
                {
                    fields && React.createElement(
                        types[type],
                        {
                            item: item, 
                            fields: fields,
                            title: list.Title, 
                            submitted:submitted,
                            email: email,
                            reset: reset,
                            post: post
                        }
                    )
                }
            </span>
        )
    }
}