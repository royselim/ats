import React, { Component } from 'react'

class NewsDetails extends Component {
	render() {
		const item = this.props.item
		const pdate = item.Published_Date && new Date(item.Published_Date).toLocaleDateString()
		return (
			<a href={item.Link.Url}>
				<div className="ui widgets no-bg">
					<div className="ui ten wide column no-bg">
					<div className="no-bg">
						<div className="content no-bg">
						<div className="header no-bg">
							<h4>{item.Title}</h4>
						</div>
						<div className="meta no-bg">
							<div className="description paragraph no-bg">
								{`${pdate ? pdate + ' - ': ''}${item.Description}`}
							</div>
						</div>
						</div>
					</div>        
					</div>
				</div>
			</a>
		)
	}
}

export default NewsDetails
