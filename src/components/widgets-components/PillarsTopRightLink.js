import React, {Component} from 'react';
import '../../css/App.css'; 

export default class PillarsTopRightLink extends Component{
    render(){
        const {firstTxt, secondTxt, thirdTxt} = this.props
        return (
            <div className="pillars-top-right-link-container">
                <div className="pillars-top-right-link-text1">{firstTxt}</div>
                <div className="pillars-top-right-link-text2">{secondTxt}</div>
                <div className="pillars-top-right-link-text3">{thirdTxt}</div>
            </div>
        )
    }
}