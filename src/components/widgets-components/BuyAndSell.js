import React,{Component} from 'react'

export default class BuyAndSell extends Component{
	getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
	}

    render(){
	    const {icons,item,next,prev} = this.props
		const leftArrow = icons.find( i => i.Title === 'Alert left arrow button').Icon2Id
		const rightArrow = icons.find( i => i.Title === 'Alert right arrow button').Icon2Id

		const leftIconUrl = leftArrow && this.getImage(leftArrow)
		const rightIconUrl = rightArrow && this.getImage(rightIconUrl)
		const pollImageUrl = item && this.getImage(item.Background2Id)
        
        return(
			<div id="poll-content" className="ui grid"
				style={{
					backgroundImage: pollImageUrl ? 'url("'+ pollImageUrl +'")' : 'none',
					backgroundSize: 'cover',
					backgroundPosition: 'center center',
					padding: '15px ',
					borderRadius: '5px',
					border: '1px solid #a9a9a9'
				}}  
			>
				<div className="one wide column centered">
                    <div onClick={prev}>
						<img src={leftIconUrl} alt="" style={{display: 'block', height: '50%', maxHeight: '24px', maxWidth: '24px', margin: 'auto', padding: '0 0', verticalAlign: 'middle'}} />
					</div>
				</div>
				<div className="fourteen wide column centered">
                    <h1 style={{color: 'white'}}>Buy And Sell</h1>
				</div>
				<div className="one wide column centered">
                    <div onClick={next} >
						<img src={rightIconUrl} alt="" style={{display: 'block', height: '50%', maxHeight: '24px', maxWidth: '24px', margin: 'auto', padding: '0 0', verticalAlign: 'middle'}} />
					</div>
				</div>
            </div>
        )
    }
}