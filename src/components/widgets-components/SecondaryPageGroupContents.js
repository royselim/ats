import React, {Component} from 'react'
import SecondaryPageLeftBanners from './SecondaryPageLeftBanners'
import SecondaryPageContents from './SecondaryPageContents'
import SecondaryPageContentLinks from './SecondaryPageContentLinks'
import Loader from '../helpers/Loader'

export default class SecondaryPageGroupContents extends Component{
    render(){
        const {
            unit,
            banners,
            images,
            context,
            config,
            applications_parents,
            business_units_preferences,
            business_unit_quick_links,
            other_airlines,
            ip_whitelist,
            contentProps,
            content_links,
            content
        } = this.props
        return (
            <div className="ui stackable grid">
                <div className="three wide column"  style={{paddingRight: '15px'}}>
                    <div className="secondary-page-left-banners">
                        <SecondaryPageLeftBanners unit={unit} 
                            bannerItems={banners} 
                            images={images}
                            context={context}
                            config={config}
                        />
                    </div>
                </div>
                <div className="eight wide column"  style={{paddingRight: '0px', marginLeft: '-15px'}}>
                    <div className="secondary-page-application">
                        <SecondaryPageContents contentType={content} 
                            applicationsParentsItems={applications_parents}
                            businessUnit={unit}
                            contentProps={contentProps}
                            images={images}
                            context={context}
                            config={config}
                            buPreferences={business_units_preferences}
                            quickLinks={business_unit_quick_links}
                            otherAirlines={other_airlines}
                            ipWhitelist={ip_whitelist}
                        />
                    </div>
                </div>
                <div className="five wide column"  style={{paddingRight: '0px'}}>
                    <div className="secondary-page-right-banners">
                        <SecondaryPageContentLinks 
                            links={content_links} 
                            unit={unit}
                            images={images}
                            context={context}
                            config={config}
                        />
                    </div>
                </div>
            </div>				
        )
    }
}