import React from 'react'

const FooterMenu = ({items}) => {
	return (
		<div>
			{
				items.length ?
				items.map((item,index) => {
					return 	<span key={index} className="footer-img-text">
						<a href={item.Link.Url} className="footer-menu">{item.Title.toUpperCase()}</a>
						{
							index < items.length - 1 && '| '
						}
					</span>
				}) :
				<div></div>
			}
		</div>
	)
}

export default FooterMenu