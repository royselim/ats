import React, {Component} from 'react'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {How_Do_I_dropdown_menu_list_name} from '../../constants'
import {
    ERROR_GETTING_DATA,
    HOW_DO_I_DEFAULT
} from '../languages/en-us'

export default class INeedFAQs extends Component{
    constructor(props){
        super(props)
        this.state = {
            dropdown: '#',
            redirect: false
        }
    }

    componentDidMount(){
        const {context,config} = this.props
        const list = config.find(i => i.Title === How_Do_I_dropdown_menu_list_name).Value
        const url = getUrl(context)
        getSPListItems(url,list,config).then(items => {
            if(Array.isArray(items)){
                this.setState({items})
            } else {
                this.setState({response:items})
            }
        })
    }

    handleChange = (e) => {
        this.setState({
            dropdown: e.target.value,
            redirect: true            
        })
    }

    componentDidUpdate(){
        if(this.state.redirect)
            document.getElementById('nav-header-dropdown-link').click()
    }

    render(){
        let { items, response } = this.state
        return (
            <div style={{display: 'inline'}}>
                {
                    items ? items.length ? 
                    <div className="" id="nav_menu_input_select">
                        <select name="dropdown" id="" 
                            className="form-control" 
                            value={this.state.dropdown} 
                            onChange={this.handleChange}>
                            <option value="#">{HOW_DO_I_DEFAULT}</option>
                            {
                                items.map((faq,ndx) => <option key={ndx} value={faq.Link.Url}
                                >
                                    {faq.Title}
                                </option>)
                            }
                        </select>
                        <a id="nav-header-dropdown-link" 
                            href={this.state.dropdown} 
                            style={{display: 'none'}}>#
                        </a>
                    </div>:<span></span>:
                    response && <span style={{color: 'white'}}>
                        {`${ERROR_GETTING_DATA}${response.status} - ${response.statusText}`}
                    </span>
                }
            </div>
        )
    }
}