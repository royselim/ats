import React, { Component } from 'react'
import BlogSerpDetails from './BlogSerpDetails';
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {Home_main_sub_headlines_list_name} from '../../constants'

export default class BlogSerp extends Component {
	constructor(props){
        super(props)
		this.state = {}
	}

    componentDidMount(){
        const {config, context} = this.props
        const url = getUrl(context);
		const list = config.find(f=>f.Title === Home_main_sub_headlines_list_name).Value
        getSPListItems(url, list,config).then(rslt => {
            if(Array.isArray(rslt)){
                this.setState({items: rslt})
            } else { /** TODO */ }
		})
    }

	render() {
		const {items} = this.state
		const {images,context} = this.props
		let counter = 0
		let subHeadlines = {}
		items && items.forEach( s => {
			if( s.Active && counter < 5){
				subHeadlines['section' + s.Section] = s
				counter++
			}
		})

		return (
			<div>
				{
					items && 			
					// <div id="blogserp-widget" className="ui segment widgets no-padding no-border">
					<div id="blogserp-widget" className="widgets no-padding no-border" style={{padding: '0px'}}>
					<div className="ui one column stackable grid" style={{margin: '-25px'}}>
						{
							[1,2,3,4].map((s,x) => <div key={x} className="column">
								<div className={`widgets no-border`} style={{padding: '12px 10px'}}>
									<BlogSerpDetails 
										item={subHeadlines[`section${s}`]} 
										images={images} 
                                        context={context}
										detailClass="about-you"
										// detailClass="serps"
									/>
								</div>
							</div>)
						}
					</div>
				</div>
				}
			</div>
		)
	}
}