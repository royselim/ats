import React, { Component } from 'react'
import {Header_ATS_Logo_URL, Header_ATS_Logo_Link} from '../../constants'

export default class HeaderLogo extends Component {
    render(){
        const {config} = this.props
        const logoUrl = config.find(i => i.Title === Header_ATS_Logo_URL )
        const logoLink = config.find(i => i.Title === Header_ATS_Logo_Link )
        return(
            <a href={logoLink.Value || 'https://www.atsmro.com'}> 
                <span>
                    {
                        logoUrl && <div className="col col-sm-12" id="nav_menu_logo"
                            style={{
                                backgroundImage: "url('"+ logoUrl.Value +"')",
                                height: '45px',
                                backgroundSize: 'contain',
                                backgroundRepeat: 'no-repeat',
                                marginTop: '10px'               
                            }}
                        >
                            {/* <img style={{color:'white'}} src={logoUrl.Value} 
                            alt={ATS_LOGO}/> */}
                        </div>
                    }
                </span>
            </a>
        )   
    }
}