import React,{Component} from 'react'

export default class MixedTypePreference extends Component {
    constructor(props){
        super(props)
        this.state = {
            posted: false,
            item: {}
        }
    }

    componentDidUpdate(){
        this.props.submitted && this.submit()
    }   

    static getDerivedStateFromProps(props, state){
        return {
            item: (Object.keys(state.item).length !== 0 && state.item) || props.item || {}
        }
    }

    submit = () => {
        let {title,email,reset,post} = this.props
        let {item} = this.state
        
        reset()    
        
        let data = Object.assign(item, {}, {
            __metadata: { 'type': 'SP.Data.'+ title.replace(/\s/g, '_x0020_') +'ListItem'},
            Title: email
        })

        let options = {
            method: item.Id ? 'MERGE': 'POST',
            body: JSON.stringify(data),
            headers: {
                "accept": "application/json; odata=verbose",
                "content-type": "application/json;odata=verbose",
                "X-RequestDigest": document.getElementById('__REQUESTDIGEST').value,
                "IF-MATCH": "*"            
            },
            credentials: 'include'
        }

        let url = window.location.href.split('/Pages/')[0]

        fetch(
            url + "/_api/web/lists/GetByTitle('" + title + "')/" + 
                ( item.Id ? ("GetItemById("+ item.Id +")") : "items" ),
            options
        ).then(res => {
            post(title)
            if(res.ok){
                // window.location.assign(window.location.href)
            } else {
                alert('Error')
            }
        })
    }

    handleChange = (e) => {
        const name = e.target.name
        const value = e.target.value
        this.setState((state) => {
            return {
                item: Object.assign({},state.item,{
                    [name]: value
                })
            }
        })
    }

    render(){
        const {title,fields,item} = this.props
        const state = this.state
        return (
            <span>
                {
                    item && <div className="ui form my-5">
                    <h2 className="ui header">{title}</h2>                
                    <div>
                        {
                            fields && fields.map((f,x) => {
                                switch(f.FieldTypeKind){
                                    case 2:
                                        return <div key={x} style={{paddingBottom: '20px'}}>
                                            <label htmlFor={f.StaticName}>{f.Title}</label>
                                            <input type="text" 
                                                id={f.StaticName} 
                                                name={f.StaticName} 
                                                value={state.item[f.StaticName]} 
                                                onChange={this.handleChange}
                                            />
                                        </div>
                                        break
                                }
                            })
                        }
                    </div>
                </div>
                }
            </span>
        )
    }
}