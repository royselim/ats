import React, { Component } from 'react'

class BlogSerpDetails extends Component {
	constructor(props){
		super(props)
		this.state = {
			hover: false
		}
	}
	handleMouseOver = () => {
		this.setState({
			hover: true
		})
	}
	handleMouseOut = () => {
		this.setState({
			hover: false
		})
	}

	getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
	}

	render() {
		const {item, detailClass} = this.props
		let hover = this.state.hover
		const imageUrl = item && this.getImage(item.Image2Id)
		
		return (
			<a href={item.Link.Url} >
				<div className={`ui widgets ${detailClass} two column stackable grid blog-excerpt`} onMouseOver={this.handleMouseOver} onMouseOut={this.handleMouseOut}>
					<div className="five wide column" style={{padding: '10px 0px 5px 15px'}}>
						<div 						
							style={{
								backgroundImage: 'url("'+ imageUrl +'")',
								backgroundSize: 'contain',
								backgroundPosition: 'center center',
								backgroundRepeat: 'no-repeat',
								minHeight: '130px',
							}}
						/>
					</div>
					<div className="eleven wide column">
					<div className="">
						<div className="content">
						<div className="header">
							<h3 style={{color: hover ? 'white': '#4b9acc'}}>{item.Title}</h3>
						</div>
						<div className="meta">
							<h4 style={{color: hover ? 'white': '#4b9acc'}}>{item.Synopsis}</h4>
							<div className="description paragraph" style={{color: hover ? 'white': '#767676'}}>
								{item.Description}
							</div>
						</div>
						</div>
					</div>        
					</div>
				</div>
			</a>
		)
	}
}

export default BlogSerpDetails
