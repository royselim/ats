import React,{Component} from 'react'

export default class FooterImages extends Component {	
    getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
	}

	render(){
		const {item} = this.props
		const image = this.getImage(item.Image2Id)
		return (
			<a href={item.Link.Url} target={item.Link_Opens === 'new tab' ? '_blank': '_self'}>
				<div className="ui fluid card widgets no-border">
					<div className="image">
						<img src={image} alt=""/>
					</div>
				</div>    
			</a>
		)	
	}
}