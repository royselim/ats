import React,{Component} from 'react'
import SecondaryPageTopBannerSlider from './SecondaryPageTopBannerSlider'

export default class SecondaryPageVideo extends Component {
    constructor(props){
        super(props)
        this.state = {
            width: window.innerWidth
        }
    }
    updateWidth = () => {
        this.setState({
            width: window.innerWidth
        })
    }
    componentDidMount(){
        window.addEventListener("resize", this.updateWidth)
    }
    componentWillUnmount(){
        window.removeEventListener("resize", this.updateWidth)
    }
    render(){
        let width = this.state.width
        const {unit,images,context,config} = this.props
        let vtyp = unit.Video_type
        let video = unit.Video.Url

        const posterRef = images && images.find(f=>f.Id===unit.Video_Poster2Id)
        const vpRef = posterRef && posterRef.FieldValuesAsText.FileRef
        const vpUrl = context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,vpRef)

        let poster = unit.Video_Poster2Id ? vpUrl: null
        let mobilePixel = 767
        let videoWidth = width > mobilePixel ? 
            Math.round((31.25/100) * width) - 20  :
            width - 70

        return (
            <div>
                {
                    unit && unit.Image_SliderId ? 
                    <SecondaryPageTopBannerSlider 
                        images={images}
                        context={context}
                        config={config}
                        imageSliderId={unit.Image_SliderId}
                    />:
                    <div>
                        {
                            vtyp === 'External(Youtube)' ? 
                            <iframe title="External Video" width={videoWidth} height="250"
                                src={video.replace('watch?v=','embed/')}>
                            </iframe>
                            :
                            <video width={videoWidth} height="250" controls
                                poster={poster}
                            >
                                <source src={video} type="video/mp4" />
                            </video>
                        }
                    </div>
                }
            </div>
        )
    }
}