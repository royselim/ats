import React, {Component} from 'react'
import SecondaryPageLeftBanners from './SecondaryPageLeftBanners'
import SecondaryPageContents from './SecondaryPageContents'
import SecondaryPageContentLinks from './SecondaryPageContentLinks'
import Loader from '../helpers/Loader'

export default class SecondaryPagePageContents extends Component{
    render(){
        const {
            unit,
            banners,
            images,
            context,
            config
        } = this.props
        return (
            <div className="ui stackable grid">
                <div className="three wide column"  style={{paddingRight: '15px'}}>
                    <div className="secondary-page-left-banners">
                        <SecondaryPageLeftBanners unit={unit} 
                            bannerItems={banners} 
                            images={images}
                            context={context}
                            config={config}
                        />
                    </div>
                </div>
                <div className="thirteen wide column" style={{paddingRight: '0px'}}>
                    Content Widgets Here
                </div>
            </div>				
        )
    }
}