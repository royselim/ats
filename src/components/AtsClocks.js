import React, {Component} from 'react'
import Clock from '../functions/clock'
import {getSPListItems,getUrl} from './helpers/GetSPListItems'
import {
    ATS_Clocks_list_name as title,
    Custom_Logout_Button as logout
} from '../constants'

export default class AtsClocks extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }

    componentDidMount(){
        const {context,config} = this.props
        const url = getUrl(context)
        const list = config.find(f=>f.Title === title).Value
        getSPListItems(url, list, config).then(rslt => {
            if(Array.isArray(rslt)){
                this.setState({
					items: rslt
				})
            } else {
                // TODO
            }
        })
    }

    customLogout = () => {
        [
            'sp-show-ribbons',
            'O365_MainLink_Me',
            'meControlSignoutLink',
            'flexPaneCloseButton',
            'sp-show-ribbons'
        ].forEach(item => document.getElementById(item).click());
    }

    render(){
        const {config} = this.props
        const logOutUrl = config.find(f=>f.Title === logout).Value
        let {items} = this.state 
        return (
            <div className="float-right" id="time_difference_wrapper">
                {
                    <span onClick={() => this.customLogout()} 
                        className="customSignout" 
                        style={{backgroundImage: `url('${logOutUrl}')`}}
                    />
                }
                {
                    items && items.map((item, index) => {
                        return <Clock key={index} 
                            location={item.Location} 
                            offset={item.Offset} 
                            format={item.Format}
                            useSuffix={item.UseSuffix}
                        />
                    })
                }
            </div>
        )
    }
}