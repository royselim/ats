import React,{Component} from 'react'
import MiniWidget from './widgets-components/MniWidget';
import {getSPListItems,getUrl} from './helpers/GetSPListItems'
import {
    Emergency_Contacts_list_name,
    Report_an_Emergency_Modal_text as mdlTxt
} from '../constants'

export default class EmergencyContacts extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }

    componentDidMount(){
        const {config, context} = this.props
        const url = getUrl(context);
        const list = config.find(f=>f.Title === Emergency_Contacts_list_name).Value
        getSPListItems(url,list,config).then(items => {
            if(Array.isArray(items)){
                this.setState({items})
            } else {/** TODO */}
        })
    }

    render(){
        const {images,context,config} = this.props
        const modalText = config.find(f=>f.Title === mdlTxt).Value
        const {items} = this.state
        return (
            <div>
                {
                    items && 
                    <div id="mini-widget" style={{paddingLeft: '15px'}}>
                        <div className="ui two column grid">
                            {
                                items.map((item, index) => {
                                    return <MiniWidget key={index} 
                                        item={item} 
                                        images={images} 
                                        context={context}
                                        config={config}
                                        modalText={modalText}
                                    />
                                })
                            }
                        </div>
                    </div>
                }
            </div>
        )
    }
}