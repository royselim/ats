import '../css/styles.css'
import React, {Component} from 'react'
import AtsClocks from './AtsClocks'
import NavHeaderLinks from './widgets-components/NavHeaderLinks'
import INeedFAQs from './widgets-components/INeddFAQs'
import Search from './widgets-components/Search'
import HeaderGlobalNav from './widgets-components/HeaderGlobalNav'
import HeaderLogo from './widgets-components/HeaderLogo'
import HeaderHome from './widgets-components/HeaderHome'
import AboutYourWork from './widgets-components/AboutYourWork'
import {
    Business_Units_query_parameter_name,
    Content_Page_query_parameter_name,
    Secondary_Page_query_parameter_name
} from '../constants'
import {getSPResource} from './helpers/GetSPResource'
import {getUrl} from './helpers/GetSPListItems'

export default class NavAndHeader extends Component {
    constructor(props){
        super(props)

        let queryString = document.location.href.split('?')[1]
        let parameterList = queryString ? queryString.split('&'): []
        let parameters = {}
        parameterList.forEach(p => {
            let param = p.split('=')
            parameters[param[0].toLowerCase()] = decodeURIComponent(param[1]).toUpperCase()
        })
        let groupIds = [];
        getSPResource(getUrl(props.context) + 
            "/_api/web/currentuser/groups").then(data => {
                if(data && Array.isArray(data.value)){
                    data.value.forEach(group => {
                        groupIds.push(group.Id)
                    })
                }
            })
        this.state = { parameters, groupIds }
    }

    componentDidMount(){  
        const {parameters} = this.state 
        const {config} = this.props   
        const pqs = [
            Business_Units_query_parameter_name,
            Content_Page_query_parameter_name,
            Secondary_Page_query_parameter_name
        ]
        const active = config.filter(c=>pqs.includes(c.Title))
            .map(i=>i.Value.toLowerCase()).filter(f=>parameters[f] !== undefined)[0]
        this.setState({active: parameters[active] || 'HOME'})
    }

    render(){
        const e = React.createElement
        const {active, groupIds} = this.state
        const {config, context} = this.props
        const props = this.props
        return (
            <div>
                <nav className="navbar navbar-dark" id="nav_and_header_bar">  
                    <div className="col col-sm-12 col-md-3">
                        {e(HeaderLogo,{config})}
                    </div>
                    <div className="col col-sm-12 col-md-6 text-right p-0" id="nav_menu_links" align="center">
                        {e(NavHeaderLinks,props)}
                    </div>
                    <div className="col col-sm-12 col-md-3" id="nav_menu_input">
                        <div className="nav_menu_input_group" style={{display:'inline'}}>
                            {e(INeedFAQs,props)}
                        </div>
                        {e(Search,props)}
                    </div>
                </nav>

                <div className="navigation clearfix">
                    <div className="float-left">
                        <ul className="nav">
                            {active && e(HeaderHome,{context,active,config})}
                            {active && e(AboutYourWork,{context,active,config})}
                            {active && e(HeaderGlobalNav,{context,config,active, groupIds})}
                        </ul>
                    </div>
                    {e(AtsClocks,{context,config})}
                </div>
            </div>
        )
    }
}