import '../css/styles.css'
import '../css/App.css'
import React, {Component} from 'react'
import Article from './widgets-components/Article'
import ArticleTopImage from './widgets-components/ArticleTopImage'
import Banner from './widgets-components/Banner'
import Loader from './helpers/Loader'
import {getSPListItems,getUrl} from './helpers/GetSPListItems'
import SecondaryPageTopBannerSlider from './widgets-components/SecondaryPageTopBannerSlider'
import {
    Banners_list_name,
    Left_Navigation_list_name,
    Content_Page_list_name,
    Content_Page_query_parameter_name,
    Images_Document_Library_Name
} from '../constants'

export default class TertiaryPage extends Component{
    constructor(props){
        super(props)
        this.state = {}
    }

    componentDidMount(){
        const vrd = (s) => s.replace(/\s/g,'_').toLowerCase()
        const {config,context} = this.props
        const url = getUrl(context)
		const clists = [Images_Document_Library_Name,Banners_list_name,Left_Navigation_list_name,Content_Page_list_name]
        const lists = config.filter(f=>clists.includes(f.Title)).map(m=>m.Value)
        const configLists = {}
        config.filter(f=>clists.includes(f.Title)).forEach(m=>{
            configLists[vrd(m.Title)] = vrd(m.Value)
        })
        this.setState({lists: configLists})
        
        lists.forEach(l => {
            getSPListItems(url, l,config).then(rslt => {
                if(Array.isArray(rslt)){
                    this.setState({[vrd(l)]: rslt})
                } else {/** TODO */}
            })
        })
    }

    createMarkup(richText){
        return {__html: richText}
    }

    getImage = (id) => {
        const {images} = this.state
        const {context} = this.props
        const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
	}

    render(){
        const vrd = (s) => s.replace(/\s/g,'_').toLowerCase()  
        const {config,context} = this.props   
        const {lists} = this.state
        const state = this.state

        const images = lists && state[lists[vrd(Images_Document_Library_Name)]]
        const content_pages = lists && state[lists[vrd(Content_Page_list_name)]]
        const banners = lists && state[lists[vrd(Banners_list_name)]]
        
        let queryString = document.location.href.split('TertiaryPage.aspx?')[1]
        let parameterList = queryString ? queryString.split('&'): []
        let parameters = {}
        parameterList.forEach(p => {
            let param = p.split('=')
            parameters[param[0].toLowerCase()] = decodeURIComponent(param[1])
        })

        const paramName = config.find(f=>f.Title===Content_Page_query_parameter_name).Value

        const item = content_pages && content_pages.find(i => i.Title.toLowerCase() === parameters[vrd(paramName)].toLowerCase())
        
        // Change Document Title
        if(item && item.Title) document.title = item.Title;

        let layout = item ? {
            oneTitleTwoCol: item.Column2Title === null && item.Column2Content !== null,
            twoTitleTwoCol: item.Column2Title !== null && item.Column2Content !== null,
            oneTitleOneCol: item.Column2Title === null && item.Column2Content === null
        } : null

        const topImageUrl = item && images && this.getImage(item.Top_Image_BackgroundId)

        return (
            <div className="ui stackable grid about-you-page-paddings"  style={{width: '100%',maxWidth: '1550px',margin: 'auto'}}>
                <div className="sixteen wide column" style={{paddingLeft: '50px', paddingRight: '50px'}}>
                    {
                        (layout && (layout.twoTitleTwoCol || layout.oneTitleOneCol)) ?
                        <div className="ui stackable grid about-you-page-paddings">
                            <div className="eleven wide column no-padding">
                                <div className="tertiary-page-articles-container">
                                    {
                                        !item ? <div />:
                                        <ArticleTopImage url={topImageUrl} height="275px" />                                    
                                    }
                                    <div className="column">
                                        {
                                            !layout ? <Loader height={'400px'} />:
                                            <div>
                                                {
                                                    layout.oneTitleOneCol ? 
                                                    <Article title={item.Column1Title}
                                                        subTitle={item.Column1SubTitle}
                                                        content={item.Column1Content}
                                                    />
                                                    :
                                                    <div className="ui two column stackable grid">
                                                        <Article title={item.Column1Title}
                                                            subTitle={item.Column1SubTitle}
                                                            content={item.Column1Content}
                                                        />
                                                        <Article title={item.Column2Title}
                                                            subTitle={item.Column2SubTitle}
                                                            content={item.Column2Content}
                                                        />
                                                    </div>                                                
                                                }
                                            </div>
                                        }
                                    </div> 
                                </div>                                        
                            </div>
                            <div className="five wide column no-padding">
                                <div className="tertiary-page-right-banners-container">
                                    {
                                        (item && item.ImageSliderId && images) ? 
                                        <div className="tertiary-page-banner-container">
                                            <SecondaryPageTopBannerSlider 
                                                sliderHeight='275px'
                                                images={images}
                                                context={context}
                                                config={config}
                                                imageSliderId={item.ImageSliderId}
                                            />
                                        </div>: <div />                                           
                                    }
                                    {
                                        (item && item.Banner1Id && banners && images) ? 
                                        <div className="tertiary-page-banner-container">
                                            <Banner items={banners} 
                                                id={item.Banner1Id} 
                                                context={context}
                                                config={config}
                                                images={images}
                                                bannerHeight="150px"
                                            />
                                        </div>: <div />                                    
                                    }
                                    {
                                        (item && item.Banner2Id && banners && images) ? 
                                        <div className="tertiary-page-banner-container">
                                            <Banner items={banners} 
                                                id={item.Banner2Id} 
                                                context={context}
                                                config={config}
                                                images={images}
                                                bannerHeight="150px"
                                            />
                                        </div>: <div />                                  
                                    }  
                                    {
                                        (item && item.Banner3Id && banners && images) ? 
                                        <div className="tertiary-page-banner-container">
                                            <Banner items={banners} 
                                                id={item.Banner3Id} 
                                                context={context}
                                                config={config}
                                                images={images}
                                                bannerHeight="150px"
                                            />
                                        </div>: <div />                                   
                                    }   
                                </div>                                     
                            </div>
                        </div>
                        :
                        <div>
                            {
                                !item ? <Loader height={'400px'} /> :
                                <div>
                                    <div className="ui stackable grid">
                                        <div className="eleven wide column" style={{paddingRight: '0px'}}>
                                            <ArticleTopImage url={topImageUrl} height="275px" />
                                        </div>
                                        <div className="five wide column">
                                            {
                                                (item && item.ImageSliderId && images) ? 
                                                <div className="tertiary-page-banner-container">
                                                    <SecondaryPageTopBannerSlider 
                                                        sliderHeight='275px'
                                                        images={images}
                                                        context={context}
                                                        config={config}
                                                        imageSliderId={item.ImageSliderId}
                                                    />
                                                </div>: <div />                                           
                                            }
                                        </div>
                                    </div>
                                    <div className="ui stackable grid" style={{marginTop: '-15px'}}>
                                        <div className="eleven wide column" style={{padding: '0px 0px 0px 14px'}}>
                                            <div className="tertiary-page-article-container">
                                                <h2 className="tertiary-page-article-title">
                                                    {item.Column1Title.toUpperCase()}
                                                </h2> 
                                                <span className="tertiary-page-article-subtitle">
                                                    {item.Column1SubTitle}
                                                </span> 
                                            </div>​​ 
                                            <div className="ui stackable grid">​ 
                                                <div className="sixteen wide column">
                                                    <div className="ui two column stackable grid">
                                                        <div className="column">
                                                            <div dangerouslySetInnerHTML={this.createMarkup(item.Column1Content)}>
                                                            </div>​​ 
                                                        </div>
                                                        <div className="column">
                                                            <div dangerouslySetInnerHTML={this.createMarkup(item.Column2Content)}>
                                                            </div>​​ 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>​                                            
                                        </div>
                                        <div className="five wide column" style={{paddingTop: '0px'}}>
                                            <div className="">
                                                {
                                                    (item && item.Banner1Id && banners && images) ? 
                                                    <div className="tertiary-page-banner-container">
                                                        <Banner items={banners} 
                                                            id={item.Banner1Id} 
                                                            context={context}
                                                            config={config}
                                                            images={images}
                                                            bannerHeight="150px"
                                                        />
                                                    </div>: <div />                                    
                                                }
                                                {
                                                    (item && item.Banner2Id && banners && images) ? 
                                                    <div className="tertiary-page-banner-container">
                                                        <Banner items={banners} 
                                                            id={item.Banner2Id} 
                                                            context={context}
                                                            config={config}
                                                            images={images}
                                                            bannerHeight="150px"
                                                        />
                                                    </div>: <div />                                  
                                                }  
                                                {
                                                    (item && item.Banner3Id && banners && images) ? 
                                                    <div className="tertiary-page-banner-container">
                                                        <Banner items={banners} 
                                                            id={item.Banner3Id} 
                                                            context={context}
                                                            config={config}
                                                            images={images}
                                                            bannerHeight="150px"
                                                        />
                                                    </div>: <div />                                   
                                                }   
                                            </div>                                               
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>                        
                    }
                </div>
            </div>
        )
    }
}