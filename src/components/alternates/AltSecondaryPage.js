import React, {Component} from 'react'
import '../../css/styles.css'
import '../../css/App.css'
import Alerts from '../widgets-components/Alerts'
import SecondaryPageTopBanner from '../widgets-components/SecondaryPageTopBanner'
import SecondaryPageLeftBanners from '../widgets-components/SecondaryPageLeftBanners'
import SecondaryPageContentLinks from '../widgets-components/SecondaryPageContentLinks'
import SecondaryPageSubmenu from '../widgets-components/SecondaryPageSubmenu'
import SecondaryPageContents from '../widgets-components/SecondaryPageContents'
import SecondaryPageVideo from '../widgets-components/SecondaryPageVideo'
import GlobalNavigationLeftSubmenu from '../widgets-components/GlobalNavigationLeftSubmenu'
import Loader from '../helpers/Loader'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {
    Business_Units_list_name,
    Secondary_Pages_list_name,
    Applications_list_name,
    Best_Practices_List_list_name,
    Templates_List_list_name,
    Manuals_List_list_name,
    Content_Links_list_name,
    Banners_list_name,
    Applications_Parents_list_name,
    Left_Navigation_list_name,
    Global_Navigation_list_name,
    Images_Document_Library_Name,
    Business_Units_Preference_list_name,
    Business_Unit_Quick_Links_list_name,
    Other_Airlines_list_name,
    IP_Whitelist_list_name
} from '../../constants'

export default class SecondaryPage extends Component{
    constructor(props){
        super(props)
        this.state = {
            content: 'APPLICATIONS'
        }
    }

    componentDidMount(){
        const vrd = (s) => s.replace(/\s/g,'_').toLowerCase()
        const {config,context} = this.props
        const url = getUrl(context)
		const clists = [
            Business_Units_list_name,
            Secondary_Pages_list_name,
            Applications_list_name,
            Best_Practices_List_list_name,
            Templates_List_list_name,
            Manuals_List_list_name,
            Content_Links_list_name,
            Banners_list_name,
            Applications_Parents_list_name,
            Left_Navigation_list_name,
            Global_Navigation_list_name,
            Images_Document_Library_Name,
            Business_Units_Preference_list_name,
            Business_Unit_Quick_Links_list_name,
            Other_Airlines_list_name,
            IP_Whitelist_list_name
        ]
        const lists = config.filter(f=>clists.includes(f.Title)).map(m=>m.Value)
        const configLists = {}
        config.filter(f=>clists.includes(f.Title)).forEach(m=>{
            configLists[vrd(m.Title)] = vrd(m.Value)
        })
        this.setState({lists: configLists})
        
        lists.forEach(l => {
            getSPListItems(url, l,config).then(rslt => {
                if(Array.isArray(rslt)){
                    this.setState({[vrd(l)]: rslt})
                } else {/** TODO */}
            })
        })
    }

    handleChangeContent = (group) => {
        this.setState({
            content: group
        })
    }

    createMarkup(richText){
        return {__html: richText}
    }

    render(){ 
        const vrd = (s) => s.replace(/\s/g,'_').toLowerCase()  
        const {config,context} = this.props   
        const {lists,content} = this.state
        const state = this.state

        const business_units = lists && state[lists[vrd(Business_Units_list_name)]]
        const secondary_pages = lists && state[lists[vrd(Secondary_Pages_list_name)]]
        const applications = lists && state[lists[vrd(Applications_list_name)]]
        const best_practices_list = lists && state[lists[vrd(Best_Practices_List_list_name)]]
        const templates_list = lists && state[lists[vrd(Templates_List_list_name)]]
        const manuals_list = lists && state[lists[vrd(Manuals_List_list_name)]]
        const content_links = lists && state[lists[vrd(Content_Links_list_name)]]
        const applications_parents = lists && state[lists[vrd(Applications_Parents_list_name)]]
        const left_navigation = lists && state[lists[vrd(Left_Navigation_list_name)]]
        const global_navigation = lists && state[lists[vrd(Global_Navigation_list_name)]]
        const banners = lists && state[lists[vrd(Banners_list_name)]]
        const images = lists && state[lists[vrd(Images_Document_Library_Name)]]
        const business_units_preferences = lists && state[lists[vrd(Business_Units_Preference_list_name)]]
        const business_unit_quick_links = lists && state[lists[vrd(Business_Unit_Quick_Links_list_name)]]
        const other_airlines = lists && state[lists[vrd(Other_Airlines_list_name)]]
        const ip_whitelist = lists && state[lists[vrd(IP_Whitelist_list_name)]]

        let queryString = document.location.href.split('SecondaryPage.aspx?')[1]
        let plist = queryString ? queryString.split('&'): []
        let params = {}
        plist.forEach(p => {
            let param = p.split('=')
            params[param[0].toLowerCase()] = decodeURIComponent(param[1])
        })

        const display = {
            [params.group]: business_units,
            [params.page]: secondary_pages
        }

        const query = params.group || params.page

        const unit = display[query] && display[query].find(i => i.Title.toLowerCase() === query.toLowerCase())

        // Change Document Title
        if(unit && unit.Title) document.title = unit.Title;

        // let contentProps = {
        //     applications: applications,
        //     best_practices: best_practices_list,
        //     templates: templates_list,
        //     manuals: manuals_list
        // }

        let contentProps = {}

        return(
            <div style={{width: '100%',maxWidth: '1550px',margin: 'auto'}}>
                {
                    <div className="ui stackable grid" style={{marginRight: '0px'}}>
                        <div className="sixteen wide column secondary-page-whole">
                            <div className="submenu-banner-video secondary-page-row">
                                <div className="ui stackable grid">
                                    <div className="three wide column" style={{paddingRight: '0px', paddingBottom: '0px'}}>
                                        <div className="secondary-page-submenu">
                                            {
                                                params.group ? 
                                                <div>
                                                    {
                                                        left_navigation ? 
                                                        <SecondaryPageSubmenu 
                                                            handleChangeContent={this.handleChangeContent} 
                                                            group={params.group} 
                                                            content={content} 
                                                            links={left_navigation}
                                                            contentProps={contentProps}
                                                            images={images}
                                                            context={context}
                                                            config={config}
                                                        />: <Loader height={'250px'} />
                                                    }
                                                    {
                                                        global_navigation && left_navigation && 
                                                        <div style={{
                                                            marginTop: '-28px'
                                                        }}><GlobalNavigationLeftSubmenu 
                                                            items={global_navigation} 
                                                            page={params.page} 
                                                            links={left_navigation}
                                                            images={images}
                                                            context={context}
                                                            config={config}
                                                            group={params.group}
                                                        /></div>
                                                    }
                                                </div>:
                                                <div>
                                                    {
                                                        global_navigation && left_navigation?
                                                        <GlobalNavigationLeftSubmenu 
                                                            items={global_navigation} 
                                                            page={params.page} 
                                                            links={left_navigation}
                                                            images={images}
                                                            context={context}
                                                            config={config}
                                                        />:
                                                        <Loader height={'250px'} />
                                                    }
                                                </div>
                                            }
                                        </div>
                                    </div>
                                    <div className="thirteen wide column" style={{marginLeft: '-15px'}}>
                                        <div className="ui stackable grid">
                                            <div className="ten wide column" style={{paddingRight: '10px', paddingBottom: '0px'}}>
                                                <div className="secondary-page-top-banner">
                                                    {   unit ? 
                                                        <SecondaryPageTopBanner 
                                                            unit={unit} 
                                                            images={images}
                                                            context={context}
                                                            config={config}
                                                        />:
                                                        <Loader height={'250px'} /> }                                            
                                                </div>
                                            </div>
                                            <div className="six wide column" style={{paddingLeft: '5px', paddingRight: unit && unit.Image_SliderId ? '0px':'10px', paddingBottom: '0px'}}>
                                                <div className="secondary-page-video">
                                                    {   !unit ? <Loader height={'250px'} />:
                                                        <SecondaryPageVideo 
                                                            unit={unit} 
                                                            images={images} 
                                                            context={context}
                                                            config={config}
                                                        />
                                                    }
                                                </div>
                                            </div>
                                        </div> 
                                        <div className="ui stackable grid">
                                            <div className="sixteen wide column">
                                                <div className="secondary-page-alerts" style={{marginRight: '-15px'}}>
                                                    <div className="secondary-page-alerts" style={{marginTop:' -15px'}}>
                                                        { images && <Alerts context={context} config={config} images={images}/> }
                                                    </div>				
                                                </div>
                                            </div>
                                        </div>                                  
                                    </div>
                                </div>	
                            </div>
                            <div className="banner-application-banner secondary-page-row" style={{marginTop:'-35px'}}>
                                <div className="ui stackable grid">
                                    <div className="three wide column"  style={{paddingRight: '15px'}}>
                                        <div className="secondary-page-left-banners">
                                            {
                                                (unit && banners && images) ? 
                                                <SecondaryPageLeftBanners unit={unit} 
                                                    bannerItems={banners} 
                                                    images={images}
                                                    context={context}
                                                    config={config}
                                                />
                                                : <Loader height={'400px'} />
                                            }
                                        </div>
                                    </div>
                                    <div className="eight wide column"  style={{paddingRight: '0px', marginLeft: '-15px'}}>
                                        <div className="secondary-page-application">
                                            {
                                                params.group ? 
                                                unit && applications_parents &&
                                                business_units_preferences && 
                                                business_unit_quick_links && 
                                                other_airlines && 
                                                ip_whitelist && 
                                                <SecondaryPageContents contentType={content} 
                                                    applicationsParentsItems={applications_parents}
                                                    businessUnit={unit}
                                                    contentProps={contentProps}
                                                    images={images}
                                                    context={context}
                                                    config={config}
                                                    buPreferences={business_units_preferences}
                                                    quickLinks={business_unit_quick_links}
                                                    otherAirlines={other_airlines}
                                                    ipWhitelist={ip_whitelist}
                                                />: 
                                                unit && 
                                                <div dangerouslySetInnerHTML={this.createMarkup(unit.Page_Content)}
                                                    className="tertiary-page-article-content"
                                                />
                                            }
                                        </div>
                                    </div>
                                    <div className="five wide column"  style={{paddingRight: '0px'}}>
                                        <div className="secondary-page-right-banners">
                                            { unit && content_links?
                                                <SecondaryPageContentLinks 
                                                    links={content_links} 
                                                    unit={unit}
                                                    images={images}
                                                    context={context}
                                                    config={config}
                                                />
                                                : <Loader height={'200px'} /> }
                                        </div>
                                    </div>
                                </div>				
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}