import React,{Component} from 'react'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import {updateSPListItems} from '../helpers/UpdateSPListItems'
import {
	Quick_Polls_list_name,
	Icons_list_name
} from '../../constants'

export default class Poll extends Component{
	constructor(props){
		super(props)
		this.state = {
			active: 0
		}
    }

    componentDidMount(){
		this.getitems()
	}

	getitems = () => {
		const {config, context} = this.props
		const clists = [Quick_Polls_list_name,Icons_list_name]
		const lists = config.filter(f=>clists.includes(f.Title)).map(m=>m.Value)
		const url = getUrl(context)
		lists.forEach(l => {
			getSPListItems(url, l,config).then(rslt => {
				if(Array.isArray(rslt)){
					this.setState({
						[l.replace(/\s/g,'_').toLowerCase()]: rslt
					})
				} else {/** TODO */}
			})
		})
    } 
    
    handleVote = (id, data) => {
		const {config, context} = this.props
		const list = config.find(f=>f.Title===Quick_Polls_list_name).Value
		const url = getUrl(context) + "/_api/web/lists/GetByTitle('"+ list +"')/GetItemById("+ id +")"
		updateSPListItems(url, data).then(ok => ok && this.getitems())
	}
}