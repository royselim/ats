import React,{Component} from 'react'
import '../../css/alt.css'
import {
	Internal_Career_Opportunities_Popup_text as popupText,
	Footer_Links_URLs_with_popup as urls
} from '../../constants'

export default class FooterLinks extends Component {
	handleClick = (linkId,url) => {
		const {config} = this.props
		const txt = config.find(f=>f.Title === popupText).Value
		const purls = config.find(f=>f.Title === urls).Value
		if(purls.includes(url))
			alert(txt)
		document.getElementById(linkId).click()
	}

	render(){
		const { link, url, opens, context } = this.props
		const linkId = link.replace(/\s/g,'-').toLowerCase()
		return (
			<div id="footer-widget">
				{
					link !== 'Global Preferences' ? 
					<div>
						<div className="footer-links-link">
							<div style={{cursor: 'pointer'}} onClick={() => this.handleClick(linkId,url)}>{link}</div>
							<a id={linkId} 
								href={url} 
								style={{display:'none'}} 
								target={opens === 'new tab' ? '_blank': '_self'}
							>
								{link}
							</a>
						</div>
					</div>:
					<div>
						{
							context.isSiteAdmin && 
							<div>
								<div className="footer-links-link">
									<a href={url} target={opens === 'new tab' ? '_blank': '_self'}>{link}</a>
								</div>
							</div>
						}
					</div>
				}
			</div>
		)	
	}
}