import React, {Component} from 'react'
import '../../css/styles.css'
import '../../css/App.css'
import Alerts from '../widgets-components/Alerts'
import SecondaryPageLeftBanners from '../widgets-components/SecondaryPageLeftBanners'
import SecondaryPageSubmenu from '../widgets-components/SecondaryPageSubmenu'
import GlobalNavigationLeftSubmenu from '../widgets-components/GlobalNavigationLeftSubmenu'
import ContentWidget from '../widgets-components/ContentWidget'
import Loader from '../helpers/Loader'
import {getSPListItems,getUrl} from '../helpers/GetSPListItems'
import AtsAcademyRightLinks from '../widgets-components/AtsAcademyRightLinks';
import {
    Business_Units_list_name,
    Secondary_Pages_list_name,
    Applications_list_name,
    Best_Practices_List_list_name,
    Templates_List_list_name,
    Manuals_List_list_name,
    Banners_list_name,
    Left_Navigation_list_name,
    Global_Navigation_list_name,
    Images_Document_Library_Name,
    Short_Contents_list_name,
    Secondary_Page_Content_Links_list_name,
    React_Styles_list_name,
    Payroll_Calendar_Site,
    Payroll_Calendar_Title
} from '../../constants';
import EmergencyContacts from '../EmergencyContacts';
import PillarsTopRightLink from '../widgets-components/PillarsTopRightLink';

export default class AboutYou extends Component{
    constructor(props){
        super(props)
        this.state = {}
    }

    componentDidUpdate(){
        const {payroll} = this.state
        const ynp = document.getElementById('yourNextPaycheck')
        if(ynp && payroll && payroll[0]) {
            const pDdate = new Date(payroll[0].EventDate)
            pDdate.setHours(pDdate.getHours() + pDdate.getTimezoneOffset()/60);
            ynp.innerText = pDdate.toString().slice(0,15)
        }
    }

    componentDidMount(){
        const vrd = (s) => s.replace(/\s/g,'_').toLowerCase()
        const {config,context} = this.props
        const url = getUrl(context)
		const clists = [
            Business_Units_list_name,
            Secondary_Pages_list_name,
            Applications_list_name,
            Best_Practices_List_list_name,
            Templates_List_list_name,
            Manuals_List_list_name,
            Banners_list_name,
            Left_Navigation_list_name,
            Global_Navigation_list_name,
            Images_Document_Library_Name,
            Short_Contents_list_name,
            Secondary_Page_Content_Links_list_name,
            React_Styles_list_name
        ]
        const lists = config.filter(f=>clists.includes(f.Title)).map(m=>m.Value)
        const configLists = {}
        config.filter(f=>clists.includes(f.Title)).forEach(m=>{
            configLists[vrd(m.Title)] = vrd(m.Value)
        })
        this.setState({lists: configLists})
        
        lists.forEach(l => {
            getSPListItems(url, l,config).then(rslt => {
                if(Array.isArray(rslt)){
                    this.setState({[vrd(l)]: rslt})
                } else {/** TODO */}
            })
        })

        // Get the Payroll Dates
        const options = {
            headers: {"accept": "application/json; odata=verbose"},
            credentials: 'include'
        }
		const payUrl = config.find(c=>c.Title===Payroll_Calendar_Site).Value
        const payList = config.find(c=>c.Title===Payroll_Calendar_Title).Value
        const today = new Date().toISOString()
        const query = "$select=EventDate&$filter=EventDate gt '" + today + "'&$orderby=EventDate&$top=1"

        fetch(payUrl + "/_api/web/lists/GetByTitle('"+payList+"')/items?" + query, options)
        .then(res=>{
            if(res.ok){
                res.json().then(data => this.setState({payroll: data.d.results}))
            } else {
                /** TODO */
            }
        })
    }

    getImage = (id) => {
        const {context} = this.props
        const {images} = this.state
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
	}

    handleChangeContent = (group) => {
        this.setState({
            content: group
        })
    }

    createMarkup(richText){
        return {__html: richText}
    }

    render(){ 
        const vrd = (s) => s.replace(/\s/g,'_').toLowerCase()  
        const {config,context} = this.props   
        const {lists,content} = this.state
        const state = this.state

        const business_units = lists && state[lists[vrd(Business_Units_list_name)]]
        const secondary_pages = lists && state[lists[vrd(Secondary_Pages_list_name)]]
        const applications = lists && state[lists[vrd(Applications_list_name)]]
        const best_practices_list = lists && state[lists[vrd(Best_Practices_List_list_name)]]
        const templates_list = lists && state[lists[vrd(Templates_List_list_name)]]
        const manuals_list = lists && state[lists[vrd(Manuals_List_list_name)]]
        const left_navigation = lists && state[lists[vrd(Left_Navigation_list_name)]]
        const global_navigation = lists && state[lists[vrd(Global_Navigation_list_name)]]
        const banners = lists && state[lists[vrd(Banners_list_name)]]
        const images = lists && state[lists[vrd(Images_Document_Library_Name)]]
        const short_contents = lists && state[lists[vrd(Short_Contents_list_name)]]
        const secondary_page_content_links = lists && state[lists[vrd(Secondary_Page_Content_Links_list_name)]]
        const react_styles = lists && state[lists[vrd(React_Styles_list_name)]]

        let queryString = document.location.href.split('PillarsTemplate.aspx?')[1]
        let plist = queryString ? queryString.split('&'): []
        let params = {}
        plist.forEach(p => {
            let param = p.split('=')
            params[param[0].toLowerCase()] = decodeURIComponent(param[1])
        })

        const display = {
            [params.group]: business_units,
            [params.page]: secondary_pages
        }

        const query = params.group || params.page
        const unit = display[query] && display[query].find(i => i.Title.toLowerCase() === query.toLowerCase());

        // Change the document title.
        if(unit && unit.Title) {
            document.title = unit.Title
        }

        const content_widgets = short_contents && unit && short_contents.filter(f => f.Related_PageId === unit.Id)        
        content_widgets && secondary_page_content_links && content_widgets.forEach(c => 
            c['links'] = secondary_page_content_links.filter(l => l.Short_ContentId === c.Id)            
        )

        let contentProps = {
            applications: applications,
            best_practices: best_practices_list,
            templates: templates_list,
            manuals: manuals_list
        }

        const forSlide = images && {images, context, config}
        const bannerUrl = unit && this.getImage(unit.Top_Banner_Image2Id)

        return(
            <div style={{width: '100%',maxWidth: '1550px',margin: 'auto'}}>
                {
                    <div className="ui stackable grid" style={{marginRight: '0px', marginBottom: '-70px'}}>
                        <div className="sixteen wide column secondary-page-whole">
                            <div className="submenu-banner-video secondary-page-row">
                                <div className="ui stackable grid">
                                    <div className="three wide column" style={{paddingRight: '0px', paddingBottom: '0px'}}>
                                        <div className="secondary-page-submenu">
                                            {
                                                params.group ? 
                                                <div>
                                                    {
                                                        left_navigation ? 
                                                        <SecondaryPageSubmenu 
                                                            handleChangeContent={this.handleChangeContent} 
                                                            group={params.group} 
                                                            content={content} 
                                                            links={left_navigation}
                                                            contentProps={contentProps}
                                                            images={images}
                                                            context={context}
                                                            config={config}
                                                        />: <Loader height={'250px'} />
                                                    }
                                                </div>:
                                                <div>
                                                    {
                                                        global_navigation && left_navigation?
                                                        <GlobalNavigationLeftSubmenu 
                                                            items={global_navigation} 
                                                            page={params.page} 
                                                            links={left_navigation}
                                                            images={images}
                                                            context={context}
                                                            config={config}
                                                        />:
                                                        <Loader height={'250px'} />
                                                    }
                                                </div>
                                            }
                                        </div>
                                    </div>
                                    <div className="thirteen wide column" style={{marginLeft: '-15px'}}>
                                        <div className="ui stackable grid">
                                            <div className="ten wide column" style={{paddingRight: '10px', paddingBottom: '0px'}}>
                                                <div className="secondary-page-top-banner"
                                                    style={{
                                                        backgroundImage: 'url("'+ bannerUrl +'")',
                                                        backgroundSize: 'cover',
                                                        backgroundPosition: 'center center',
                                                        backgroundRepeat: 'no-repeat',
                                                        height: '250px',
                                                        margin: '0',
                                                        borderRadius: '5px'
                                                    }}                                                
                                                >
                                         
                                                </div>
                                            </div>
                                            <div className="six wide column" style={{paddingLeft: '5px', paddingRight: unit && unit.Image_SliderId ? '0px':'10px', paddingBottom: '0px'}}>

                                                <EmergencyContacts
                                                    images={images} 
                                                    context={context}
                                                    config={config} 
                                                />
                                                {/* This is temporarily a static material, will be replaced by dynamic data */}
                                                <PillarsTopRightLink
                                                    firstTxt="LOOKING FOR TRAINING MATERIALS?"
                                                    secondTxt="Plateau CBT in the top navigation, above, or"
                                                    thirdTxt ="visit the ATS Academy Page"
                                                />
                                            </div>
                                        </div> 
                                        <div className="ui stackable grid">
                                            <div className="sixteen wide column">
                                                <div className="secondary-page-alerts" style={{marginRight: '-15px'}}>
                                                    <div className="secondary-page-alerts" style={{marginTop:' -15px'}}>
                                                        { images && <Alerts context={context} config={config} images={images}/> }
                                                    </div>				
                                                </div>
                                            </div>
                                        </div>                                  
                                    </div>
                                </div>	
                            </div>
                            <div className="banner-application-banner secondary-page-row" style={{marginTop:'-35px'}}>
                                <div className="ui stackable grid">
                                    <div className="three wide column"  style={{paddingRight: '15px'}}>
                                        <div className="secondary-page-left-banners">
                                            {
                                                (unit && banners && images) ? 
                                                <SecondaryPageLeftBanners unit={unit} 
                                                    bannerItems={banners} 
                                                    images={images}
                                                    context={context}
                                                    config={config}
                                                />
                                                : <Loader height={'400px'} />
                                            }
                                        </div>
                                    </div>
                                    <div className="thirteen wide column" style={{paddingRight: '0px'}}>
                                        {
                                            content_widgets && react_styles && forSlide &&
                                            content_widgets.filter(w=>w.Location==='top').map((c,i) => 
                                                <div style={{marginLeft: '-15px', marginRight: '15px'}}><ContentWidget 
                                                    key={i} 
                                                    widget={c}
                                                    addClass={{}}
                                                    addStyle={react_styles.find(f=>f.Id===c.Custom_StyleId)}
                                                    getImage={this.getImage}
                                                    forSlide={forSlide}
                                                /></div>
                                            )                                            
                                        }
                                        <div className="ui stackable grid">
                                            <div className="ten wide column"  style={{paddingRight: '15px', marginLeft: '-15px'}}>
                                                <div className="ui stackable grid">
                                                    {
                                                        content_widgets && react_styles && forSlide &&
                                                        content_widgets.filter(w=>w.Location==='left' && w.Width === 'Half').map((c,i) => 
                                                            <div className="eight wide column"><ContentWidget 
                                                                key={i} 
                                                                widget={c}
                                                                addClass={{}}
                                                                addStyle={react_styles.find(f=>f.Id===c.Custom_StyleId)}
                                                                getImage={this.getImage}
                                                                forSlide={forSlide}
                                                            /></div>
                                                        )                                                        
                                                    }
                                                </div>
                                                <div>
                                                    {
                                                        content_widgets && react_styles && forSlide &&
                                                        content_widgets.filter(w=>w.Location==='left' && w.Width==='Full').map((c,i) => 
                                                            <ContentWidget 
                                                                key={i} 
                                                                widget={c}
                                                                addClass={{}}
                                                                addStyle={react_styles.find(f=>f.Id===c.Custom_StyleId)}
                                                                getImage={this.getImage}
                                                                forSlide={forSlide}
                                                                leftImageBgSize="contain"
                                                            />
                                                        )
                                                    }
                                                </div>
                                            </div>
                                            <div className="six wide column"  style={{paddingRight: '0px',marginLeft: '-15px'}}>
                                                <div>
                                                    {
                                                        content_widgets && react_styles && forSlide &&
                                                        content_widgets.filter(w=>w.Location==='right').map((c,i) => 
                                                            <ContentWidget 
                                                                key={i} 
                                                                widget={c}
                                                                addClass={{textAlign: 'center'}}
                                                                addStyle={react_styles.find(f=>f.Id===c.Custom_StyleId)}
                                                                getImage={this.getImage}
                                                                forSlide={forSlide}
                                                            />
                                                        )
                                                    }
                                                    {
                                                        // EXCEPTIONS - special additions 
                                                        params.page.toLowerCase() === "about ats" && images &&
                                                        // <div>Hello World</div>
                                                        <AtsAcademyRightLinks 
                                                            config={config}
                                                            context={context}
                                                            images={images}
                                                        />
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>				
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}