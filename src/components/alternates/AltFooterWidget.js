import React from 'react'
import '../../css/alt.css'

const FooterWidget = ({ header, cityState, zip, address, country, locations }) => {
    
    return (
        <div className="column">
            <div id="footer-widget">
                <div>
                    <div className="footer-widget-header">{header || ''}</div>
                    <div>{address || ''}</div>
                    <div>{cityState || ''}</div>
                    <div>{zip || ''}</div>
                    <div>{country || ''}</div>
                    <br />
                    <a href={locations} target="_blank" style={{fontWeight: 'bold'}}>Other ATS Locations</a>
                </div>
            </div>
        </div> 
    )
}

export default FooterWidget