import React, { Component } from 'react'

export default class AltAlertDetailsScroll extends Component {
	getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
	}

	render() {
		const {icons,item} = this.props
		let ityp = item && item.Alert_type
		let leftIconTitle = 'Alert left arrow button'
		leftIconTitle += ityp === 'warning' ? ' white': '' 
		let rightIconTitle = 'Alert right arrow button'
		rightIconTitle += ityp === 'warning' ? ' white': '' 

		let leftIconId = icons && icons.find( i => i.Title === leftIconTitle).Icon2Id
		let rightIconId = icons && icons.find( i => i.Title === rightIconTitle).Icon2Id

		const leftIconUrl = item && leftIconId && this.getImage(leftIconId)
		const rightIconUrl = item && rightIconId && this.getImage(rightIconId)

		return (
			<div>
				<div className="ui three column grid">
					<div className="two wide column row grid"
						onClick={this.props.handlePrev}
						style={{marginLeft: '0', marginRight: '0'}}
					>
						<img src={leftIconUrl} alt="" style={{display: 'block', height: '50%', maxHeight: '24px', width: '50%', maxWidth: '24px', margin: 'auto', padding: '0 0'}} />
					</div>
					
					<div className="twelve wide column centered row grid" style={{textAlign: 'left'}}>
						<span>
							<a href={(item && item.Link.Url) || ''} 
								className={ityp === 'warning'? 'alert-headline-warning': 'alert-headline'}>
								{item && item.Title.toUpperCase()}
							</a>  
						</span> 						
						<p className={ityp === 'warning'? 'alert-details-warning': 'alert-details'}>
							{item && item.Details}
						</p>						          
					</div>

					<div className="two wide column row grid"
						onClick={this.props.handleNext}
						style={{marginLeft: '0', marginRight: '0'}}
					>
						<img src={rightIconUrl} alt="" style={{display: 'block', height: '50%', maxHeight: '24px', width: '50%', maxWidth: '24px', margin: 'auto', padding: '0 0'}} />
					</div>					
				</div>        
			</div>
		)
	}
}