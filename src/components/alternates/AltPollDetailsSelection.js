import React, { Component } from 'react'

export default class AltPollDetailsSelection extends Component {
	constructor(props){
		super(props)
		this.state = {}
	}

	handleChange = (option) => {
		this.setState({
			option: option
		})
	}

	handleVote = ( e ) => {	
		e.preventDefault()
		e.stopPropagation()

		let {
			context,
			item,
			vote
		} = this.props

		let data = {
			__metadata: { 'type': 'SP.Data.Quick_x0020_PollListItem' }
		}

		let column = null
		const dateVoted = new Date().valueOf()
		const username = context.userEmail.split('@')[0]
		const newData = `${username}|${dateVoted}`
		for(var n in item){
			if(item[n] === this.state.option){
				column = n + 'Votes'
				data[column] = item[column] ? item[column] + ',' + newData : newData
			}
		}

		column && vote(item.Id, data);
	}

    render() {
		let opt = this.state.option
		let { item } = this.props
		let Options = [
			item.Option1,
			item.Option2,
			item.Option3,
			item.Option4,
			item.Option5
		].filter(item => item !== null)
		return (
			<div id="poll-form" className="ui form">
				<div className="grouped fields">
					<label>
						<h4 style={{color: item.FontColor}}>{item.Title}</h4>
					</label>
					{
						Options.map((option, index) => {
							return (
								<div key={index} className="field">
									<div className="ui radio checkbox" onClick={() => this.handleChange(option)}>
										<input type="radio" name={`Option${option.Id}`} checked={opt === option}/>
										<label><span style={{color: item.FontColor}}>{option}</span></label>
									</div>
								</div>								
							)
						})
					}
				</div>
				<button className="ui button blue" onClick={(e)=>this.handleVote(e)}>VOTE</button>
			</div>
		)
    }
}