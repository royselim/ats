import React, { Component } from 'react'
import AltPollDetailsSelection from './AltPollDetailsSelection';
import {Quick_Poll_label,Quick_Poll_Thank_You_text} from '../../constants';
import Chart from 'react-google-charts';

export default class AltPollDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			displayPollResult: false,
		};
	}

	toggleDisplayPollResult = () => {
		this.setState((state) => {
			return {
				displayPollResult: !state.displayPollResult
			}
		});
	}

	getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
	}

	render() {
		const {icons,config} = this.props
		const {displayPollResult} = this.state
		const icon = icons.find(i => i.Title === 'Poll Icon').Icon2Id
		const { item, context, vote } = this.props
		const results = [
			['', ''],
			[item.Option1, item.Option1Votes ? item.Option1Votes.split(',').length: 0],
			[item.Option2, item.Option2Votes ? item.Option2Votes.split(',').length: 0],
			[item.Option3, item.Option3Votes ? item.Option3Votes.split(',').length: 0],
			[item.Option4, item.Option4Votes ? item.Option4Votes.split(',').length: 0],
			[item.Option5, item.Option5Votes ? item.Option5Votes.split(',').length: 0],
		]
		const filteredRes = results.filter((itm) => typeof itm[0] === 'string');

		const pollIconUrl = icon && this.getImage(icon)
		const pollImageUrl = item && item.Background2Id && this.getImage(item.Background2Id)

		const qpLabel = config.find(f=>f.Title === Quick_Poll_label).Value
		const thankYou = config.find(f=>f.Title === Quick_Poll_Thank_You_text).Value

		return (
			<div id="poll-content" className="ui grid"
				style={{
					backgroundImage: pollImageUrl && !displayPollResult? 'url("'+ pollImageUrl +'")' : 'none',
					backgroundSize: 'cover',
					backgroundPosition: 'center center',
					padding: '15px ',
					borderRadius: '5px',
					border: '1px solid #a9a9a9'
				}}  
			>
				{
					!item.Voted ?
					<div className="sixteen wide column">
						<div className="ui grid">
							<div className="three wide column centered quick-poll-title-choices"
								style={{padding: '0px', 
									display: 'flex', 
									flexDirection: 'column', 
									justifyContent: 'space-between'}}
							>
								<h3 className="" style={{color: item.FontColor}}>{qpLabel}</h3>
								<img src={pollIconUrl} 
										onClick={() => {this.toggleDisplayPollResult()}} 
										alt="Poll Graph" 
										style={{
											width: '80%', 
											minWidth: '80px', 
											cursor: 'pointer'
											}
										}
								/>
							</div>
							<div className="thirteen wide column quick-poll-title-choices">
								<div className="ui widgets row grid">
									{displayPollResult ? 
										<Chart
										width={'100%'}
										height={'100%'}
										chartType="BarChart"
										loader={<div>Loading Chart</div>}
										data={filteredRes || []}
										options={{
										  // Material design options
										  legend: 'none',
										//   chart: {
										// 	title: (item.Title || '').slice(0, 40) + ' ...',
										// 	subtitle: 'hover over the bar to see more details.',
										//   },
										}}
										// For tests
										rootProps={{ 'data-testid': '2' }}
									  /> : 
										<AltPollDetailsSelection item={item} context={context} vote={vote}/>
									}
								</div>
							</div>
						</div>
						<div className="ui grid" style={{marginTop: -15, marginLeft: -25, padding: 0, cursor: 'pointer'}}>
							<div className="sixteen wide column">
								<span onClick={() => {this.toggleDisplayPollResult()}}>
									{`Click here ${this.state.displayPollResult ? 'to show poll' : 'for results'}`}
								</span>
							</div>
						</div>
					</div>:
					<div className="sixteen wide column centered quick-poll-voted">
						<center>{thankYou}</center>
						{
							item[item.Answer] ? <div style={{padding: 20}}>
								<span style={{fontSize: 16}}>The correct answer is: </span>
								<span style={{fontSize: 20, fontWeight: 'bold', color: '#054C02'}}>{item[item.Answer]}</span>
							</div>: <div />
						}
					</div>
				}
			</div>
		)
	}
}