import React, {Component} from 'react'
import {getUrl} from '../helpers/GetSPListItems'
import {getSPListFields} from '../helpers/GetSPListFields'
import {Business_Units_Preference_list_name} from '../../constants'
import semanticConverter from '../helpers/semanticUIConverter'

export default class BusinessUnits extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }

    componentDidMount(){
        const {context, config,setDefault} = this.props
        const url = getUrl(context)
        const list = config && config.find(i => i.Title === Business_Units_Preference_list_name).Value

        getSPListFields(url, list,'&$select=Title').then(rslt => {
            if(Array.isArray(rslt)){
                setDefault(rslt[0].Title)
                this.setState({fields: rslt})
            } else {/** TODO */}
        })

        window.addEventListener("resize", this.updateWidth)
    }

    updateWidth = () => {
        var itms = document.getElementsByClassName('bu-quick-links')
        this.setState({itemWidth: itms[1] && itms[1].clientWidth})
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.updateWidth)
    }

    render(){
        const {fields, itemWidth} = this.state
        const cem = itemWidth >= 100 ? 1: itemWidth/100
        const {context, items, defaultBu, changeGroup} = this.props

        const busFields = items && items.filter(i => i.Title === context.userEmail)[0]
        const bus = busFields && Object.keys(busFields).filter(b => 
            typeof busFields[b] === 'boolean' && busFields[b]).map(m => m.replace(/_x0020_/g, ' '))
        
        const links = bus || ( fields && fields.map(f => f.Title) )

        return (
            <div className={`ui ${semanticConverter[links && links.length]} item menu`}>
                {
                    links && links.map((l,x)=>
                        <a key={x} className={`item ${defaultBu !== l ? 'active': ''} bu-quick-links`} 
                            style={{
                                fontWeight: defaultBu === l ? 'bold': 'normal',
                                color: '#155781', 
                                overflow: 'hidden', 
                                fontSize: `${cem.toString()}em`
                            }}
                            onClick={()=>{changeGroup(l)}}>{l.toUpperCase()}
                        </a>
                    )
                }
            </div>
        )
    } 
}