import React, { Component } from 'react'
import AltAlertDetailsScroll from './AltAlertDetailsScroll';

class AltAlertDetails extends Component {
	constructor(props){
		super(props)
		this.state = {
			currentItem: 0
		}
	}

	changeAlert = () => {
		let alertItems = this.props.alertItems
        let keys = alertItems.map((i,x) => x)
        let currentIndex = keys.indexOf(this.state.currentItem)
        keys.push(keys.shift());
        this.setState({
            currentItem: keys[currentIndex]
        })
	}
	
	componentDidMount(){
		const {timeInterval} = this.props;

        this.timerID = setInterval(
            () => this.changeAlert(),
            (timeInterval || 5000)
        )
	}

	handleNext = () => {
		this.setState((state, props) => {
			return {
				currentItem: state.currentItem < props.alertItems.length - 1 ? 
					state.currentItem + 1 : 0
			}
		})
	}

	handlePrev = () => {
		this.setState((state, props) => {
			return {
				currentItem: state.currentItem > 0 ? 
					state.currentItem - 1 : props.alertItems.length - 1
			}
		})
	}

	getImage = (id) => {
		const {images,context} = this.props
		const i = images && images.find(f => f.Id === id)
		const m = i && i.FieldValuesAsText.FileRef
		return (m && context.siteAbsoluteUrl.replace(context.webServerRelativeUrl,m)) || null
	}

	render() {
		const {images,context} = this.props
		let item = this.props.alertItems[this.state.currentItem]
		let icons = this.props.icons
		let alertText = this.props.alertText
		let ityp = item && item.Alert_type
		let iconTitle = ityp === 'warning' ? 'Alert main white': 'Alert main'
		let alertIcon = icons.find( i => i.Title === iconTitle )
		const alertIconUrl = alertIcon && this.getImage(alertIcon.Icon2Id)

		return (
			<div id={`alert-widget-${ityp}`} 
                className="ui grid"
                style={{borderRadius: '5px', marginTop: '15px !important'}}
			>
				<div className="three wide column text-center" style={{margin: '-10px', minWidth: '80px !important'}}>
                    <div>
                        <div>
                            <span style={{color: ityp === 'warning' ? 'white': 'black'}} className="secondary-page-alert">{alertText}</span>
                        </div>
                        <div>
                            <img src={alertIconUrl} alt="" />
                        </div>        
                    </div>
				</div>
				<div className="thirteen wide column" style={{borderLeft: '1px solid black'}}>
                    <div>
						<AltAlertDetailsScroll item={item} 
							handleNext={this.handleNext} 
							handlePrev={this.handlePrev} 
							icons={icons}
							ityp={ityp}
							images={images}
							context={context}
						/>   
					</div>  
				</div>
			</div> 
		)
	}
}

export default AltAlertDetails
