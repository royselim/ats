import React, {Component} from 'react'

export default class AltOtherFilter extends Component {
    handleChange = (e) => {
        const {change} = this.props
        change(e.target.value)
    }

    render(){
        const {otherAirlines,selected,changeLetter,content,parentId,otherLetter} = this.props
        const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
        const fl = content.filter(f=>f.ParentId === parentId).map(o => o.Title.charAt(0))
        return <div>
            <div>
                <select style={{padding: '5px', width: '100%'}} 
                    onChange={this.handleChange} 
                    className="ui selection dropdown" 
                    value={selected}
                >
                    <option value="0">Select a company</option>
                    {
                        otherAirlines.map((o,x)=><option key={x} value={o.Id}>{o.Title}
                        </option>)
                    }
                </select>
            </div>
            <div style={{padding: '10px 0px 0px 5px'}} className="ui grid">
                <div className="column">
                    <span>{`doc starts with: `}</span>
                    {
                        alphabet.map((a,x)=>
                            {
                                const inAlpha = fl.includes(a)
                                const inLet = otherLetter === a
                                // const inAlpha = otherAirlines.find(f=>f.Title.charAt(0).toUpperCase()===a)
                                return <span key={x}
                                    onClick={() => inAlpha && changeLetter(a)}
                                    style={{
                                        fontWeight: inLet ? 'bold': 'normal',
                                        padding: '1px 5px', 
                                        textDecoration: inAlpha ? (inLet ? 'none': 'underline'): 'none',
                                        cursor: inAlpha ? 'pointer': 'default',
                                        color: inAlpha ? (inLet ? 'white': 'blue'): 'black',
                                        backgroundColor: inLet ? 'blue': 'white',
                                        borderRadius: '3px'
                                    }}>{a}
                                </span>
                            }
                        )
                    }
                    {/* <span>{`or`}</span> */}
                </div>
            </div>
        </div>
    }
}