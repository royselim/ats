import '../css/styles.css'
import React, {Component} from 'react'
import RightSideHero from './RightSideHero'
import '../css/App.css'
import EmergencyContacts from './EmergencyContacts'
import Poll from './widgets-components/Poll';
import Alerts from './widgets-components/Alerts';
import EventsCalendar from './widgets-components/EventsCalendar';
import DiversityCalendar from './widgets-components/DiversityCalendar';
import BlogSerp from './widgets-components/BlogSerp';
import News from './widgets-components/News';
import {Images_Document_Library_Name} from '../constants'
import {getSPListItems,getUrl} from './helpers/GetSPListItems'

export default class Content extends Component{
    constructor(props){
        super(props)
        this.state = {}
    }
    
    componentDidMount(){
        const url = getUrl(this.props.context)
        const {config} = this.props
        const imagesList = config.find(f=>f.Title === Images_Document_Library_Name).Value
        getSPListItems(url,imagesList,config).then(images=>{
            if(Array.isArray(images)){
                this.setState({images})
            } else {/** TODO */}
        })
    }

    render(){
        const {config,context} = this.props
        const {images} = this.state
        const e = React.createElement
        return (
            <div style={{width: '100%',maxWidth: '1550px',margin: 'auto'}} className="ui two column stackable grid">
                 <div className="ten wide column" style={{marginTop: '-15px',marginRight: '-15px'}}>
                    {[RightSideHero, BlogSerp, News].map((c,x) => 
                        images && <div key={x}>{e(c,{config,context,images})}</div>)}                   
                </div>
                <div className="six wide column" style={{marginTop: '-15px'}}>
                    {[EmergencyContacts,Alerts,Poll,EventsCalendar, DiversityCalendar].map((c,x) => 
                        images && <div key={x}>{e(c,{config,context,images})}</div>)}
                </div>
            </div>
        )
    }
}