import React, { Component } from 'react'

class Loader extends Component {
	render() {
		let specs = this.props
		
		return (
			<div className="ui segment" style={{height: specs.height}}>
				<div className="ui active dimmer" style={{backgroundColor: specs.bgColor}}>
				<div className={`ui ${specs.size} text loader`}>
					{
						specs.text ? specs.text: 'Fetching data...'
					}
				</div>
				</div>
			</div>
		)
	}
}

Loader.defaultProps = {
  height: '330px',
  bgColor: '#a7c1d1',
  size: 'medium'
}

export default Loader
