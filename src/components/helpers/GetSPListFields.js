const options = {
    headers: {"accept": "application/json; odata=verbose"},
    credentials: 'include'
}

export const getSPListFields = (url,title,filter='') => {
    const u = "/_api/web/lists/GetByTitle('" + title + 
        "')/fields?$filter=Hidden eq false and CanBeDeleted eq true"
    return fetch(url + u + filter, options).then(res => {
        if(res.ok){
            return res.json().then(data => data.d.results)
        } else {
            return res
        }
    })
}