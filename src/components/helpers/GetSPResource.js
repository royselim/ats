const options = {
    headers: {"accept": "application/json; odata=nometadata"},
    credentials: 'include'
}

export const getSPResource = (url) => {
    return fetch(url, options).then(res => {
        if(res.ok){
            return res.json().then(data => data)
        } else {
            return res
        }
    })
}