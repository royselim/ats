import sconfig from '../../siteConfig'

const options = {
    headers: {"accept": "application/json; odata=verbose"},
    credentials: 'include'
}

export const getSPListItems = (url,title,siteConfig=null,query='') => {
    const list = siteConfig && siteConfig.find(f=>f.Value === title).Title
    const listQuery = list && sconfig.find(f=>f.Title === list)

    const u = "/_api/web/lists/GetByTitle('"+ title +"')/items?"
    const q = (listQuery && listQuery.query) // || (l && l.query)
    return fetch(url + u + (q || '') + query, options).then(res => {
        if(res.ok){
            return res.json().then(data => data.d.results)
        } else {
            return Promise.reject('Error in the request.')
        }
    })
}

export const getUrl = (context = {}) => {
    const href = context.webAbsoluteUrl || window.location.href
    return href.includes('localhost')? '': href
}