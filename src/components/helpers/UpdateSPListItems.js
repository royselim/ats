export const updateSPListItems = (url, data = {}, method = 'MERGE') => {
    const options = {
        method: method,
        body: JSON.stringify(data),
        headers: {
            "accept": "application/json; odata=verbose",
            "content-type": "application/json;odata=verbose",
            "X-RequestDigest": document.getElementById('__REQUESTDIGEST').value,
            "IF-MATCH": "*"            
        },
        credentials: 'include'
    }
    
    return fetch(url, options).then(res => res.ok)
}