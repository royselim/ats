import React, {Component} from 'react'
import {getUrl,getSPListItems} from './helpers/GetSPListItems'
import {
    Home_ATS_News_list_name,
    Archive_Items_Per_Page_value as ipp
} from '../constants'

export default class Archive extends Component{
    constructor(props){
        super(props)
        this.state = {
            page: 1,
            pages: null,
            itemsPerPage: Number(props.config.find(f=>f.Title === ipp).Value),
            sortAsc: true,
            searchText: '[a-zA-Z0-9]'
        }
    }

    componentDidMount(){
        const {context,config} = this.props
        const {itemsPerPage} = this.state
        const url = getUrl(context)
        const list = config.find(f=>f.Title===Home_ATS_News_list_name).Value
        getSPListItems(url,list,config,' and Archived eq 1').then(items => {
            if(Array.isArray(items)){
                const pages = Math.ceil(items.length/itemsPerPage)
                this.setState({items, pages})
            } else { /** TODO */ }
        })
    }

    changePage = (e) => {
        this.setState({page: e.target.value})
    }

    reverseSort = () => {
        this.setState((state) => {
            return {
                sortAsc: !state.sortAsc
            }
        })
    }

    handleSearch = (e) => {
        this.setState({
            searchText: e.target.value,
            page: 1
        })
    }

    render(){
        const {items,page,itemsPerPage,sortAsc,searchText} = this.state

        const vItems = items && items.filter(i => {
            const regex = new RegExp(searchText, ['i'])
            return regex.test(i.Title)
        })

        const options = []
        let pages = vItems && Math.ceil(vItems.length/itemsPerPage)
        for(let i = 0; i < pages; i++){
            options.push(<option key={i} value={i+1}>{i+1}</option>)
        }
        const start = (page - 1) * itemsPerPage
        const end = start + itemsPerPage
        const visible = vItems && vItems.slice(start,end)

        visible && visible.sort((a,b)=>{
            const aDate = new Date(a.Published_Date)
            const bDate = new Date(b.Published_Date)
            return sortAsc ? aDate - bDate : bDate - aDate
        })

        return <div className="ui stackable grid">
            <div className="sixteen wide column" style={{margin: '20px 15px -5px 20px', minHeight: '450px'}}>
                <div style={{fontSize: '20px', fontWeight: 'bold'}}>ATS News Archives</div>
                <div style={{paddingTop: '10px'}}>
                    <table className="ui celled table">
                        <thead>
                            <tr>
                                <th>
                                    <span onClick={this.reverseSort} style={{cursor: 'pointer'}}>
                                        {`Published Date `}<i className={`chevron ${sortAsc ? 'up': 'down'} icon`} />                                        
                                    </span>
                                </th>
                                <th>
                                    {`Title  `}
                                    <span className="ui input">
                                        <input name="search" type="text" placeholder="Search..." onChange={this.handleSearch} />
                                    </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                visible && visible.map((i,x)=>
                                    {
                                        const pdate = i.Published_Date && new Date(i.Published_Date).toLocaleDateString()
                                        return <tr key={x} style={{paddingTop: '5px'}}>
                                            <td>
                                                <span>{pdate}</span>
                                            </td>
                                            <td>
                                                <a href={i.Link && i.Link.Url}>
                                                    {i.Title}
                                                </a>
                                            </td>
                                        </tr>
                                    }
                                )
                            }
                        </tbody>
                    </table>
                </div>
                <div style={{paddingTop: '10px'}}>
                    <span>{`page: `}</span>
                    <select value={page} onChange={this.changePage}>
                        {options}
                    </select>
                </div>
            </div>
        </div>
    }
}