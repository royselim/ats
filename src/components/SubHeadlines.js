import React from 'react'

const SubHeadLines = ({subHeadlinesItems}) => {
    let counter = 0
    let subHeadlines = {}
    subHeadlinesItems.forEach( s => {
        if( s.Active && counter < 5){
            subHeadlines['section' + s.Section] = <div style={{height: 'inherit', backgroundImage: "url('"+ s.Image.Url +"')"}}>
                <div className="sub-headlines-text sub-headlines-text-title">{s.Title}</div>
                <div className="sub-headlines-text sub-headlines-text-synopsis">{s.Synopsis}</div>
                <div className="sub-headlines-text">{s.Description}</div>
            </div>
            counter++
        }
    })

    return (
        <div className="container" style={{paddingTop: '15px'}}>
            <div className="row">
                <div className="col sub-headlines">{subHeadlines.section1}</div>
                <div className="col sub-headlines">{subHeadlines.section2}</div>
            </div>
            <div className="row" style={{paddingTop: '15px'}}>
                <div className="col sub-headlines">{subHeadlines.section3}</div>
                <div className="col sub-headlines">{subHeadlines.section4}</div>
            </div>
        </div>
    )
}

export default SubHeadLines