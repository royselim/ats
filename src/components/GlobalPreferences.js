import React, {Component} from 'react'
import Preferences from './widgets-components/Preferences'
import {getUrl,getSPListItems} from './helpers/GetSPListItems'
import {Global_Preferences_list_name} from '../constants'

export default class GlobalPreferences extends Component {
    constructor(props){
        super(props)
        this.state = {
            submitted: false,
            count: 0
        }
    }

    componentDidMount(){
        const {context,config} = this.props
        const url = getUrl(context)
        const list = config.find(f=>f.Title===Global_Preferences_list_name).Value
        getSPListItems(url,list,config).then(items => {
            if(Array.isArray(items)){
                items.forEach(i => {
                    getSPListItems(url,i.Title,config).then(litems => {
                        if(Array.isArray(litems)){
                            const stn = i.Title.replace(/\s/g, '_').toLowerCase()
                            this.setState({[stn]: litems, [stn + '_posted']: false})
                        }
                        else { /** TODO */ }
                    })
                })
                this.setState({items})
            } else { /** TODO */ }
        })
    }

    handlePost = (list) => {
        const stn = list.replace(/\s/g, '_').toLowerCase()
        this.setState({
            [`${stn}_posted`]: true
        })
        const forPost = Object.entries(this.state).filter(f=>f[0].includes('_posted')).filter(f=>!f[1])
        if(forPost.length === 0){
            window.location.assign(window.location.href)
        }
    }

    handleSubmit = () => {
        this.setState({
            submitted: true
        })
    }

    resetSubmitted = () => {
        this.setState({
            submitted: false
        })
    }

    handleReset = () => {
        window.location.assign(window.location.href)
    }

    render(){
        // console.dir(this.state)
        const {config,context} = this.props
        const {items} = this.state

        const list = config.find(f=>f.Title===Global_Preferences_list_name).Value

        const globalPrefList = {
            Title: list,
            items: items
        }

        let prefsListsTitles = items && items.map(m => m.Title)

        let prefsLists = prefsListsTitles && prefsListsTitles.map(p => {
            let items = this.state[p.replace(/\s/g, '_').toLowerCase()]
            return { Title: p, items: items }
        })

        // let allPosted = true
        // for(var n in this.state){
        //     if( this.state[`${n}_posted`] ){
        //         allPosted = this.state[`${n}_posted`] === true
        //     }
        // }

        return(
            <div style={{width: '100%',maxWidth: '600px',margin: 'auto',minWidth: '41vh'}}>
                <h1>Global Preferences</h1>
                <hr />
                {
                    prefsLists && prefsLists.map((p,x) => 
                        p.items && <Preferences key={x} 
                            list={p} 
                            type={globalPrefList.items.find(l => l.Title === p.Title).Preference_x0020_Type}
                            email={context.userEmail}
                            submitted={this.state.submitted}
                            reset={this.resetSubmitted}
                            post={this.handlePost}
                        />
                    )
                }
                <div style={{paddingTop: '10px'}}>
                    <input className="ui button primary" type="button" onClick={this.handleReset} value="Reset/Refresh" />
                    <input className="ui button primary" type="button" onClick={this.handleSubmit} value="Save All" />
                </div>
            </div>
        )
    }
}