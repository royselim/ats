import React, {Component} from 'react'

export default class AtsmNews extends Component{
    render(){
        let atsmNewsItems = this.props.atsmNewsItems
        let newsItems = atsmNewsItems.map( (n,i) => {
            return (
                <div key={i} style={{paddingBottom: '20px'}}>
                    <div style={{fontWeight: 'bold', fontSize: '15px'}}>{n.Title}</div>
                    <a href={n.Link.Url}><div style={{color: 'rgb(0,0,128)'}}>{n.Description}</div></a>
                </div>
            )
        })
        let evenNews = newsItems.filter((n,i) => i % 2 === 0 )
        let oddNews = newsItems.filter((n,i) => i % 2 === 1 )

        return (
            <div className="row" style={{padding: '20px'}}>
                <div className="col" style={{border: '1px solid #e2e2fd', backgroundColor: '#fdfdfd'}}>
                    <div className="row" style={{padding: '10px'}}>
                        <div className="col">
                            ATSM NEWS (MEMOS, ETC)
                        </div>
                    </div>
                    <div className="row" style={{padding: '10px'}}>
                        <div className="col">
                            {
                                evenNews
                            }
                        </div>
                        <div className="col">
                            {
                                oddNews
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}