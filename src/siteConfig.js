import * as constants from './constants'

const config = [
    {
        Title: constants.Alerts_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Active,Alert_type,Details,Link'
    },
    {
        Title: constants.Admin_Preferences_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,ASAP_x0020_Email,ATS_x0020_Safety_x0020_Report_x0'
    },
    {
        Title: constants.Applications_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Description,Link,ParentId,Active,Other_AirlinesId'
    },
    {
        Title: constants.Applications_Parents_list_name,
        query: '$select=Id,Title,Logo2Id'
    },
    {
        Title: constants.ATS_Clocks_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Location,Offset,Active,Format,UseSuffix'
    },
    {
        Title: constants.Home_ATS_News_list_name,
        query: '$select=Id,Title,Description,Link,Active,Archived,Published_Date&$orderby=Published_Date desc&$filter=Active eq 1'
    },
    {
        Title: constants.Banners_list_name,
        query: '$select=Id,Title,Image2Id,Link'
    },
    {
        Title: constants.Best_Practices_List_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Description,Link,ParentId,Other_AirlinesId'
    },
    {
        Title: constants.Business_Units_list_name,
        query: '$select=Id,Title,Top_Banner_Image2Id,Banner_Title,Banner_Subtitle,Banner_Details,Top_Banner_Transparency,Video,Video_type,Video_Poster2Id,Left_Banner1Id,Left_Banner2Id,Left_Banner3Id,Content_Link1Id,Content_Link2Id,Content_Link3Id,Content_Link4Id,Image_SliderId'       
    },
    {
        Title: constants.Business_Units_Preference_list_name,
        query: '$select=Id,Title,Airframe,Components,Engineering,Business_x0020_Support'       
    },
    {
        Title: constants.Business_Unit_Quick_Links_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Description,Business_Unit,Link,Link_Opens'       
    },
    {
        Title: constants.Buy_and_Sell_list_name,
        query: '$filter=Active eq 1&$top=1&$select=Id,Title,Active,Background2Id'       
    }, 
    {
        Title: constants.Content_Links_list_name,
        query: '$select=Id,Title,Subtitle,Description,Image2Id,Link'
    },
    {
        Title: constants.Content_Page_list_name,
        query: '$select=Id,Title,Top_Image_BackgroundId,Column1Title,Column1SubTitle,Column1Content,Column2Title,Column2SubTitle,Column2Content,Banner1Id,Banner2Id,Banner3Id,ImageSliderId,BlogContent'
    },
    {
        Title: constants.Emergency_Contacts_list_name,
        query: '$filter=Active eq 1&$top=4&$select=Id,Title,Banner2Id,Active,Link,Link_Opens'
    },
    {
        Title: constants.Footer_Images_1_list_name,
        query: '$filter=Active eq 1&$top=1&$select=Id,Title,Image2Id,Active,Link,Link_Opens'
    },
    {
        Title: constants.Footer_Images_2_list_name,
        query: '$filter=Active eq 1&$top=1&$select=Id,Title,Image2Id,Active,Link,Link_Opens'
    },
    {
        Title: constants.Footer_Links_1_list_name,
        query: '$filter=Active eq 1&$top=5&$select=Id,Title,Link,Active,Link_Opens&$orderby=Link_Order'
    },
    {
        Title: constants.Footer_Links_2_list_name,
        query: '$filter=Active eq 1&$top=4&$select=Id,Title,Link,Active'
    },
    {
        Title: constants.Footer_Locations_list_name,
        query: '$filter=Active eq 1&$top=1&$select=Id,Title,Address1,City,State,ZIP,Active,Country'
    },
    {
        Title: constants.Global_Navigation_list_name,
        query: '$filter=Active eq 1&$top=500&$select=Id,Title,Link,Menu_Level,Active,Parent,Item_Type,Link_Order,Users_AllowedId,Opens_in&$orderby=Menu_Level,Parent,Link_Order'
    },
    {
        Title: constants.Global_Preferences_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Preference_x0020_Type'
    },
    {
        Title: constants.Header_top_links_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Link,Link_Order,Target&$orderby=Link_Order'
    },
    {
        Title: constants.Icons_list_name,
        query: '$select=Id,Title,Icon2Id'
    },
    {
        Title: constants.IP_Whitelist_list_name,
        query: '$filter=Active eq 1&$select=Id,IP_Address'
    },
    {
        Title: constants.Image_Sliders_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Image1Id,Image2Id,Image3Id,Image4Id,Image5Id,Image6Id,Image7Id,Image8Id,Image9Id,Image10Id'
    },
    {
        Title: constants.Images_Document_Library_Name,
        query: '$select=Id,ATS_x0020_Title&$expand=FieldValuesAsText&$top=1000'
    },
    {
        Title: constants.How_Do_I_dropdown_menu_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Link,Link_Order&$orderby=Link_Order'
    },
    {
        Title: constants.Left_Navigation_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Link,Active'
    },
    {
        Title: constants.Manuals_List_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Description,Link,ParentId,Other_AirlinesId'
    },
    {
        Title: constants.Quick_Polls_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Option1,Option2,Option3,Option4,Option5,Option1Votes,Option2Votes,Option3Votes,Option4Votes,Option5Votes,Active,Background2Id,FontColor,Answer'
    },    
    {
        Title: constants.Home_main_slider_list_name,
        query: '$filter=Active eq 1&$top=3&$select=Id,Title,Description,Date,Image2Id,Active,Section,Link,Transparency,Transparency_Background,Transparency_Value&$orderby=Section'
    },
    {
        Title: constants.Other_Airlines_list_name,
        query: '$filter=Active eq 1&$select=Id,Title&$orderby=Title'       
    },
    {
        Title: constants.React_Styles_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Style'       
    },
    {
        Title: constants.Secondary_Page_Content_Links_list_name,
        query: '$select=Id,Title,URL,Short_ContentId'       
    },
    {
        Title: constants.Secondary_Page_Content_Link_Groups_list_name,
        query: '$select=Id,Title'       
    },
    {
        Title: constants.Secondary_Pages_list_name,
        query: '$select=Id,Title,Top_Banner_Image2Id,Banner_Title,Banner_Subtitle,Banner_Details,Top_Banner_Transparency,Video,Video_type,Video_Poster2Id,Left_Banner1Id,Left_Banner2Id,Left_Banner3Id,Content_Link1Id,Content_Link2Id,Content_Link3Id,Page_Content,Image_SliderId'       
    },
    {
        Title: constants.Short_Contents_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Sub_Title,Show_Header,Show_Border,Content,Related_PageId,Location,Location_Order,Custom_StyleId,Content_Left_ImageId,Content_SliderId,Background_ImageId,BI_Custom_Style,Width'       
    },
    {
        Title: constants.Home_main_sub_headlines_list_name,
        query: '$filter=Active eq 1&$top=4&$select=Id,Title,Description,Synopsis,Image2Id,Active,Section,Link'
    },
    {
        Title: constants.Templates_List_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Description,Link,ParentId,Other_AirlinesId'
    },
    {
        Title: constants.Materials_Navigation_list_name,
        query: '$filter=Active eq 1&$select=Id,Title,Subtitle,Description,ImageId,Menu,Link,Location,Order0,Check_List_Access'
    }
]
export default config