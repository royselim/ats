import React, {Component} from 'react'

export default class Clock extends Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }
  
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }
  
    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    stdTimeZoneOffset = (today) => {
        const jan = new Date(today.getFullYear(), 0, 1);
        const jul = new Date(today.getFullYear(), 6, 1);
        return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
    }
  
    isDstObserved = (today) => {
        return today.getTimezoneOffset() - this.stdTimeZoneOffset(today);
    }

    tick() {
        let today = new Date();
        const dst = this.isDstObserved(today) ? 1: 0;
        today.setHours(today.getHours() + today.getTimezoneOffset()/60 + this.props.offset + dst);

        this.setState({
            date: today
        });
    }
  
    render() {
        const {date} = this.state;
        const {format, useSuffix} = this.props;
        const dateArray = date.toLocaleTimeString().split(':');
        const meridian = dateArray[2].split(' ')[1];
        const mltryHour = date.getHours();
        const twelveHr = format === '12-hour';
        return (
            <div className="time-item-wrapper text-center">
                <span className="time-item">
                    <span>{`${twelveHr ? dateArray[0] : mltryHour}:${dateArray[1]}`}</span>
                    <span style={{fontSize: '12px'}}>{` ${twelveHr && useSuffix ? meridian: ''}`}</span>
                    {/* {this.state.date.toLocaleTimeString().slice(0, -6)} */}
                </span>
                <small>{this.props.location}</small>
            </div>
        );
    }
}