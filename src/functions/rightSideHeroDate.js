const months = {
    Jan: 'January',
    Feb: 'February',
    Mar: 'March',
    Apr: 'April',
    May: 'May',
    Jun: 'June',
    Jul: 'July',
    Aug: 'August',
    Sep: 'September',
    Oct: 'October',
    Nov: 'November',
    Dec: 'December'
}
const fullMonth = (mon) => months[mon]

const rightSideHeroDate = (date) => {
    let parts = new Date(date).toDateString().slice(4).split(' ')
    return fullMonth(parts[0]) + ' ' + parts[1] + ', ' + parts[2]
}

export default rightSideHeroDate