import 'babel-polyfill'

import Content from './components/Content'
import React from 'react'
import { render } from 'react-dom'
import NavAndHeader from './components/NavAndHeader'
import Footer from './components/widgets-components/Footer'
// import SecondaryPage from './components/SecondaryPage'
import SecondaryPage from './components/alternates/AltSecondaryPage'
import TertiaryPage from './components/TertiaryPage'
import GlobalPreferences from './components/GlobalPreferences'
import AsapForm from './components/forms/AsapFormFinal'
import SafetyConcern from './components/forms/SafetyConcern'
import {getSPListItems,getUrl} from './components/helpers/GetSPListItems'
import Archive from './components/Archive'
import AboutYou from './components/alternates/AboutYou'
import PillarsTemplate from './components/alternates/PillarsTemplate';
import DocumentLibrarySearch from './components/widgets-components/DocumentLibrarySearch';
import DeiRecommendationsOnlineForm from './components/forms/DeiRecommendationsOnlineForm';
import NearMissReporting from './components/forms/NearMissReporting';
import MobileStipendForm from './components/forms/MobileStipendForm';
import ATSEthicsReportingForm from './components/forms/ATSEthicsReportingForm';
import BusinessCardRequestForm from './components/forms/BusinessCardRequestForm'

const getConfig = async(context) => {
    const url = getUrl(context)
    const config = getSPListItems(url,'Config',null,'$filter=Active eq 1&$select=Id,Title,Value&$top=500')
    .then(rslt=>{
        if(Array.isArray(rslt)){
            return rslt
        } else {
            return null
        }
    })
    return await config
}

setTimeout(()=>{
    const context = JSON.parse(document.getElementById('data').innerHTML)
    getConfig(context).then(config => {
        if(config && context){
            const doms = {
                navAndHeader: NavAndHeader,
                root: Content,
                secondaryPage: SecondaryPage,
                TertiaryPage: TertiaryPage,
                globalPreferences: GlobalPreferences,
                asapForm: AsapForm,
                reportSafetyQualityConcern: SafetyConcern,
                footer: Footer,
                archive: Archive,
                aboutYou: AboutYou,
                pillarsTemplate: PillarsTemplate,
                documentLibrarySearch: DocumentLibrarySearch,
                DeiRecommendationsOnlineForm: DeiRecommendationsOnlineForm,
                NearMissReporting: NearMissReporting,
                MobileStipendForm: MobileStipendForm,
                ATSEthicsReportingForm: ATSEthicsReportingForm,
                BusinessCardRequestForm: BusinessCardRequestForm,
            }  
            
            for(var n in doms){
                document.getElementById(n) && 
                render(
                    React.createElement(doms[n],{context,config}),
                    document.getElementById(n)
                )
            }            
        } else {
            render(
                <h1>SYSTEM ERROR! Please contact your Administrator</h1>,
                document.getElementById('navAndHeader')
            )
        }
    })
},100)
