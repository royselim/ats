const config = [
    {
        Title: 'Alerts',
        query: '$filter=Active eq 1&$select=Id,Title,Active,Alert_type,Details,Link'
    },
    {
        Title: 'Applications',
        query: '$filter=Active eq 1&$select=Id,Title,Description,Link,ParentId,Business_UnitId,Active'
    },
    {
        Title: 'Applications Parents',
        query: '$select=Id,Title,Logo'
    },
    {
        Title: 'ATS Clocks',
        query: '$filter=Active eq 1&$select=Id,Title,Location,Offset,Active,Active'
    },
    {
        Title: 'ATSM NEWS',
        query: '$filter=Active eq 1&$top=4&$select=Id,Title,Description,Link,Active'
    },
    {
        Title: 'ATS Safety Hotline Reports',
        query: '$top=0&$select=Id'
    },
    {
        Title: 'Banners',
        query: '$select=Id,Title,Image,Link'
    },
    {
        Title: 'Best Practices List',
        query: '$filter=Active eq 1&$select=Id,Title,Description,Link,Business_UnitId'
    },
    {
        Title: 'Business Units',
        query: '$select=Id,Title,Top_Banner_Image,Banner_Title,Banner_Subtitle,Banner_Details,Top_Banner_Transparency,Video,Video_type,Video_poster,Left_Banner1Id,Left_Banner2Id,Left_Banner3Id,Content_Link1Id,Content_Link2Id,Content_Link3Id'       
    },
    {
        Title: 'Business Units Preferences',
        query: '$select=Id,Title,Airframe,Components,Technical_x0020_Solutions,Business_x0020_Support'       
    },
    {
        Title: 'Buy and Sell',
        query: '$filter=Active eq 1&$top=1&$select=Id,Title,Background,Active,Background2Id'       
    }, 
    {
        Title: 'Content Links',
        query: '$select=Id,Title,Subtitle,Description,Image,Link'
    },
    {
        Title: 'Content Pages',
        query: '$select=Id,Title,TopImageBackground,Column1Title,Column1SubTitle,Column1Content,Column2Title,Column2SubTitle,Column2Content,Banner1Id,Banner2Id,Banner3Id'
    },
    {
        Title: 'Emergency Contacts',
        query: '$filter=Active eq 1&$top=4&$select=Id,Title,Banner,Banner2Id,Active,Link,Link_Opens'
    },
    {
        Title: 'Events',
        query: '$filter=Active eq 1&$select=Id,Title,Date,Description,Link,Active'
    },
    {
        Title: 'Footer Images 1',
        query: '$filter=Active eq 1&$top=1&$select=Id,Title,Image,Active,Link,Link_Opens'
    },
    {
        Title: 'Footer Images 2',
        query: '$filter=Active eq 1&$top=1&$select=Id,Title,Image,Active,Link,Link_Opens'
    },
    {
        Title: 'Footer Links 1',
        query: '$filter=Active eq 1&$top=5&$select=Id,Title,Link,Active,Link_Opens'
    },
    {
        Title: 'Footer Links 2',
        query: '$filter=Active eq 1&$top=4&$select=Id,Title,Link,Active'
    },
    {
        Title: 'Footer Locations',
        query: '$filter=Active eq 1&$top=4&$select=Id,Title,Address1,City,State,ZIP,Active'
    },
    {
        Title: 'Global Navigation',
        query: '$filter=Active eq 1&$select=Id,Title,Link,Menu_Level,Active,Parent,Item_Type,Link_Order,Users_AllowedId,Opens_in&$orderby=Menu_Level,Parent,Link_Order'
    },
    {
        Title: 'Global Preferences',
        query: '$filter=Active eq 1&$select=Id,Title,Preference_x0020_Type'
    },
    {
        Title: 'Header Links',
        query: '$filter=Active eq 1&$select=Id,Title,Link,Link_Order,Target&$orderby=Link_Order'
    },
    {
        Title: 'Icons',
        query: '$select=Id,Title,Icon,Icon2Id'
    },
    {
        Title: 'Images',
        query: '$select=Id&$expand=FieldValuesAsText'
    },
    {
        Title: 'I Need FAQs',
        query: '$filter=Active eq 1&$select=Id,Title,Link,Link_Order&$orderby=Link_Order'
    },
    {
        Title: 'Left Navigation',
        query: '$filter=Active eq 1&$select=Id,Title,Link,Active'
    },
    {
        Title: 'Manuals List',
        query: '$filter=Active eq 1&$select=Id,Title,Description,Link,Business_UnitId'
    },
    {
        Title: 'Quick Poll',
        query: '$filter=Active eq 1&$select=Id,Title,Option1,Option2,Option3,Option4,Option5,Option1Votes,Option2Votes,Option3Votes,Option4Votes,Option5Votes,Active,Background,Background2Id'
    },    
    {
        Title: 'Right Side Hero',
        query: '$filter=Active eq 1&$top=3&$select=Id,Title,Description,Date,Image,Image2Id,Active,Section,Link,Transparency,Transparency_Background,Transparency_Value'
    },
    {
        Title: 'Secondary Pages',
        query: '$select=Id,Title,Top_Banner_Image,Banner_Title,Banner_Subtitle,Banner_Details,Top_Banner_Transparency,Video,Video_type,Video_poster,Left_Banner1Id,Left_Banner2Id,Left_Banner3Id,Content_Link1Id,Content_Link2Id,Content_Link3Id,Page_Content'       
    },
    {
        Title: 'Static Texts',
        query: '$filter=Active eq 1&$select=Id,Title,Text,Active'
    },
    {
        Title: 'Sub Headlines',
        query: '$filter=Active eq 1&$top=4&$select=Id,Title,Description,Synopsis,Image,Image2Id,Url,Active,Section,Link'
    },
    {
        Title: 'Templates List',
        query: '$filter=Active eq 1&$select=Id,Title,Description,Link,Business_UnitId'
    }
]
export default config